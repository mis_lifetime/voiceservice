﻿Imports System.Threading
Imports System.Net.Sockets
Imports System.Net
Imports System.Text

Public Class StateObjectMulti
    ' Client  socket.
    Public workSocket As Socket = Nothing
    ' Size of receive buffer.
    Public Const BufferSize As Integer = 1024
    ' Receive buffer.
    Public buffer(BufferSize) As Byte
    ' Received data string.
    Public sb As New StringBuilder
End Class

Public Class NotPickDirectorMulti
    Public ipHostInfo As IPHostEntry = Dns.GetHostEntry(Dns.GetHostName())
    Public ipAddress As IPAddress = ipHostInfo.AddressList(1)
    Public lstnPort As Integer = 11001

    Public Delegate Function ConsoleEventDelegate(ByVal MyEvent As ConsoleEvent) As Boolean
    Private Declare Function SetConsoleCtrlHandler Lib "kernel32" (ByVal handlerRoutine As ConsoleEventDelegate, ByVal add As Boolean) As Boolean
    Private handler As ConsoleEventDelegate

    ' Monitor Data
    Private Shared monitorMode As Boolean = True    
    Dim ipAddressDCSMsg As IPAddress = ipAddress.Parse("172.16.1.156")
    Dim lstnPortDCSMsg As Integer = 6780
    Dim monitorEndPointDSCMsg As IPEndPoint = New IPEndPoint(ipAddressDCSMsg, lstnPortDCSMsg)
    Private Shared lockF As New Object

    ' Test Mode
    Private testMode As Boolean = False

    ' Counters
    Private nextCartRequestCounter As Integer = 0
    Private dropcartrequestcounter As Integer = 0
    Private pickDownloadCounter As Integer = 0
    Private heartBeatsSent As Integer = 0

    ' Thread signals
    Private threadSignal As New ManualResetEvent(False)
    Private socketAcceptSignal As New ManualResetEvent(False)

    ' Create a TCP/IP socket
    Private listener As New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)

    ' Timer Jobs
    Private listOfTimerJobs As Dictionary(Of String, Timer)

    ' Variables
    Private queueOfUnusedThreads As Queue(Of Thread) = New Queue(Of Thread)
    Private listOfUsedThreads As Dictionary(Of String, Thread) = New Dictionary(Of String, Thread)
    Private threadNumber As Long = 1
    Private listSock As List(Of Socket) = New List(Of Socket)

    ' Construction / control
    Public isRunning As Boolean = False
    Private Shared notPickDirectorInstance As NotPickDirectorMulti = Nothing
    Private notPickDirectorThread As Thread = Nothing

    'Stats Dictionary
    Private Shared statsPack As Dictionary(Of String, String) = New Dictionary(Of String, String)

    Public Enum ConsoleEvent
        CTRL_C_EVENT = 0
        CTRL_BREAK_EVENT = 1
        CTRL_CLOSE_EVENT = 2
        CTRL_LOGOFF_EVENT = 5
        CTRL_SHUTDOWN_EVENT = 6
    End Enum

    'Client
    Private clientSocket As Socket = Nothing

    Private Shared Sub writeToConsole(ByVal msg As String, Optional ByVal overrideFlg As Boolean = False)
        If msg.Contains("LogMode:") OrElse
            msg.Contains("LastDropCartScan:") OrElse
            msg.Contains("TotalPickDownloads:") OrElse
            msg.Contains("TotalDropCartRequests:") OrElse
            msg.Contains("TotalCartStatus:") OrElse
            msg.Contains("PickDownloadLines:") Then

            Dim lines As String() = msg.Split(",")
            For Each widget As String In lines
                If widget.Contains(":") Then
                    Dim key As String = widget.Substring(0, widget.IndexOf(":")).Replace("=> ", "")
                    Dim val As String = widget.Substring(widget.IndexOf(":") + 1)
                    If statsPack.ContainsKey(key) Then
                        statsPack.Remove(key)
                    End If

                    statsPack.Add(key, val)
                End If
            Next
        ElseIf overrideFlg OrElse Not monitorMode Then

            SyncLock lockF

                If msg.Contains("Received:") AndAlso Not msg.Contains("HeartBeat") Then

                    Console.Beep(300, 100)

                    If msg.Contains("PICKDOWNLOAD") Then
                        Console.ForegroundColor = ConsoleColor.Green
                    ElseIf msg.Contains("ITEMSTATUS") Then
                        Console.ForegroundColor = ConsoleColor.Yellow
                    ElseIf msg.Contains("CARTSTATUS") Then
                        Console.ForegroundColor = ConsoleColor.Blue
                    ElseIf msg.Contains("CARTSTATUS") Then
                        Console.ForegroundColor = ConsoleColor.Cyan
                    ElseIf msg.Contains("OPERATORSTATUS") Then
                        Console.ForegroundColor = ConsoleColor.White
                    ElseIf msg.Contains("NEXT") Then
                        Console.ForegroundColor = ConsoleColor.DarkYellow
                    ElseIf msg.Contains("CANCEL") Then
                        Console.ForegroundColor = ConsoleColor.Red
                    Else
                        Console.ResetColor()
                    End If

                    Console.WriteLine("{0} - {1}", Format(Date.Now(), "M/d/yyyy HH:mm:ss:fff"), msg)
                    Console.ResetColor()
                ElseIf msg.Contains("Received:") AndAlso msg.Contains("HeartBeat") Then
                    Console.ForegroundColor = ConsoleColor.Magenta
                    Console.Beep(200, 100)
                    Console.Beep(200, 100)

                    Console.WriteLine("{0} - {1}", Format(Date.Now(), "M/d/yyyy HH:mm:ss:fff"), msg)
                    Console.ResetColor()
                ElseIf msg.Contains("_ACK") Then
                    Console.ForegroundColor = ConsoleColor.DarkCyan

                    Console.WriteLine("{0} - {1}", Format(Date.Now(), "M/d/yyyy HH:mm:ss:fff"), msg)
                    Console.ResetColor()
                Else

                    Console.WriteLine("{0} - {1}", Format(Date.Now(), "M/d/yyyy HH:mm:ss:fff"), msg)
                    Console.ResetColor()
                End If
            End SyncLock
        End If
    End Sub

    Private Function cnsEvent(ByVal [event] As ConsoleEvent) As Boolean

        Dim cancel As Boolean = False

        Select Case [event]

            Case ConsoleEvent.CTRL_C_EVENT
                MsgBox("CTRL+C received!")
            Case ConsoleEvent.CTRL_BREAK_EVENT
                MsgBox("CTRL+BREAK received!")
            Case ConsoleEvent.CTRL_CLOSE_EVENT
                setupMonitor(True)
            Case ConsoleEvent.CTRL_LOGOFF_EVENT
                MsgBox("User is logging off!")
            Case ConsoleEvent.CTRL_SHUTDOWN_EVENT
                MsgBox("Windows is shutting down.")
                ' My cleanup code here
        End Select

        Return cancel ' handling the event.

    End Function

    Public Shared Function getNotPickDirectorInstance() As NotPickDirectorMulti
        writeToConsole("=== getNotPickDirectorInstance() ===")

        If notPickDirectorInstance Is Nothing Then
            notPickDirectorInstance = New NotPickDirectorMulti
        End If

        Return notPickDirectorInstance
    End Function

    Public Sub stopThread()
        writeToConsole("=== stopThread() ===")

        isRunning = False
        Try
            notPickDirectorThread.Abort()
        Catch ex As Exception
        End Try

        listener.Shutdown(SocketShutdown.Both)
        listener.Close()
    End Sub

    Private Sub New()
        writeToConsole("=== New() ===")

        notPickDirectorThread = New Thread(AddressOf runSock)
        notPickDirectorThread.Start()

        listOfTimerJobs = New Dictionary(Of String, Timer)
        loadTimers()
    End Sub

    ' This server waits for a connection and then uses  asychronous operations to
    ' accept the connection, get data from the connected client, 
    ' echo that data back to the connected client.
    ' It then disconnects from the client and waits for another client. 
    Private Sub runSock()
        writeToConsole("=== runSock() ===")

        ' Data buffer for incoming data.
        Dim bytes() As Byte = New [Byte](1023) {}

        ' Establish the local endpoint for the socket.
        
        Dim localEndPoint As New IPEndPoint(ipAddress, lstnPort)

        ' Bind the socket to the local endpoint and listen for incoming connections.
        listener.Bind(localEndPoint)
        listener.Listen(100)

        ' Started by thread so turn on
        isRunning = True

        handler = AddressOf cnsEvent
        If Not SetConsoleCtrlHandler(handler, True) Then
            MsgBox("FAILED HARD")
        End If

        While isRunning
            writeToConsole("==> Running . . . ")
            socketAcceptSignal.Reset()

            Dim t As Thread = getWorkerThread()

            writeToConsole("=== Starting " + t.Name + " ===")

            t.Start()
            listOfUsedThreads.Add(t.Name, t)
            socketAcceptSignal.WaitOne()
        End While

            isRunning = False
    End Sub 'Main

    Private Sub loadTimers()
        If monitorMode Then
            writeToConsole("=== loadTimers( MONITOR MODE ) ===")
            setupMonitor()
            listOfTimerJobs.Add("HOUSEKEEPING", New Timer(AddressOf checkLists, Nothing, 60000, 60000))
            'listOfTimerJobs.Add("ClEAR", New Timer(AddressOf clearScreen, Nothing, 300000, 300000))
        End If

        If testMode Then
            writeToConsole("=== loadTimers( TEST MODE ) ===")

            listOfTimerJobs.Add("HEARTBEAT", New Timer(AddressOf sendHeartbeat, Nothing, 60000, 60000))
            listOfTimerJobs.Add("NEXTCARTREQUEST", New Timer(AddressOf sendNextCartRequest, Nothing, 30000, 30000))
            listOfTimerJobs.Add("DROPCARTREQUEST", New Timer(AddressOf sendDropCartRequest, Nothing, 40000, 30000))
            listOfTimerJobs.Add("HOUSEKEEPING", New Timer(AddressOf checkLists, Nothing, 10000, 10000))
        End If
    End Sub

    Public Sub setupMonitor(Optional ByVal turnOff As Boolean = False)
        writeToConsole("=== setupMonitor( " + turnOff.ToString + " ) ===", True)

        Dim kv As String = "REGISTER"
        Dim regUnregTemplate As String = "^^^^SOCKETMONITOR^^{""MT"":""SOCKETMONITOR"",""IP"":""" & ipAddress.ToString & """,""Port"":""11001"",""KV"":""{REGVAL}""}"
        Dim retString As String = "NOTHING"

        If turnOff Then
            kv = "UNREGISTER"
        Else
            kv = "REGISTER"
        End If

        regUnregTemplate = regUnregTemplate.Replace("{REGVAL}", kv)
        'retString = sendAndReceiveData(monitorEndPointPDMsg, regUnregTemplate, True)

        'writeToConsole("=== PD-RECEVIED: " + retString + " ===", True)

        retString = sendAndReceiveData(monitorEndPointDSCMsg, regUnregTemplate, True)

        writeToConsole("=== DTC-RECEVIED: " + retString + " ===", True)
    End Sub

    Private Sub clearScreen(ByVal state As Object)
        writeToConsole("=== clearScreen() ===")

        Console.Clear()
    End Sub

    Private Sub sendHeartbeat(ByVal state As Object)
        writeToConsole("=== sendHeartbeat() ===")

        If clientSocket IsNot Nothing Then
            Dim testData As Dictionary(Of String, String) = New Dictionary(Of String, String)
            Dim bvct As BaseVoiceCommandTest = New HeartbeatTest
            bvct.supplyTestData(testData)

            heartBeatsSent += 1
            Send(clientSocket, bvct.getFormattedMessage)
        Else
            writeToConsole("=== No Client ===")
        End If
    End Sub

    Private Sub showStats(ByVal state As Object)
        writeToConsole("=== showStats() ===")

        Console.Beep(2000, 500)

        ' copy so we don't get error on update of the stats pack
        Dim xpack As Dictionary(Of String, String) = New Dictionary(Of String, String)
        For Each widget As String In statsPack.Keys
            xpack.Add(widget, statsPack.Item(widget))
        Next

        Dim stats As StringBuilder = New StringBuilder
        Dim cntr As Integer = 0
        For Each widget As String In xpack.Keys
            Dim kpad As Integer = 30 - widget.Length
            Dim kpStr As StringBuilder = New StringBuilder
            Dim vpad As Integer = 25 - xpack.Item(widget).Length
            Dim vpStr As StringBuilder = New StringBuilder

            For i As Integer = 1 To kpad
                kpStr.Append(" ")
            Next

            For i As Integer = 1 To vpad
                vpStr.Append(" ")
            Next

            If cntr > 2 Then
                stats.AppendLine(widget & ": " & kpStr.ToString & xpack.Item(widget) & vpStr.ToString)
                cntr = 0
            Else
                stats.Append(widget & ": " & kpStr.ToString & xpack.Item(widget) & vpStr.ToString)
                cntr += 1
            End If
        Next

        Console.WriteLine(stats)
        xpack = Nothing
        statsPack = New Dictionary(Of String, String)
    End Sub

    Private Sub sendNextCartRequest(ByVal state As Object)
        writeToConsole("=== sendNextCartRequest() ===")

        If clientSocket IsNot Nothing Then
            Dim testData As Dictionary(Of String, String) = New Dictionary(Of String, String)
            Dim bvct As BaseVoiceCommandTest = New NextCartRequestTest
            testData.Add("Area", "KOHLS")
            bvct.supplyTestData(testData)

            Send(clientSocket, bvct.getFormattedMessage)
        Else
            writeToConsole("=== No Client ===")
        End If
    End Sub

    Private Sub sendDropCartRequest(ByVal state As Object)
        writeToConsole("=== sendNextCartRequest() ===")

        If clientSocket IsNot Nothing Then

            Dim testData As Dictionary(Of String, String) = New Dictionary(Of String, String)
            Dim bvct As BaseVoiceCommandTest = New DropCartRequestTest
            testData.Add("CartID", "PPKOHLS02-092361")
            testData.Add("LastPickLoc", "KZ1078A")
            bvct.supplyTestData(testData)

            Send(clientSocket, bvct.getFormattedMessage)
        Else
            writeToConsole("{=== No Client ===")
        End If
    End Sub

    Private Sub socketAccept()
        writeToConsole("=== socketAccept() ===")

        ' Set the event to nonsignaled state.
        threadSignal.Reset()

        ' Start an asynchronous socket to listen for connections.
        writeToConsole("")
        writeToConsole("=================================")
        writeToConsole("== Waiting for a connection... ==")
        writeToConsole("=================================")
        writeToConsole("")

        listener.BeginAccept(New AsyncCallback(AddressOf AcceptCallback), listener)

        ' Wait until a connection is made and processed before continuing.
        threadSignal.WaitOne()
    End Sub

    Private Sub loadThreads()
        writeToConsole("=== loadThreads() ===")

        For i As Integer = 1 To 3 - queueOfUnusedThreads.Count
            Dim t As Thread = New Thread(AddressOf socketAccept)
            t.Name = "Thread" & threadNumber

            writeToConsole("=== " + t.Name + " Loading ===")

            queueOfUnusedThreads.Enqueue(t)
            threadNumber += 1
        Next
    End Sub

    Private Function getWorkerThread() As Thread
        writeToConsole("=== getWorkerThread() ===")

        If queueOfUnusedThreads.Count - 1 < 3 Then loadThreads()
        Return queueOfUnusedThreads.Dequeue
    End Function

    Public Sub AcceptCallback(ByVal ar As IAsyncResult)
        writeToConsole("=== AcceptCallback() ===")

        socketAcceptSignal.Set()

        ' Get the socket that handles the client request.
        Dim listener As Socket = CType(ar.AsyncState, Socket)
        ' End the operation.
        Dim handler As Socket = listener.EndAccept(ar)

        ' Create the state object for the async receive.
        Dim state As New StateObjectMulti
        state.workSocket = handler
        handler.BeginReceive(state.buffer, 0, StateObjectMulti.BufferSize, 0, New AsyncCallback(AddressOf ReadCallback), state)
    End Sub 'AcceptCallback

    Public Sub ReadCallback(ByVal ar As IAsyncResult)
        writeToConsole("=== ReadCallback() ===")

        Dim content As String = String.Empty

        ' Retrieve the state object and the handler socket
        ' from the asynchronous state object.
        Dim state As StateObjectMulti = CType(ar.AsyncState, StateObjectMulti)
        Dim handler As Socket = state.workSocket

        ' Read data from the client socket. 
        Dim bytesRead As Integer = handler.EndReceive(ar)

        If bytesRead > 0 Then
            ' There  might be more data, so store the data received so far.
            state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead))

            ' Check for end-of-file tag. If it is not there, read 
            ' more data.
            content = state.sb.ToString()

            'writeToConsole("=> Read " + content.Length.ToString + " bytes from socket", True)
            writeToConsole("=> " + content, True)

            If content.Contains("$filter$") AndAlso Not monitorMode Then
                writeToConsole("")
                writeToConsole("=== Client Connected ===", True)
                writeToConsole("")

                clientSocket = handler
            Else
                If checkIfThreadClosable(content) Then

                    writeToConsole("")
                    writeToConsole("======================")
                    writeToConsole("=== Closing Thread ===")
                    writeToConsole("======================")
                    writeToConsole("")

                    handler.Shutdown(SocketShutdown.Both)
                    handler.Close()

                    threadSignal.Set()
                Else
                    writeToConsole("=== Not Closing Thread ===")
                End If
            End If
        End If
    End Sub 'ReadCallback

    Private Function checkIfThreadClosable(ByVal s As String) As Boolean
        writeToConsole("=== checkIfThreadClosable ====")

        If s.Contains("NEXTCART") Then
            nextCartRequestCounter += 1
            Return False
        ElseIf s.Contains("HEARTBEAT") Then
            Return False
        ElseIf s.Contains("UNKNOWN") Then
            Return False
        ElseIf s.Contains("DROPCART") Then
            dropcartrequestcounter += 1
            Return False
        ElseIf s.Contains("PICKDOWNLOAD") Then
            pickDownloadCounter += 1
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub Send(ByVal handler As Socket, ByVal data As String)
        writeToConsole("=== Send() ===")

        ' Convert the string data to byte data using ASCII encoding.
        writeToConsole("=== Data: " + data + " ===")
        Dim byteData As Byte() = Encoding.ASCII.GetBytes(data)

        ' Begin sending the data to the remote device.
        handler.BeginSend(byteData, 0, byteData.Length, 0, New AsyncCallback(AddressOf SendCallback), handler)
        Thread.Sleep(10)
    End Sub 'Send


    Private Sub SendCallback(ByVal ar As IAsyncResult)
        writeToConsole("=== sendCallback() ===")

        ' Retrieve the socket from the state object.
        Dim handler As Socket = CType(ar.AsyncState, Socket)

        ' Complete sending the data to the remote device.
        Dim bytesSent As Integer = handler.EndSend(ar)
        writeToConsole("=== Sent " + bytesSent.ToString + " bytes to client ===")

        'handler.Shutdown(SocketShutdown.Both)
        'handler.Close()

        ' Signal the main thread to continue.
        Dim state As New StateObjectMulti
        state.workSocket = handler

        Try
            handler.BeginReceive(state.buffer, 0, StateObjectMulti.BufferSize, 0, New AsyncCallback(AddressOf ReadCallback), state)
        Catch ex As Exception
            writeToConsole("")
            writeToConsole("===================================")
            writeToConsole("=== ERROR-SencCallback: " + ex.ToString + " ===")
            writeToConsole("===================================")
            writeToConsole("")
        End Try
    End Sub 'SendCallback

    Private Function checkLists(ByVal state As Object) As Boolean
        writeToConsole("=== checkLists() ===")

        If clientSocket Is Nothing Then
            writeToConsole("=== No Client Registered ===")
        Else

            Dim isConnected As Boolean = True
            If clientSocket.Poll(0, SelectMode.SelectRead) Then
                Dim byteArray As Byte() = New Byte(1) {}
                If (clientSocket.Receive(byteArray, SocketFlags.Peek)) = 0 Then isConnected = False
            End If

            writeToConsole("=== Client is " + If(isConnected, "ALIVE", "DEAD") + " ===")

            If Not isConnected Then clientSocket = Nothing
        End If

        Dim cloneThreadList As Thread() = listOfUsedThreads.Values.ToArray

        If cloneThreadList.Count < 1 Then
            writeToConsole("=== No Used Threads Alive ===")
        Else
            For Each thrd As Thread In cloneThreadList
                If thrd.IsAlive Then
                    writeToConsole("=== " + thrd.Name + " IS " + If(thrd.IsAlive, "ALIVE", "DEAD") + " ===")
                Else
                    writeToConsole("=== " + thrd.Name + " IS DEAD ===")
                    listOfUsedThreads.Remove(thrd.Name)
                End If
            Next
        End If

        writeToConsole("")

        writeToConsole("== PICKDOWNLOADS: " + pickDownloadCounter.ToString + "    ===")
        writeToConsole("== NEXTCARTREQUESTS: " + nextCartRequestCounter.ToString + "    ===")
        writeToConsole("== DROPCARTREQUESTS: " + dropcartrequestcounter.ToString + "    ===")
        writeToConsole("== HEARBEATS-OUT: " + heartBeatsSent.ToString + "    ===")

        writeToConsole("")

        Return True
    End Function
End Class

