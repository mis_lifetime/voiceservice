﻿Public Class ItemStatusHandler
    Inherits PickDirectorMessageHandler

    Public Overrides ReadOnly Property messageType As String
        Get
            Return "ITEMSTATUS_ACK"
        End Get
    End Property

    Protected Overrides Function loadMocaCommand() As String
        Return "process usr voice command where exec_id = '{0}' and msg = '{1}' and seqnum = {2}"
    End Function

    Protected Overrides Function loadMocaCommandParameters() As System.Collections.Generic.Queue(Of String)
        Dim queueOfParms As Queue(Of String) = New Queue(Of String)
        queueOfParms.Enqueue(baseMessage.messageDictionary.Item("MessageType"))
        queueOfParms.Enqueue(pickDirectorBaseMessage.getMocaMessageString)

        Try
            queueOfParms.Enqueue(baseMessage.messageDictionary.Item("SequenceNumber"))
        Catch ex As Exception
            queueOfParms.Enqueue("")
        End Try

        Return queueOfParms
    End Function

    Sub New(ByVal msgIn As DelimitedMessageParser)
        MyBase.New(msgIn)
    End Sub
End Class