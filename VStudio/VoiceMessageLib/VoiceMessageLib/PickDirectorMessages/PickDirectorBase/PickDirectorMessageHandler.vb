﻿Public MustInherit Class PickDirectorMessageHandler
    Inherits BaseMessageHandler

    Public Property seqNum As String = String.Empty
    Public Shared Property sharedMessageTemplate As String = "{0}{1}^{2}^{3}^{4}^{5}^{6}^{7}{8}"    

    Private Property messageTemplateParametersQueue() As Queue(Of String)
    Public Overridable Property publisherLocation() As String = serviceLocationGlobal
    Public Overridable Property publisherType() As String = serviceTypeGlobal
    Public Overridable Property subscriberLocation() As String = "MessageDLoc"
    Public Overridable Property subscriberType() As String = "MessageDistributor"
    Public MustOverride ReadOnly Property messageType() As String
    Public Property sequenceNumber() As String = String.Empty
    Public Overrides Property delimiter() As String = "^"
    Protected Overridable Property bodyDelimiter() As String = "^"
    Public Overrides Property messageFormat() As String = sharedMessageTemplate
    Public Property messageBodyTemplate() As String
    Public Property messageBodyParameterQueue() As Queue(Of String)
    Public Property pickDirectorBaseMessage As PickDirectorMessageParser       

    Protected Function loadMessageFormatQueue() As System.Collections.Generic.Queue(Of String)
        Dim queueOfParms As Queue(Of String) = New Queue(Of String)
        queueOfParms.Enqueue(Chr(2))
        queueOfParms.Enqueue(publisherLocation)
        queueOfParms.Enqueue(publisherType)
        queueOfParms.Enqueue(subscriberLocation)
        queueOfParms.Enqueue(subscriberType)
        queueOfParms.Enqueue(messageType)                

        Dim sn As String = String.Empty

        Try
            sn = baseMessage.messageDictionary.Item("SequenceNumber")
        Catch ex As Exception
        End Try

        If Not sn.Equals(String.Empty) Then
            queueOfParms.Enqueue(sn & "_ACK")
        Else
            queueOfParms.Enqueue(seqNum)
        End If

        queueOfParms.Enqueue("{Message}")
        queueOfParms.Enqueue(Chr(3))

        Return queueOfParms
    End Function

    Public Function getMainTemplate() As String
        seqNum = "{SEQNUM}"
        Dim q As Queue(Of String) = loadMessageFormatQueue()
        Return autoMessageFormatToTemplate(sharedMessageTemplate, q)
    End Function

    Protected Overrides Function getReturnMessage() As String

        messageBodyParameterQueue = loadMessageBodyParameterQueue()

        Dim aStr = autoMessageFormatToTemplate(messageFormat, messageTemplateParametersQueue)
        aStr = aStr.Replace("{Message}", messageBodyTemplate)
        aStr = autoMessageFormatToTemplate(aStr, messageBodyParameterQueue)

        Return aStr
    End Function

    Public Function getPlainReturnMessage() As String
        messageBodyParameterQueue = loadMessageBodyParameterQueue()

        Dim astr = autoMessageFormatToTemplate(messageBodyTemplate, messageBodyParameterQueue)
        Return astr
    End Function

    Protected Overridable Function loadMessageBodyTemplate() As String
        Return String.Empty
    End Function

    Protected Overridable Function loadMessageBodyParameterQueue() As Queue(Of String)
        Return Nothing
    End Function

    Sub New()
        MyBase.New()

        messageBodyTemplate = loadMessageBodyTemplate()
        messageTemplateParametersQueue = loadMessageFormatQueue()
        subscriberLocation = "MessageDistributor Location"
        subscriberType = "MessageDistributor"
    End Sub

    Sub New(ByVal msgIn As PickDirectorMessageParser)
        MyBase.New(msgIn)

        pickDirectorBaseMessage = msgIn
        subscriberLocation = msgIn.publisherLocation
        subscriberType = msgIn.publisherType
        messageBodyTemplate = loadMessageBodyTemplate()
        messageTemplateParametersQueue = loadMessageFormatQueue()        
    End Sub
End Class