﻿Public Class HeartbeatParser
    Inherits PickDirectorMessageParser

    Public Overrides Property requiresReturnMsg As Boolean = False

    Public Overrides Property messageType As String = "HEARTBEAT"

    Protected Overrides Function loadMessageBodyKeys() As String()
        Return Nothing
    End Function

    Sub New(ByVal msg As String)
        MyBase.New(msg)
    End Sub
End Class