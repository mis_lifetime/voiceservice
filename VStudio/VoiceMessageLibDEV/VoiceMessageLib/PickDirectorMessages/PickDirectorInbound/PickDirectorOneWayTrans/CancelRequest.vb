﻿Public Class CancelRequest
    Inherits PickDirectorMessageParser

    Public Overrides Property passThroughMessage As Boolean = True

    Protected Overrides Function loadMessageBodyKeys() As String()
        Dim mbk As String() = {"MT", "CancelType", "CancelID"}
        Return mbk
    End Function

    Public Overrides Property messageType As String = "CANCELREQUEST"

    Sub New(ByVal msg As String)
        MyBase.New(msg)
    End Sub
End Class
