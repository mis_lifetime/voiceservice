﻿Public Class NextCartRequest
    Inherits PickDirectorMessageParser

    Public Overrides Property messageType() As String = "NEXTCARTREQUEST"

    Protected Overrides Function loadMessageBodyKeys() As String()
        Dim mbk As String() = {"MT", "Zone", "OperatorID"}
        Return mbk
    End Function

    Public Shared Property messageBodyTemplate As String = "{""MT"":""{0}"",""Zone"":""{1}"",""OperatorID"":""{2}""}"

    Sub New(ByVal msg As String)
        MyBase.New(msg)
    End Sub
End Class