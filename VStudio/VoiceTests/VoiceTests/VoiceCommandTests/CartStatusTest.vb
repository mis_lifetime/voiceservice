﻿Imports VoiceMessageLibDEV

Public Class CartStatusTest
    Inherits BaseVoiceCommandTest

    Protected Overrides ReadOnly Property messageBodyTemplate As String
        Get
            Return "{""MT"":""{0}"",""CartID"":""{1}"",""Date"":""{2}"",""Time"":""{3}"",""OperatorID"":""{4}""" & _
                ",""Type"":""{5}"",""DropOffLocation"":""{6}""}"
        End Get
    End Property

    Protected Overrides Property testMessageType As String = "CARTSTATUS"
    Protected Overrides Property testName As String = "Cart Status Test"

    Protected Overrides Function getMessageBodyDataParameters() As Queue(Of String)
        Dim q As Queue(Of String) = New Queue(Of String)

        q.Enqueue(testMessageType)
        q.Enqueue(testData.Item("CartID"))
        q.Enqueue("06/19/2014")
        q.Enqueue("140000")
        q.Enqueue("JOHNDOE")
        q.Enqueue("DIRECTSCAN")
        q.Enqueue("DLOC")

        Return q
    End Function

    Sub New()
        MyBase.New(messageParsers.CartStatus, messageHandlers.CartStatusHandler)
    End Sub

    Protected Overrides Function validateTestData() As Dictionary(Of String, String)
        Dim tdResults As Dictionary(Of String, String) = New Dictionary(Of String, String)

        If Not testData.ContainsKey("CartID") Then
            tdResults.Add("NO_ARG", "Missing Test Data Parameter CartID")
            Return tdResults
        Else
            Return tdResults
        End If
    End Function
End Class
