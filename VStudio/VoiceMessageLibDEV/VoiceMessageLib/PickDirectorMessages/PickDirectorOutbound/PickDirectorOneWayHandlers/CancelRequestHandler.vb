﻿Public Class CancelRequestHandler
    Inherits PickDirectorMessageHandler

    Protected Overrides Function loadMessageBodyTemplate() As String
        Return sharedMessageBodyTemplate
    End Function

    Public Shared ReadOnly Property sharedMessageBodyTemplate
        Get
            Return "{""MT"":""{0}"",""CancelType"":""{1}"",""CancelID"":""{2}""}"
        End Get
    End Property

    Protected Overrides Function loadMessageBodyParameterQueue() As System.Collections.Generic.Queue(Of String)
        Dim msgQ As Queue(Of String) = New Queue(Of String)
        Dim aStr As String() = Nothing
        Dim cncType As String = String.Empty
        Dim cncID As String = String.Empty        

        Try
            aStr = messageOutParameters.Item("msg_out").Split(",")
        Catch
        End Try

        Try
            cncType = pickDirectorBaseMessage.messageBodyDictionary.Item("CancelType")
        Catch
        End Try

        Try
            cncID = pickDirectorBaseMessage.messageBodyDictionary.Item("CancelID")
        Catch
        End Try

        cncID = cncID.Replace(Chr(3), "")

        msgQ.Enqueue(messageType)
        msgQ.Enqueue(cncType)
        msgQ.Enqueue(cncID)

        Return msgQ
    End Function

    Public Overrides ReadOnly Property messageType As String
        Get
            Return "CANCELREQUEST"
        End Get
    End Property

    Sub New(ByVal msgIn As DelimitedMessageParser)
        MyBase.New(msgIn)
    End Sub
End Class
