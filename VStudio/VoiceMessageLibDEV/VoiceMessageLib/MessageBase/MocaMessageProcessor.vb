﻿Public Interface MocaMessageProcessor
    Property mocaCommandTemplate() As String
    Property translatedMocaCommand() As String
    Property returnData() As Dictionary(Of String, Dictionary(Of String, String))
    Property mocaCommandParameters() As Queue(Of String)
    Property mocaCmdUsed As String
    Property mocaCmdStartTime As String
    Property mocaCmdEndTime As String
    Property mocaCmdResult As String

    Function loadMocaCommandParameters() As Queue(Of String)
    Function loadReturnData(mc As RedPrairie.MOCA.Client.FullConnection) As Dictionary(Of String, Dictionary(Of String, String))
End Interface
