﻿Imports System.Threading
Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports VoiceMessageLibDEV

Public Class DCSConnectListener

    ' Thread signal
    Public id As String = "DCS-6781"
    Private rcpId As ULong
    Private msgId As ULong
    Private rcpSt As Date
    Private rcpEt As Date
    Private msgSt As Date
    Private msgEt As Date
    Public allDone As New ManualResetEvent(False)
    Private socketState As socketStates
    Private otherInformation As String = String.Empty
    Private listener As Socket
    Private serviceMessage As PickDirectorMessageParser
    Private loggingModeVal As loggingmodes = loggingmodes.release
    Private isRunning As Boolean = False
    Private timers As Dictionary(Of String, Timer) = New Dictionary(Of String, Timer)

    Property loggingmode() As String
        Get
            Return loggingModeVal
        End Get
        Set(ByVal value As String)
            loggingModeVal = value
        End Set
    End Property

    Enum loggingmodes
        verbose
        normal
        release
    End Enum

    Sub startServiceEvent()

        logGenericAlertMessage("Starting Up PD-6781", "STARTUP", logLevel.HIGH)

        signalSocketEvent(socketStates.Starting)

        isRunning = True
        ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf workerThread))
        serverStartTime = Date.Now
    End Sub ' lstnr_start

    Private Sub workerThread()
        ' Data buffer for incoming data.        
        Dim bytes(bufferSize) As Byte

        ' Establish the local endpoint for the socket.        
        Dim ipHostInfo As IPHostEntry = Dns.GetHostEntry(Dns.GetHostName())
        Dim ipAddress As IPAddress = ipHostInfo.AddressList(1)
        Dim lstnPort As Integer = 6781
        Dim localEndPoint As New IPEndPoint(ipAddress, lstnPort)

        ' Create a TCP/IP socket.        
        listener = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)

        ' Bind the socket to the local endpoint and listen for incoming connections.
        listener.Bind(localEndPoint)
        listener.Listen(100)

        listener.ReceiveTimeout = 10000
        listener.SendTimeout = 10000

        'timers.Add("HEARTBEAT", New Timer(AddressOf sendStatistics, Nothing, 60000, 60000))
        'logAlertMessage("Service Starting on " & localEndPoint.Address.ToString & " : " & localEndPoint.Port, id, logLevel.NORMAL)

        rcpId = 0
        msgId = 0
        While isRunning

            Try
                rcpId += 1
                signalSocketEvent(socketStates.Started)

                ' Set the event to nonsignaled state.
                allDone.Reset()

                ' Start an asynchronous socket to listen for connections.            
                listener.BeginAccept(New AsyncCallback(AddressOf AcceptCallback), listener)

                ' Wait until a connection is made and processed before continuing.
                signalSocketEvent(socketStates.Blocking)
                allDone.WaitOne()

                rcpEt = Now
                logServiceMessage(rcpId, serviceMessage.baseMessage, Format(rcpSt, "yyyyMMddHHmmssfff"), Format(rcpEt, "yyyyMMddHHmmssfff"), 0, "", "BASE", "BASE", "", False, id)
                msgId = 0
            Catch ex As Exception
                If isRunning Then
                    logGenericAlertMessage("Err 6781: " & ex.ToString, id, logLevel.APPERROR)
                End If
            End Try
        End While

        signalSocketEvent(socketStates.Stopped)
    End Sub

    Sub stopServiceEvent()
        isRunning = False

        Try
            logGenericAlertMessage("PD-6781 Service is Stopping", id, logLevel.HIGH)
            'notifyClients("Service is Stopping", "PD-6781")

            listener.Close()

            logGenericAlertMessage("PD-6781 Service is Stopped", id, logLevel.HIGH)
            allDone.Set()
        Catch ex As Exception
            logGenericAlertMessage("Err Stop LTBMBL2: " & ex.ToString, id, logLevel.APPERROR)
        End Try
    End Sub ' lstnr_stop

    ' This Sub Handles the Client
    Public Sub AcceptCallback(ByVal ar As IAsyncResult)
        Try
            ' Get the socket that handles the client request.
            Dim listener As Socket = CType(ar.AsyncState, Socket)
            ' End the operation.
            Dim handler As Socket = listener.EndAccept(ar)

            ' Create the state object for the async receive.
            Dim state As New StateObject
            state.workSocket = handler
            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, New AsyncCallback(AddressOf ReadCallback), state)
        Catch ex As Exception
            otherInformation = ex.ToString
            signalSocketEvent(socketStates.Borked)
            allDone.Set()
        End Try
    End Sub 'AcceptCallback

    Public Sub ReadCallback(ByVal ar As IAsyncResult)

        ' Retrieve the state object and the handler socket
        ' from the asynchronous state object.
        Dim state As StateObject = CType(ar.AsyncState, StateObject)
        Dim handler As Socket = state.workSocket

        Try
            rcpSt = Now
            Dim content As String = String.Empty
            Dim isUnreckognizedMessage = False

            serviceMessage = Nothing

            ' Read data from the client socket. 
            Dim bytesRead As Integer = handler.EndReceive(ar)

            signalSocketEvent(socketStates.Receiving)

            ' There  might be more data, so store the data received so far.
            state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead))

            ' Check for end-of-file tag. If it is not there, read 
            ' more data.
            content = state.sb.ToString()

            'notifyClients("Service Received: " & content, "PD-6781")
            'logAlertMessage("Rcv: " & content, "SRV6781", logLevel.HIGH)

            Try
                serviceMessage = PickDirectorMessageParserFactory.getPickDirectorParser(content)

                'logAlertMessage("ReadCallback(" & bytesRead & "): " & content, id & serviceMessage.messageType, logLevel.NORMAL)
                'logAlertMessage("MessageType: " & serviceMessage.messageType, id, logLevel.LOW)

                'StatisticVarsModule.updateStatistic(serviceMessage)

                signalSocketEvent(socketStates.Received)
                Send(handler, String.Empty)
            Catch ohNos As Exception
                logGenericAlertMessage("SRV6781: " & "Bad Msg->" & content, logLevel.CRITICAL)

                Try
                    handler.Shutdown(SocketShutdown.Both)
                    handler.Close()
                Catch ex As Exception
                Finally
                    allDone.Set()
                End Try
            End Try
        Catch ex As Exception
            otherInformation = ex.ToString
            signalSocketEvent(socketStates.Borked)

            Try
                handler.Shutdown(SocketShutdown.Both)
                handler.Close()
            Catch hce As Exception
            Finally
                allDone.Set()
            End Try
        End Try
    End Sub 'ReadCallback

    Private Sub Send(ByVal handler As Socket, ByVal data As String)
        msgId += 1
        msgSt = Now

        Dim byteData As Byte() = Nothing

        If serviceMessage.messageType.Equals("SOCKETMONITOR") Then handleRegisterUnregister(serviceMessage)

        If serviceMessage.requiresFastAck Then
            Dim ackConn As MocaConnection = getNextMocaConn()
            SendMessageFactory.sendFastAckMessage(serviceMessage, ackConn)
            ackConn.cleanUp()
            If ackConn.isUnmanaged Then
                ackConn = Nothing
            End If
        End If

        If serviceMessage.requiresReturnMsg Then

            Dim sendData As String = String.Empty
            Dim runConn As MocaConnection = getNextMocaConn()
            Dim msgTimings As ReturnMessageTimings = SendMessageFactory.getSendMessageWithTimings(serviceMessage, runConn, rcpId, msgId)
            sendData = msgTimings.returnMessage

            runConn.cleanUp()
            If runConn.isUnmanaged Then
                runConn = Nothing
            End If

            logServiceMessage(rcpId, msgTimings.mocaCmd, msgTimings.mocaCmdSt, msgTimings.mocaCmdEt, msgId, msgTimings.mocaCmdResult, "MOCA", "MOCA", _
                                            msgTimings.mocaCmdResult, False, id)

            ' May want to just set the socket and move on
            If sendData = String.Empty Then sendData = "ERROR: Nothing to Send"

            'logGenericAlertMessage("send(): " & sendData, id & serviceMessage.messageType, logLevel.HIGH)

            ' Convert the string data to byte data using ASCII encoding.
            byteData = Encoding.ASCII.GetBytes(sendData)

            ' Begin sending the data to the remote device.            
            otherInformation = Encoding.ASCII.GetString(byteData, 0, byteData.Length)
            signalSocketEvent(socketStates.Other)
            handler.BeginSend(byteData, 0, byteData.Length, 0, New AsyncCallback(AddressOf SendCallback), handler)

            msgEt = Now
            logServiceMessage(rcpId, serviceMessage.baseMessage, Format(msgSt, "yyyyMMddHHmmssfff"), Format(msgEt, "yyyyMMddHHmmssfff"), msgId, sendData, serviceMessage.messageType, _
                              serviceMessage.messageType, "", False, id)
        Else
            'logAlertMessage("send(): Message Does Not Require a Return", id & serviceMessage.messageType, logLevel.HIGH)

            Dim runConn As MocaConnection = getNextMocaConn()
            SendMessageFactory.getSendMessage(serviceMessage, runConn)
            runConn.cleanUp()
            If runConn.isUnmanaged Then
                runConn = Nothing
            End If

            msgEt = Now
            logServiceMessage(rcpId, serviceMessage.baseMessage, Format(msgSt, "yyyyMMddHHmmssfff"), Format(msgEt, "yyyyMMddHHmmssfff"), msgId, "", serviceMessage.messageType, _
                              serviceMessage.messageType, "", False, id)

            Try
                handler.Shutdown(SocketShutdown.Both)
                handler.Close()
            Catch hce As Exception
                logGenericAlertMessage("send() ERR: ", hce.ToString, logLevel.HIGH)
            Finally                
                allDone.Set()
            End Try
        End If
    End Sub 'Send

    Private Sub SendCallback(ByVal ar As IAsyncResult)
        Try
            signalSocketEvent(socketStates.Sending)

            ' Retrieve the socket from the state object.
            Dim handler As Socket = CType(ar.AsyncState, Socket)

            ' Complete sending the data to the remote device.
            Dim bytesSent As Integer = handler.EndSend(ar)

            otherInformation = bytesSent & " bytes were sent back to the client"
            signalSocketEvent(socketStates.Other)

            Try
                handler.Shutdown(SocketShutdown.Both)
                handler.Close()

                ' Signal the main thread to continue.
                allDone.Set()
            Catch ex As SocketException
                otherInformation = ex.ToString
                signalSocketEvent(socketStates.Borked)
            End Try
        Catch ex As Exception
            EventLog.WriteEntry("LTBMMDAServiceSocket", ex.ToString, _
                                   EventLogEntryType.Error, 200, 200)
            otherInformation = ex.ToString
            signalSocketEvent(socketStates.Borked)
            allDone.Set()
        End Try
    End Sub 'SendCallback

    Private Function signalSocketEvent(ByVal sState As socketStates) As String
        Dim eLogMsg As String = String.Empty

        Try
            Dim logIt As Boolean = False
            Dim lvl As logLevel = logLevel.NORMAL

            socketState = sState

            Select Case loggingmode
                Case loggingmodes.verbose
                    logIt = True
                Case loggingmodes.normal
                    logIt = False
                Case loggingmodes.release
                    logIt = False
                Case Else
                    logIt = True
            End Select

            Select Case sState
                Case socketStates.Blocking
                    eLogMsg = "Socket is Blocking / Waiting for a Connection"
                Case socketStates.Receiving
                    eLogMsg = "Socket is Receiving Message"
                Case socketStates.Sending
                    eLogMsg = "Socket is Sending Back Data"
                Case socketStates.Starting
                    eLogMsg = "Socket Thread is Starting Up"
                Case socketStates.Started
                    eLogMsg = "Socket Thread Running"
                Case socketStates.ConnectingMOCA
                    eLogMsg = "Getting a MOCA Connection via MMDA"
                Case socketStates.ReceivingMOCA
                    eLogMsg = "Receiving Data From MOCA via MMDA"
                Case socketStates.Stopping
                    eLogMsg = "Socket is Shutting Down"
                Case socketStates.Stopped
                    eLogMsg = "Socket has Stopped"
                Case socketStates.Borked
                    lvl = logLevel.APPERROR
                    eLogMsg = otherInformation
                    logIt = True
                Case socketStates.Received
                    eLogMsg = "Socket has Received All Data"
                Case socketStates.WaitingForEOF
                    eLogMsg = "Socket Receiving And Waiting For EOF"
                Case Else
                    logIt = True
                    eLogMsg = otherInformation
            End Select

            If logIt Then logGenericAlertMessage(eLogMsg, id, lvl)

            'notifyClients(eLogMsg, id)
        Catch ex As Exception
            eLogMsg = ex.ToString
            logGenericAlertMessage(eLogMsg, id & "ERROR", logLevel.APPERROR)
            allDone.Set()
        End Try

        Return eLogMsg
    End Function

    Public Function getSocketState() As Object
        Return socketState
    End Function
End Class
