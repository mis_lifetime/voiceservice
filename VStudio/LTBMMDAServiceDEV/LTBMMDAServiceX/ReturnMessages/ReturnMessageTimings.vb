﻿Public Class ReturnMessageTimings
    Public Property mocaCmd As String
    Public Property mocaCmdSt As String
    Public Property mocaCmdEt As String
    Public Property mocaCmdResult As String
    Public Property returnMessage As String

    Sub New(mc As String, mcs As String, mce As String, mcr As String, rm As String)
        mocaCmd = mc
        mocaCmdSt = mcs
        mocaCmdEt = mce
        mocaCmdResult = mcr
        returnMessage = rm
    End Sub
End Class
