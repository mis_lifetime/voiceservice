﻿Public Class OperatorStatus
    Inherits PickDirectorMessageParser

    Public Overrides Property requiresFastAck As Boolean = True
    Public Overrides Property requiresReturnMsg As Boolean = False

    Protected Overrides Function loadMessageBodyKeys() As String()
        Dim mbk As String() = {"MT", "OperatorID", "TalkmanSerialNumber", "Date", "Time", "Status", "Zone"}
        Return mbk
    End Function

    Public Overrides Property messageType As String = "OPERATORSTATUS"

    Sub New(ByVal msg As String)
        MyBase.New(msg)
    End Sub
End Class