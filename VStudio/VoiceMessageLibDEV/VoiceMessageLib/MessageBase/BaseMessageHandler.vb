﻿Public MustInherit Class BaseMessageHandler
    Implements DelimitedMessageHandler, StaticMessageProcessor, MocaMessageProcessor

    Public Property baseMessage() As DelimitedMessageParser Implements DelimitedMessageHandler.baseMessage
    Public Overridable Property mocaCommand() As String Implements MocaMessageProcessor.mocaCommandTemplate
    Public Property returnData() As Dictionary(Of String, Dictionary(Of String, String)) = New Dictionary(Of String, Dictionary(Of String, String)) _
        Implements MocaMessageProcessor.returnData
    Public Property translatedMocaCommand() As String Implements MocaMessageProcessor.translatedMocaCommand
    Public Property mocaCommandParameters() As Queue(Of String) = New Queue(Of String) _
        Implements MocaMessageProcessor.mocaCommandParameters
    Public Property messageOutParameters() As Dictionary(Of String, String) = New Dictionary(Of String, String) _
        Implements DelimitedMessageHandler.messageOutParameters
    Public Property returnMessage() As String Implements StaticMessageProcessor.returnMessage
    Public Property mocaCmdUsed As String Implements MocaMessageProcessor.mocaCmdUsed
    Public Property mocaCmdStartTime As String Implements MocaMessageProcessor.mocaCmdStartTime
    Public Property mocaCmdEndTime As String Implements MocaMessageProcessor.mocaCmdEndTime
    Public Property mocaCmdResult As String Implements MocaMessageProcessor.mocaCmdResult

    Public Function loadReturnData(mc As RedPrairie.MOCA.Client.FullConnection) As Dictionary(Of String, Dictionary(Of String, String)) Implements MocaMessageProcessor.loadReturnData
        Dim res As Dictionary(Of String, Dictionary(Of String, String))
        res = processMocaCmdAndGetResults(mc, translatedMocaCommand)

        If res.ContainsKey("MOCA_PERF") Then
            Dim mp As Dictionary(Of String, String) = res.Item("MOCA_PERF")
            If mp.ContainsKey("CMD") Then mocaCmdUsed = mp.Item("CMD")
            If mp.ContainsKey("ST") Then mocaCmdStartTime = mp.Item("ST")
            If mp.ContainsKey("ET") Then mocaCmdEndTime = mp.Item("ET")
            If mp.ContainsKey("RSL") Then mocaCmdResult = mp.Item("RSL")
        End If

        Return res
    End Function

    Protected Overridable Function loadMocaCommandParameters() As Queue(Of String) Implements MocaMessageProcessor.loadMocaCommandParameters
        Return Nothing
    End Function

    Public MustOverride Property delimiter() As String Implements DelimitedMessageHandler.delimiter
    Public MustOverride Property messageFormat() As String Implements DelimitedMessageHandler.messageFormat

    Protected MustOverride Function getReturnMessage() As String
    Protected Overridable Function loadMocaCommand() As String
        Return "[select 1 msg_out from dual]"
    End Function

    Sub processMessage(mc As RedPrairie.MOCA.Client.FullConnection, Optional ByVal fastFlg As Boolean = False)

        mocaCommandParameters = loadMocaCommandParameters()
        translatedMocaCommand = autoMessageFormatToTemplate(mocaCommand, mocaCommandParameters)

        If Not fastFlg Then returnData = loadReturnData(mc)
        If Not fastFlg Then messageOutParameters = returnData("MOCA_1")

        returnMessage = getReturnMessage()
    End Sub

    Sub New(ByVal msgIn As BaseMessageParser)

        baseMessage = msgIn
        'mocaCommand = "publish data where wh_id = 'WMD1' | " & loadMocaCommand()
        mocaCommand = loadMocaCommand()
    End Sub

    Sub New()

        'mocaCommand = "publish data where wh_id = 'WMD1' | " & loadMocaCommand()
        mocaCommand = loadMocaCommand()
    End Sub
End Class
