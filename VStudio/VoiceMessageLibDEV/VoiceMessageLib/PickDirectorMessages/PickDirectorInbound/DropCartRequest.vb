﻿Public Class DropCartRequest
    Inherits PickDirectorMessageParser

    Protected Overrides Function loadMessageBodyKeys() As String()
        Dim mbk As String() = {"MT", "CartID", "LastPickLoc", "OperatorID"}
        Return mbk
    End Function

    Public Overrides Property messageType As String = "DROPCARTREQUEST"

    Sub New(ByVal msg As String)
        MyBase.New(msg)
    End Sub
End Class
