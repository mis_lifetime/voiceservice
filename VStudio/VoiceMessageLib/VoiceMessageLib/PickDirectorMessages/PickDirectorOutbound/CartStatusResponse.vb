﻿Public Class CartStatusResponse
    Inherits PickDirectorMessageHandler

    Public Overrides ReadOnly Property messageType() As String
        Get
            Return "CARTSTATUSRESPONSE"
        End Get
    End Property

    Protected Overrides Function loadMessageBodyParameterQueue() As System.Collections.Generic.Queue(Of String)
        Dim msgQ As Queue(Of String) = New Queue(Of String)
        Dim aStr As String() = Nothing
        Dim cId As String = String.Empty        
        Dim cSt As String = String.Empty

        Try
            aStr = messageOutParameters.Item("msg_out").Split(",")
        Catch
        End Try

        Try
            cId = baseMessage.messageDictionary.Item("MB_CartID")
        Catch
        End Try

        Try
            cSt = aStr(0)
        Catch
        End Try

        msgQ.Enqueue(messageType)
        msgQ.Enqueue(cId)
        msgQ.Enqueue(cSt)        

        Return msgQ
    End Function

    Protected Overrides Function loadMessageBodyTemplate() As String
        Return "{""MT"":""{0}"",""CartID"":""{1}"",""Status"":""{2}""}"
    End Function

    Protected Overrides Function loadMocaCommand() As String
        Return "process usr voice command where exec_id = '{0}' and msg = '{1}'"
    End Function

    Protected Overrides Function loadMocaCommandParameters() As System.Collections.Generic.Queue(Of String)
        Dim queueOfParms As Queue(Of String) = New Queue(Of String)
        Dim x As String = String.Empty

        queueOfParms.Enqueue(baseMessage.messageDictionary.Item("MessageType"))
        queueOfParms.Enqueue(pickDirectorBaseMessage.getMocaMessageString)

        Return queueOfParms
    End Function

    Sub New(ByVal msgIn As DelimitedMessageParser)
        MyBase.New(msgIn)
    End Sub
End Class