﻿Public Class SocketMonitor
    Inherits PickDirectorMessageParser

    Public Overrides Property passThroughMessage As Boolean = True
    Public Overrides Property messageType() As String = "SOCKETMONITOR"

    Protected Overrides Function loadMessageBodyKeys() As String()
        Dim mbk As String() = {"MT", "IP", "Port", "KV"}
        Return mbk
    End Function

    Public Shared Property messageBodyTemplate As String = "{""MT"":""{0}"",""IP"":""{1}"",""Port"":""{2}"", ""KV"":""{3}""}"

    Sub New(ByVal msg As String)
        MyBase.New(msg)
    End Sub
End Class
