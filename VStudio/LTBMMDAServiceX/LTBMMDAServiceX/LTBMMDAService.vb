﻿Imports System.Threading

Public Class LTBMMDAService


    'Dim ltbMocaBridgeListener As LTBMocaBridgeListener
    Dim dcsConnectListener As DCSConnectListener
    Dim greenService As PickDirectorConnectionGreen
    Dim yellowService As PickDirectorConnectionYellow
    Dim orangeService As PickDirectorConnectionOrange

    Protected Overrides Sub OnStart(ByVal args() As String)
        If Not EventLog.SourceExists(EVT_SOURCE, EVT_MACHINE) Then
            eventData.MachineName = "."
            EventLog.CreateEventSource(eventData)
        End If

        loadMocaConnections()

        logGenericAlertMessage("LTBMMDAService in OnStart()", "LTBMMDAService", logLevel.CRITICAL)
        EventLog.WriteEntry("LTBMMDAService Starting Init")

        LTBMMDAServiceGlobals.initProcess()

        EventLog.WriteEntry("LTBMMDAService Starting GreenPDConn")
        greenService = New PickDirectorConnectionGreen
        greenService.startSock()

        EventLog.WriteEntry("LTBMMDAService Starting YellowPDConn")
        yellowService = New PickDirectorConnectionYellow
        yellowService.startSock()

        EventLog.WriteEntry("LTBMMDAService Starting OrangePDConn")
        orangeService = New PickDirectorConnectionOrange
        orangeService.startSock()

        EventLog.WriteEntry("LTBMMDAService Starting 6781")
        dcsConnectListener = New DCSConnectListener
        dcsConnectListener.startServiceEvent()

        'EventLog.WriteEntry("LTBMMDAService Starting 6780")
        'ltbMocaBridgeListener = New LTBMocaBridgeListener
        'ltbMocaBridgeListener.startServiceEvent()
    End Sub

    Protected Overrides Sub OnStop()
        logGenericAlertMessage("LTBMMDAService in OnStop()", "LTBMMDAService")

        'ltbMocaBridgeListener.stopServiceEvent()
        dcsConnectListener.stopServiceEvent()

        greenService.stopSock()
        yellowService.stopSock()
        orangeService.stopSock()

        Thread.Sleep(1500)

        Try
            stopMocaConnections()
        Catch ex As Exception
        End Try
    End Sub
End Class
