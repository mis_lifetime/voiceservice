﻿Imports System.Xml

Public Module VoiceMessageLibGlobals
    Public mocaCmd As String = "log usr voice msg where vmsg_typ = '{0}' and vmsg_data = '{1}' and vmsg_status = '{2}' and tsval = '{3}'"
    Public serviceTypeGlobal As String = "LTBMMDAService"

    ' Moca Server Info    
    'Public serviceLocationGlobal As String = "PD-LTB-NJ"
    'Private mocaServerIP As String = "172.16.1.44"
    'Private mocaServerPort As Short = 4800
    'Private mocaServerEnv As String = "prod"

    ' LIVE SETTINGS
    'Private mocaServerIP As String = "172.16.1.44"
    'Private mocaServerPort As Short = 4800
    'Private mocaServerEnv As String = "prod"
    'Public serviceLocationGlobal As String = "PD-LTB-NJ"

    ' TEST SETTINGS
    'Private mocaServerIP As String = "172.16.1.43"
    'Private mocaServerPort As Short = 4600
    'Private mocaServerEnv As String = "newprod"
    'Public serviceLocationGlobal As String = "PD-TEST-NJ"

    'JDA PRODUCTION SETTINGS
    'Private mocaServerIP As String = "172.16.1.171"
    'Private mocaServerPort As Short = 4250
    'Private mocaServerEnv As String = "dev"
    'Public serviceLocationGlobal As String = "PD-DEV-NJ"

    ' DEV SETTINGS
    Private mocaServerIP As String = "172.16.1.133"
    Private mocaServerPort As Short = 4250
    Private mocaServerEnv As String = "dev"
    Public serviceLocationGlobal As String = "PD-DEV-NJ"

    Public Function autoMessageFormatToTemplate(ByVal returnMessage As String, ByVal parameterQ As Queue(Of String)) As String

        Dim paramCounter As Integer = 0
        Do

            If returnMessage.Contains("{" & paramCounter & "}") Then
                Try
                    Dim s As String = parameterQ.Dequeue

                    returnMessage = returnMessage.Replace("{" & paramCounter & "}", s)
                    paramCounter += 1
                Catch ex As Exception
                    ' TODO: Handle
                    returnMessage = returnMessage.Replace("{" & paramCounter & "}", "")
                    paramCounter = -1
                End Try
            Else
                paramCounter = -1
            End If
        Loop Until paramCounter < 0

        If parameterQ IsNot Nothing AndAlso parameterQ.Count > 0 Then
            'TODO: Handle
        End If

        Return returnMessage
    End Function

    Public Function processMocaCmdAndGetResults(mocaConn As RedPrairie.MOCA.Client.FullConnection, ByVal mCmd As String) As Dictionary(Of String, Dictionary(Of String, String))
        Return processMocaCmdAndGetResults(mocaConn, mCmd, True)
    End Function

    Public Sub connectMocaConn(ByRef mc As RedPrairie.MOCA.Client.FullConnection)
        mc.Connect("WMD1", mocaServerIP, mocaServerPort, mocaServerEnv, False)
        mc.Login("VOICE_SRV", "1234")
    End Sub

    Public Function getMocaConn() As RedPrairie.MOCA.Client.FullConnection
        Dim comObj As RedPrairie.MOCA.Client.FullConnection = New RedPrairie.MOCA.Client.FullConnection

        connectMocaConn(comObj)

        Return comObj
    End Function

    Public Function processMocaCmdAndGetResults(mocaConn As RedPrairie.MOCA.Client.FullConnection, ByVal mCmd As String, logSQL As Boolean) As Dictionary(Of String, Dictionary(Of String, String))

        Dim resultsDictionary As Dictionary(Of String, String) = New Dictionary(Of String, String)
        Dim mocaResults As Dictionary(Of String, String) = Nothing
        Dim dictionaryList As New Dictionary(Of String, Dictionary(Of String, String))
        Dim mocaPerfomance As New Dictionary(Of String, String)
        Dim mCmdWithPrefix As String = "publish data where wh_id = 'WMD1' | " + mCmd

        resultsDictionary.Add("VOICE_MSG_COMMAND", mCmdWithPrefix)

        Try
            ' MMDA.dll setup            
            ' Dim comObj As RedPrairie.MOCA.Client.FullConnection = New RedPrairie.MOCA.Client.FullConnection

            ' MMDA.dll Return Object            
            Dim errorMsg As String = String.Empty
            Dim mocaExecResult As Integer = 9999

            ' Initialize Moca Server Connection
            ' comObj.Connect("WMD1", mocaServerIP, mocaServerPort, mocaServerEnv, False)
            ' comObj.Login("VOICE_SRV", "1234")

            ' Execute Moca Command    
            Dim DT As DataTable = New DataTable

            Dim st As Date = Now
            mocaExecResult = mocaConn.Execute(mCmdWithPrefix, DT)
            Dim et As Date = Now

            mocaPerfomance.Add("CMD", mCmdWithPrefix)
            mocaPerfomance.Add("ST", Format(st, "yyyyMMddHHmmssfff"))
            mocaPerfomance.Add("ET", Format(et, "yyyyMMddHHmmssfff"))
            mocaPerfomance.Add("RSL", mocaExecResult.ToString)

            resultsDictionary.Add("VOICE_CMD_RESULT", mocaExecResult.ToString)

            If mocaExecResult = 0 AndAlso DT.Rows.Count > 0 Then

                Dim lineCount As Integer = 1
                For Each row As DataRow In DT.Rows
                    mocaResults = New Dictionary(Of String, String)
                    For Each column As DataColumn In DT.Columns

                        mocaResults.Add(column.ToString, row(column).ToString)
                    Next

                    dictionaryList.Add("MOCA_" & lineCount, mocaResults)

                    lineCount += 1
                Next

                resultsDictionary.Add("LINES", lineCount - 1)
            Else

                resultsDictionary.Add("LINES", 0)
                dictionaryList.Add("MOCA_1", New Dictionary(Of String, String))
                resultsDictionary.Add("ERROR", errorMsg)
            End If

            Try
                DT = Nothing
            Catch ex As Exception
            End Try

            'Try
            'comObj.Close()
            'Catch ex As Exception
            'End Try

            'comObj = Nothing
        Catch ex As Exception
            Try
                resultsDictionary.Add("LINES", 0)
            Catch exx As Exception
            End Try

            Try
                dictionaryList.Add("MOCA_1", New Dictionary(Of String, String))
            Catch exx As Exception
            End Try

            Try
                resultsDictionary.Add("ERROR2", ex.ToString)
            Catch exx As Exception
            End Try
        End Try

        dictionaryList.Add("VOICE CMD RESULTS", resultsDictionary)
        dictionaryList.Add("MOCA_PERF", mocaPerfomance)

        'If logSQL Then
        'For Each widget As Object In dictionaryList.Item("VOICE CMD RESULTS")
        'logMessage(widget.Key & ": " & widget.Value, "RETURNDATA", False, "0", Format(Now, "yyyyMMddHHmmssfff"))
        'Next
        'End If

        Return dictionaryList
    End Function ' ProcessMocaCmdAndGetResults

    Public Sub logMessage(msg As String, logtyp As String, tstamp As String)
        logMessage(msg, logtyp, False, 0, tstamp)
    End Sub

    Public Function getValueFromDictionary(ByVal idx As String, ByRef inDictionary As Dictionary(Of String, String)) As String
        Dim retString As String = String.Empty

        If inDictionary.ContainsKey(idx) Then
            retString = inDictionary.Item(idx)
        Else
            retString = String.Empty
        End If

        Return retString
    End Function

    'Public Sub logMessage(msg As String, logtyp As String, xyz As Boolean, mres As String)
    '    Dim l As DBLogger = New DBLogger
    '    Dim t As New System.Threading.Thread(
    '                    AddressOf l.logIt)

    '    l.message = msg
    '    l.logType = logtyp
    '    l.xyz = xyz
    '    l.mRes = mres

    '    t.Start()
    'End Sub

    Public Sub logMessage(msg As String, logtyp As String, xyz As Boolean, mres As String, tstamp As String)
        msg = msg.Replace("'", "''")
        Dim qos As Queue(Of String) = New Queue(Of String)
        qos.Enqueue(logtyp)
        qos.Enqueue(msg)
        qos.Enqueue(mres)
        qos.Enqueue(tstamp)

        Dim tmcmd = autoMessageFormatToTemplate(mocaCmd, qos)

        Dim resDta As Dictionary(Of String, String) = New Dictionary(Of String, String)
        resDta = processMocaCmdAndGetResults(getMocaConn, tmcmd, xyz).Item("VOICE CMD RESULTS")
    End Sub ' logMessage

    Public Sub initProcess()
        ' Put the XML file in the installed dirctory c:\programfiles\ltbmmdaservice   
        Dim reader As XmlTextReader = New XmlTextReader(AppDomain.CurrentDomain.BaseDirectory + "LTBMMDAServiceInit.xml")

        Do While (reader.Read())
            Select Case reader.NodeType
                Case XmlNodeType.Element 'Display beginning of element.
                    If reader.Name = "moca-server-ip" Then
                        reader.Read()
                        mocaServerIP = reader.Value
                    ElseIf reader.Name = "moca-server-port" Then
                        reader.Read()
                        mocaServerPort = reader.Value
                    ElseIf reader.Name = "moca-server-env" Then
                        reader.Read()
                        mocaServerEnv = reader.Value                    
                    ElseIf reader.Name = "service-tag" Then
                        reader.Read()
                        serviceLocationGlobal = reader.Value
                    End If
            End Select
        Loop

        reader.Close()
    End Sub
End Module
