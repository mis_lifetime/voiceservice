﻿Imports VoiceMessageLibDEV

Public Module MessageParserFactory

    Public Enum messageParsers
        NextCartRequest
        DropCartRequest
        CancelRequest
        ItemStatus
        OperatorStatus
        CartStatus
        CartonStatus
        CancelStatus
        PickDownload
        Heartbeat
    End Enum

    Public Enum messageHandlers
        NextCartResponse
        DropCartResponse
        PickDownloadHandler
        CancelResponse
        CartStatusHandler
        ItemStatusHandler
        OperatorStatusHandler
        CartonStatusHandler
        CancelStatusHandler
        HeartbeatHandler
    End Enum

    Function getMessageParser(ByVal mp As messageParsers, ByVal msg As String) As PickDirectorMessageParser
        Return PickDirectorMessageParserFactory.getPickDirectorParser(msg)

        'Select Case mp
        '   Case messageParsers.NextCartRequest
        'Return New NextCartRequest(msg)
        '   Case messageParsers.CartStatus
        'Return New CartStatus(msg)
        '   Case Else
        'Return Nothing
        'End Select
    End Function

    Function getMessageHandler(ByVal mh As messageHandlers, ByVal pdmp As PickDirectorMessageParser) As PickDirectorMessageHandler
        Select Case mh
            Case messageHandlers.CartStatusHandler
                Return New CartStatusHandler(pdmp)
            Case messageHandlers.NextCartResponse
                Return New NextCartResponse(pdmp)
            Case messageHandlers.PickDownloadHandler
                Return New PickDownload(pdmp)
            Case Else
                Return Nothing
        End Select
    End Function
End Module
