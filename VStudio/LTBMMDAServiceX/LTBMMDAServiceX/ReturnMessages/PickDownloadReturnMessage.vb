﻿Imports VoiceMessageLib
Imports System.Text
Imports System.Threading
Imports System.Net.Sockets
Imports System.Net

Public Class PickDownloadReturnMessage
    Inherits ReturnMessage

    Private Function processMessage() As String
        Dim sendString As String = String.Empty

        If pickDirectorMessageHandler.sequenceNumber IsNot Nothing AndAlso Not pickDirectorMessageHandler.sequenceNumber.Equals(String.Empty) Then
            Return Nothing
        End If

        Try
            Dim md As Dictionary(Of String, Dictionary(Of String, String)) = Nothing
            Dim lines As Integer = 0

            md = pickDirectorMessageHandler.returnData

            Try
                lines = Integer.Parse(md.Item("VOICE CMD RESULTS").Item("LINES"))
            Catch ex As Exception
                EventLog.WriteEntry("LTBMMDAServiceSocket", ex.ToString, _
                                   EventLogEntryType.Error, 200, 200)
                lines = 0
            End Try

            If lines = 0 Then
                sendString = "ERROR: No Data"
            Else

                Dim mocaConn As MocaConnection = getNextMocaConn()
                Dim remoteEP As New IPEndPoint(commPoint.Address, commPoint.Port)

                ' Data buffer for incoming data.
                Dim bytes(bufferSize) As Byte

                ' Create a TCP/IP socket.
                Dim PDSender As New Socket(AddressFamily.InterNetwork, _
                    SocketType.Stream, ProtocolType.Tcp)

                PDSender.SendTimeout = 30000
                PDSender.Connect(remoteEP)

                For i As Integer = 1 To lines
                    Dim st As Date = Now
                    Dim idx As String = "MOCA_" & i.ToString
                    sendString = PickDownload.getReturnStringFromDictionary(pickDirectorMessageHandler.getMainTemplate, md.Item(idx))
                    Dim mocaCmd = PickDownload.getUpdateCommandFromDictionary(pickDirectorMessageHandler.getMainTemplate, md.Item(idx))
                    'logGenericAlertMessage("send(PickDownloadReturnMessage): " & mocaCmd, "(PickDownloadReturnMessage) " & pickDirectorMessageHandler.messageType, logLevel.HIGH)

                    Dim id As String = "N/A"

                    Try
                        id = md.Item(idx).Item("line_id")
                    Catch ex As Exception
                        logGenericAlertMessage("send(PickDownloadReturnMessage): " & ex.ToString, "(PickDownloadReturnMessage)-RTR", logLevel.CRITICAL)
                    End Try

                    'logGenericAlertMessage("send(PickDownloadReturnMessage): " & sendString, "(PickDownloadReturnMessage) " & pickDirectorMessageHandler.messageType, logLevel.HIGH)
                    'sendAndReceiveData(commPoint, sendString, False, pickDirectorMessageHandler.messageType)
                    Dim msg As Byte() = Encoding.ASCII.GetBytes(sendString)

                    ' Send the data through the socket.        
                    logGenericAlertMessage("sendAndReceiveData(=>" & commPoint.ToString & "): " & sendString, "SEND", logLevel.NORMAL)

                    Dim bytesSent As Integer = PDSender.Send(msg)

                    Try
                        mocaConn.execute(mocaCmd)
                    Catch mx As Exception
                        logGenericAlertMessage("send(PickDownloadReturnMessage): " & mx.ToString, "(PickDownloadReturnMessage)-MC", logLevel.CRITICAL)
                    End Try

                    Dim et As Date = Now

                    Try
                        logServiceMessage(rcpId, sendString, Format(st, "yyyyMMddHHmmssfff"), Format(et, "yyyyMMddHHmmssfff"), msgId, mocaCmd, "PICKDOWNLOADRM", _
                                  "PICKDOWNLOADRM", "", False, "PICKDOWNLOADRM")
                    Catch ex As Exception
                        logGenericAlertMessage("send(PickDownloadReturnMessage): " & ex.ToString, "(PickDownloadReturnMessage)-LG", logLevel.CRITICAL)
                    End Try

                    st = Nothing
                    et = Nothing
                    msgId += 1

                    'StatisticVarsModule.updateStatistic("PICKDOWNLOADLINES", id)

                    'notifyClients(sendString, "PICKDOWNLOAD-PRC")
                    Thread.Sleep(50)
                Next

                mocaConn.cleanUp()
                If mocaConn.isUnmanaged Then
                    mocaConn.closeConnection()
                    mocaConn = Nothing
                End If

                Try
                    PDSender.Shutdown(SocketShutdown.Both)
                    PDSender.Close()
                Catch ex As Exception
                    logGenericAlertMessage("send(PickDownloadReturnMessage): " & ex.ToString, "(PickDownloadReturnMessage)-SC", logLevel.CRITICAL)
                End Try

                sendString = "ACK: " & lines & " Lines Sent to Pick Director!"
            End If
        Catch ex As Exception
            EventLog.WriteEntry("LTBMMDAServiceSocket", ex.ToString, _
                               EventLogEntryType.Error, 200, 200)

            sendAndReceiveData(commPoint, sendString, False, "ERROR")
            sendString = ex.Message
        End Try

        Return sendString
    End Function

    Public Overrides Function getReturnMessage() As String
        Return processMessage()
    End Function

    Public Overrides Function getReturnMessageWithTimings() As ReturnMessageTimings

        rmTimings.returnMessage = processMessage()
        Return rmTimings
    End Function

    Sub New(ByVal pdmh As PickDirectorMessageHandler, mc As MocaConnection, r As ULong, m As ULong)
        MyBase.New(pdmh, mc)

        rcpId = r
        msgId = m
    End Sub
End Class

