﻿Imports VoiceMessageLibDEV

Public Class NextCartRequestTest
    Inherits BaseVoiceCommandTest

    Protected Overrides ReadOnly Property messageBodyTemplate As String
        Get
            Return "{""MT"":""{0}"",""Zone"":""{1}"",""OperatorID"":""{2}""}"
        End Get
    End Property

    Protected Overrides Property testName As String = "Next Cart Request Test"
    Protected Overrides Property testMessageType As String = "NEXTCARTREQUEST"
    Private Property messageBodyDictionary As Dictionary(Of String, String) = New Dictionary(Of String, String)
    Private Property mt As String
    Private Property zone As String
    Private Property operatorId As String

    Protected Overrides Function getMessageBodyDataParameters() As Queue(Of String)
        Dim q As Queue(Of String) = New Queue(Of String)
        q.Enqueue("NEXTCARTREQUEST")
        q.Enqueue(testData.Item("Area"))
        q.Enqueue("JOHNDOE")

        Return q
    End Function

    Protected Overrides Function validateTestData() As Dictionary(Of String, String)
        Dim testDataResults As Dictionary(Of String, String) = New Dictionary(Of String, String)

        If Not testData.ContainsKey("Area") Then
            testDataResults.Add("NO_ARG", "Missing TestData Parameter Area")
            Return testDataResults
        Else
            Return testDataResults
        End If
    End Function

    Sub New()
        MyBase.New(messageParsers.NextCartRequest, messageHandlers.NextCartResponse)
    End Sub
End Class
