﻿Public Class DropCartResponse
    Inherits PickDirectorMessageHandler

    Protected Overrides Function loadMessageBodyParameterQueue() As System.Collections.Generic.Queue(Of String)
        Dim msgQ As Queue(Of String) = New Queue(Of String)
        Dim aStr As String() = Nothing
        Dim cId As String = String.Empty
        Dim lId As String = String.Empty
        Dim vLId As String = String.Empty

        Try
            aStr = messageOutParameters.Item("msg_out").Split(",")
        Catch
        End Try

        Try
            cId = baseMessage.messageDictionary.Item("MB_CartID")
        Catch
        End Try

        Try
            lId = aStr(0)
        Catch
        End Try

        Try
            vLId = aStr(1)
        Catch
        End Try

        msgQ.Enqueue(messageType)
        msgQ.Enqueue(cId)
        msgQ.Enqueue(lId)
        msgQ.Enqueue(vLId)

        Return msgQ
    End Function

    Public Shared ReadOnly Property sharedMessageBodyTemplate As String
        Get
            Return "{""MT"":""{0}"",""CartID"":""{1}"",""DropLocation"":""{2}"",""VoiceDropLoc"":""{3}"",""SpecialHandling"":""{4}""}"
        End Get
    End Property

    Protected Overrides Function loadMessageBodyTemplate() As String
        Return sharedMessageBodyTemplate
    End Function

    Protected Overrides Function loadMocaCommand() As String
        Return "process usr voice command where exec_id = '{0}' and msg = '{1}'"
    End Function

    Protected Overrides Function loadMocaCommandParameters() As Queue(Of String)
        Dim queueOfParms As Queue(Of String) = New Queue(Of String)
        queueOfParms.Enqueue(baseMessage.messageDictionary.Item("MessageType"))
        queueOfParms.Enqueue(pickDirectorBaseMessage.getMocaMessageString)

        Return queueOfParms
    End Function

    Public Overrides ReadOnly Property messageType As String
        Get
            Return "DROPCARTRESPONSE"
        End Get
    End Property

    Sub New(ByVal msgIn As DelimitedMessageParser)
        MyBase.New(msgIn)
    End Sub
End Class
