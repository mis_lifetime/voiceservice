﻿Imports System.Net.Sockets
Imports System.Net
Imports System.Text
Imports System.Threading
Imports VoiceMessageLib

Public Class PickDirectorConnectionOrange
    Private Const loggerId As String = "ORANGE"
    Private isOrangeConnectedToPickDirector As Boolean = False
    Private pickDirectorOrangeConnectionThread As Thread = Nothing

    Private data As String = Nothing
    Private rcpId As ULong
    Private msgId As ULong
    Private tNameSeq As Integer = 1
    Private isOrangeRunning As Boolean = True
    Private handler As Socket
    Private pickDirectorOrange As Socket = Nothing
    Private pickDirectorOrangeEndPoint As IPEndPoint = Nothing
    'Private serviceSocketEndPoint As IPEndPoint = Nothing
    Private bytes(1024) As Byte
    Private messageQ As Queue(Of PickDirectorMessageParser)
    Private leftOverMessage As String = ""
    Private timers As Dictionary(Of String, Timer) = New Dictionary(Of String, Timer)
    'Private Shared timerMessages As Dictionary(Of String, PickDirectorDownloadTimers) = New Dictionary(Of String, PickDirectorDownloadTimers)

    Public Sub startSock()
        isOrangeRunning = True

        'notifyClients("PD Connection Starting . . .", "PDCONN.startSock(1)")
        logGenericAlertMessage("Starting Up PD Conn ORANGE", "STARTUP", logLevel.CRITICAL)

        ' signalSocketEvent(socketStates.Starting)
        isOrangeRunning = True

        If pickDirectorOrangeConnectionThread IsNot Nothing Then
            'notifyClients(pickDirectorConnectionThread.Name & ": Aborting . . .", "PDCONN.startup()")
            'logAlertMessage(pickDirectorConnectionThread.Name & ": Aborting . . .")
            Try
                pickDirectorOrangeConnectionThread.Abort()
            Catch ex As Exception
            End Try

            pickDirectorOrangeConnectionThread = Nothing
        End If

        If pickDirectorOrangeConnectionThread Is Nothing Then

            pickDirectorOrangeConnectionThread = New Thread(AddressOf workerSub)
            pickDirectorOrangeConnectionThread.Name = "PDCONNORANGE-" & tNameSeq

            'notifyClients(pickDirectorConnectionThread.Name & ": Starting . . .", "PDCONN.startup()")
            'logAlertMessage(pickDirectorConnectionThread.Name & ": Starting Up . . .", "INFO", logLevel.NORMAL)
        End If

        tNameSeq += 1
        pickDirectorOrangeConnectionThread.Start()
    End Sub

    Public Sub stopSock()
        isOrangeRunning = False

        logGenericAlertMessage("Shutting down PD Conn ORANGE", "SHUTDOWN", logLevel.CRITICAL)

        Try

            Try
                pickDirectorOrange.Close()

                logGenericAlertMessage("PD Conn ORANGE shut down", "SHUTDOWN", logLevel.CRITICAL)
            Catch ex As Exception
                logGenericAlertMessage("PDC Shutdown ORANGE Err: " & ex.ToString, "SHUTDOWN", logLevel.APPERROR)
            End Try
        Catch ex As Exception
            'notifyClients(ex.Message, "PDCONN.stopSock(2)")
            logGenericAlertMessage("PDC Shutdown Err2: " & ex.ToString, "WARN", logLevel.APPERROR)
            EventLog.WriteEntry("LTBMMDAServiceSocket", "Error Stopping PD ORANGE: " & ex.ToString, _
                           EventLogEntryType.Error, 300, 300)
        End Try
    End Sub

    'Public Shared Sub addPickDirectorDownloadTimer(ByVal nam As String, ByVal pdT As PickDirectorDownloadTimers)
    '    timerMessages.Add(nam, pdT)
    'End Sub

    'Public Shared Sub removePickDirectorDownloadTimer(ByVal kval As String)
    '    timerMessages.Remove(kval)
    'End Sub

    Private Sub workerSub()
        ' Data buffer for incoming data.

        ' Establish the local endpoint for the socket.
        ' Dns.GetHostName returns the name of the 
        ' host running the application.

        Try
            'Dim serviceSocketAddress As IPAddress = Nothing
            Dim pickDirectorAddress As IPAddress = Nothing


            IPAddress.TryParse(pickDirectorIP, pickDirectorAddress)
            'IPAddress.TryParse(serviceSocketIP, serviceSocketAddress)

            pickDirectorOrangeEndPoint = New IPEndPoint(pickDirectorAddress, pickDirectorPort)
            'serviceSocketEndPoint = New IPEndPoint(serviceSocketAddress, serviceSocketPort)

            pickDirectorOrange = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
            pickDirectorOrange.SendTimeout = 15000

            'notifyClients("Connecting to PD . . .", "PDCONN.workerSub(1)")            
            logGenericAlertMessage("ORANGE Connecting to PD", "PDCONNORANGE", logLevel.HIGH)

            ' Try to connect, if there is an issue re-try
            Do While Not connectToSocket()
                Thread.Sleep(retryInterval)
            Loop

            logGenericAlertMessage("ORANGE Connected to PD", "PDCONNORANGE", logLevel.HIGH)

            'notifyClients("Connected to PD . . .", "PDCONN.workerSub(2)")
            ' addPickDirectorDownloadTimer("PICKDOWNLOAD", New PDPickDownloadTimer(sender))
            If Not timers.ContainsKey("CHECKSOCKET") Then timers.Add("CHECKSOCKET", New Timer(AddressOf checkSocket, Nothing, 60000, 60000))
            messageQ = New Queue(Of PickDirectorMessageParser)

            Dim rcpTag As String = Nothing
            Dim rcpSDte As Date = Nothing
            Dim rcpEDte As Date = Nothing
            Dim msgNote As String = Nothing
            Dim idlSDte As Date = Nothing
            Dim idlEDte As Date = Nothing

            rcpId = 0
            msgId = 0

            While isOrangeRunning
                Try
                    'logAlertMessage("Waiting for PD Message", "PDCONN", logLevel.HIGH)

                    Dim bytesRec As Integer = pickDirectorOrange.Receive(bytes)
                    Dim retmsg = Encoding.ASCII.GetString(bytes, 0, bytesRec)

                    rcpId += 1
                    rcpSDte = Now
                    rcpTag = Format(rcpSDte, "yyyyMMdd-" & rcpId.ToString)

                    'notifyClients(retmsg, "PDCONN.workerSub(3)")
                    'logAlertMessage(rcpTag & ": " & retmsg, "PDCONN-RCV", logLevel.HIGH)

                    If leftOverMessage.Length > 0 Then
                        retmsg = leftOverMessage + retmsg
                        'logAlertMessage("Rcv(" & rcpId.ToString & "): " & leftOverMessage, "PDCONN-GLOM1", logLevel.HIGH)
                        'logAlertMessage("Rcv(" & rcpId.ToString & "): " & retmsg, "PDCONN-GLOM2", logLevel.HIGH)
                        leftOverMessage = ""
                    End If

                    handlMessageGlom(retmsg)
                    msgNote = "Messages in Packet: " & messageQ.Count.ToString
                    'logAlertMessage("Messages in Packet(" & rcpId.ToString & "): " & messageQ.Count.ToString, "PDCONN", logLevel.HIGH)

                    Dim sDte As Date = Nothing
                    Dim eDte As Date = Nothing
                    Dim ackSDte As Date = Nothing
                    Dim ackEDte As Date = Nothing
                    Dim retSDte As Date = Nothing
                    Dim retEDte As Date = Nothing
                    Dim msgTag As String = Nothing
                    Dim returnData As String = Nothing
                    Dim fastAckData As String = Nothing
                    Dim vMsg As PickDirectorMessageParser = Nothing

                    msgId = 0
                    For msgCnt As Integer = 1 To messageQ.Count
                        msgId += 1
                        vMsg = messageQ.Dequeue
                        sDte = Now
                        msgTag = Format(sDte, "yyyyMMdd-" & rcpId.ToString & "-" & msgId.ToString)

                        'logAlertMessage(msgTag & "(" & Format(sDte, "yyyyMMddHHmmssfff") & "):" & vMsg.baseMessage, "PDCONN->START", logLevel.HIGH)

                        If vMsg.requiresFastAck Then
                            ackSDte = Now
                            Dim ackConn As MocaConnection = getNextMocaConn()
                            fastAckData = SendMessageFactory.getFastAckMessage(vMsg, ackConn)
                            pickDirectorOrange.Send(Encoding.ASCII.GetBytes(fastAckData))
                            ackConn.cleanUp()
                            If ackConn.isUnmanaged Then
                                ackConn = Nothing
                            End If
                            ackEDte = Now
                        End If

                        retSDte = Now

                        Dim runConn As MocaConnection = getNextMocaConn()
                        Dim msgTimings As ReturnMessageTimings = SendMessageFactory.getSendMessageWithTimings(vMsg, runConn)

                        returnData = msgTimings.returnMessage
                        runConn.cleanUp()
                        If runConn.isUnmanaged Then
                            runConn = Nothing
                        End If

                        logServiceMessage(rcpId, msgTimings.mocaCmd, msgTimings.mocaCmdSt, msgTimings.mocaCmdEt, msgId, msgTimings.mocaCmdResult, "MOCA", "MOCA", _
                                            msgTimings.mocaCmdResult, True, loggerId)

                        If vMsg.requiresReturnMsg Then
                            pickDirectorOrange.Send(Encoding.ASCII.GetBytes(returnData))
                        End If
                        retEDte = Now

                        ' Dim dcsMsg As String = sendAndReceiveData(serviceSocketEndPoint, vMsg, True, "MAINSOCK")

                        'If dcsMsg <> String.Empty AndAlso dcsMsg IsNot Nothing Then

                        '    logAlertMessage("Sending to socket: " & dcsMsg, "PDCONN", logLevel.HIGH)

                        ' Send the data through the socket.                
                        '     pickDirector.Send(Encoding.ASCII.GetBytes(dcsMsg))
                        'End If

                        eDte = Now
                        'logAlertMessage(msgTag & "(" & Format(eDte, "yyyyMMddHHmmssfff") & "):" & vMsg.baseMessage, "PDCONN->STOP", logLevel.HIGH)

                        logServiceMessage(rcpId, vMsg.baseMessage, Format(sDte, "yyyyMMddHHmmssfff"), Format(eDte, "yyyyMMddHHmmssfff"), msgId, returnData, vMsg.messageType, _
                                            vMsg.messageType, fastAckData, True, loggerId)

                        sDte = Nothing
                        eDte = Nothing
                        ackSDte = Nothing
                        ackEDte = Nothing
                        retSDte = Nothing
                        retEDte = Nothing
                        vMsg = Nothing
                        msgTag = Nothing
                        returnData = Nothing
                        fastAckData = Nothing
                        msgTimings = Nothing
                        idlSDte = Nothing
                        idlEDte = Nothing
                    Next

                    rcpEDte = Now
                    logServiceMessage(rcpId, retmsg, Format(rcpSDte, "yyyyMMddHHmmssfff"), Format(rcpEDte, "yyyyMMddHHmmssfff"), 0, msgNote, "BASE", "BASE", "", True, loggerId)

                    retmsg = Nothing
                    rcpTag = Nothing
                    rcpSDte = Nothing
                    rcpEDte = Nothing
                    msgNote = Nothing
                Catch ex As Exception
                    If isOrangeRunning Then
                        'notifyClients(ex.Message, "PDCONN.workerSub(4)")
                        EventLog.WriteEntry("LTBMMDAServiceSocket", ex.ToString, _
                                       EventLogEntryType.Error, 300, 300)

                        Try
                            'notifyClients("PD Connection Shutting Down . . .", "PDCONN.workerSub(5)")
                            stopSock()
                        Catch exc As Exception
                            'notifyClients(exc.Message, "PDCONN.workerSub(6)")
                            EventLog.WriteEntry("LTBMMDAServiceSocket", "STOPERR: " & exc.ToString, _
                                   EventLogEntryType.Error, 300, 300)
                        End Try

                        pickDirectorDisconnects += 1
                        lastPickDirectorDisconnect = Date.Now
                        Thread.Sleep(1000)

                        Try
                            'notifyClients("PD Connection Starting Up . . .", "PDCONN.workerSub(7)")
                            startSock()
                        Catch exc As Exception
                            'notifyClients(exc.Message, "PDCONN.workerSub(8)")
                            EventLog.WriteEntry("LTBMMDAServiceSocket", "STARTERR: " & exc.ToString, _
                                   EventLogEntryType.Error, 300, 300)
                        End Try
                    End If
                End Try
            End While

            'notifyClients("Process Stopped . . .", "PDCONN.workerSub(10)")
        Catch mainEx As Exception
            'notifyClients(mainEx.Message, "PDCONN.workerSub(9)")
            EventLog.WriteEntry("LTBMMDAServiceSocket", "RUNSOCK: " & mainEx.ToString, _
                                   EventLogEntryType.Error, 300, 300)
        End Try
    End Sub

    Private Sub handlMessageGlom(ByVal message As String)
        Dim noMoreChunks As Boolean = False

        Do
            If message.StartsWith(Chr(2)) Then
                If message.Contains(Chr(3)) Then
                    Dim termPos As Integer = message.IndexOf(Chr(3))
                    If message.Substring(0, termPos + 1).StartsWith(Chr(2)) And message.Substring(0, termPos + 1).EndsWith(Chr(3)) Then
                        Try
                            messageQ.Enqueue(PickDirectorMessageParserFactory.getPickDirectorParser(message.Substring(0, termPos + 1)))
                        Catch mqEx As Exception
                            logGenericAlertMessage("Bad MsgEvQ([]): " & message.Substring(0, termPos + 1), "ENQUE-ERR", logLevel.HIGH)
                        End Try
                    Else
                        logGenericAlertMessage("Bad Msg([]): " & message.Substring(0, termPos + 1), "UNGLOM", logLevel.HIGH)
                    End If

                    message = message.Substring(termPos + 1)
                    If message.Length = 0 Then
                        noMoreChunks = True
                    End If
                Else
                    noMoreChunks = True
                End If
            Else
                noMoreChunks = True
            End If
        Loop Until noMoreChunks

        If message.Length > 0 Then
            leftOverMessage = message
            'logAlertMessage("LoM: " & leftOverMessage, "PDCONN-LoM", logLevel.HIGH)
            message = Nothing
        End If
    End Sub

    Private Function checkSocket(ByVal state As Object) As Boolean
        Dim isConnected As Boolean = True

        Try
            If pickDirectorOrange.Poll(0, SelectMode.SelectRead) Then
                Dim byteArray As Byte() = New Byte(1) {}
                If (pickDirectorOrange.Receive(byteArray, SocketFlags.Peek)) = 0 Then isConnected = False
            End If
        Catch ex As Exception
            logGenericAlertMessage(ex.ToString, "CHECKSOCKORANGE", logLevel.APPERROR)
            'notifyClients(ex.Message, "PDCONN.checkSocket(1)")

            isConnected = False
        End Try

        'notifyClients("CHECKSOCKET: " & isConnected, "PDCONN.checkSocket(2)")
        'logAlertMessage("CHECKSOCKET: " & isConnected, "CHECKSOCKET", logLevel.NORMAL)

        Return isConnected
    End Function

    Private Function connectToSocket() As Boolean

        isOrangeConnectedToPickDirector = False

        Try
            pickDirectorOrange.Connect(pickDirectorOrangeEndPoint)

            'logAlertMessage("connectToSocket(): " & filterMainSocket, "PICKDCONNFILTERSENT")
            pickDirectorOrange.Send(Encoding.ASCII.GetBytes(filterOrange))

            'logAlertMessage("Connected to PD", "PDCONN", logLevel.HIGH)

            pickDirectorConnectTime = Date.Now
            isOrangeConnectedToPickDirector = True

            Return True
        Catch ex As Exception
            'notifyClients(ex.Message, "PDCONN.connectToSocket(1)")
            logGenericAlertMessage(ex.ToString, "ORANGE-ERR", logLevel.APPERROR)
            EventLog.WriteEntry("LTBMMDAServiceSocket", "Unable to connect to pick director: " & ex.Message, _
                           EventLogEntryType.Error, 300, 300)

            pickDirectorDisconnects += 1
            lastPickDirectorDisconnect = Date.Now
            isOrangeConnectedToPickDirector = False
            Return False
        End Try
    End Function
End Class
