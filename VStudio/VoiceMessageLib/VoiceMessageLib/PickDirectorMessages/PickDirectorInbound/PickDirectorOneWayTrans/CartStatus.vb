﻿Public Class CartStatus
    Inherits PickDirectorMessageParser

    Protected Overrides Function loadMessageBodyKeys() As String()
        Dim mbk As String() = {"MT", "CartID", "Date", "Time", "OperatorID", "Type", "DropOffLocation", "LineCount"}
        Return mbk
    End Function

    Public Overrides Property messageType As String = "CARTSTATUS"

    Sub New(ByVal msg As String)
        MyBase.New(msg)
    End Sub
End Class