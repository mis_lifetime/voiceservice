﻿Imports System.Text
Imports System.Net.Sockets
Imports System.Threading

Module VoiceTestGlobals
    ' PickDirector Connection Variables
    Public retryInterval As Integer = 180000
    Public serviceIP As String = "172.16.1.247"
    Public pickDirectorIP As String = "172.16.1.247"
    Public servicePort As Integer = 11000
    Public pickDirectorPort As Integer = 11100
    Public filterMessage As String = Chr(2) & "PL^PT^^MessageDistributor^$filter$^^PT,'VC_VOICE',=" & Chr(3)
    Public pickDirectorConnectionThread As Thread = Nothing

    ' constant message delimiters and characters
    Public Const messageDelimiter As Char = "^"
    Public Const messageDataDelimiter As Char = ","
    Public Const stxVal As String = Chr(2)
    Public Const etxVal As String = Chr(3)

    ' Event Log Constants
    Public Const EVT_SOURCE As String = "LTBMMDAServiceSocket"
    Public Const EVT_LOG As String = "Application"
    Public Const EVT_EVENT As String = "Socket State Event"
    Public Const EVT_MACHINE As String = "."
    Public eventData As New EventSourceCreationData(EVT_SOURCE, EVT_LOG)
    Public LTBMMDAEventLog As EventLog = New EventLog(EVT_LOG, EVT_MACHINE, EVT_SOURCE)

    Public Const genericMocaCommand = "[select 1 val from dual]"

    ' Thread States
    Public Enum threadStates
        Started
        Stopped
    End Enum

    ' Socket State Enumeration
    Public Enum socketStates
        Stopping
        Stopped
        Starting
        Started
        Blocking
        Receiving
        Received
        Sending
        ConnectingMOCA
        ReceivingMOCA
        WaitingForEOF
        Borked
        Other
    End Enum

    Public Function sendAndReceiveData(ByVal cAddr As Net.IPEndPoint, ByVal msgData As String, ByVal reqRespFlg As Boolean) As String
        ' Establish the local endpoint for the socket.                        
        Dim remoteEP As New Net.IPEndPoint(cAddr.Address, cAddr.Port)

        ' Data buffer for incoming data.
        Dim bytes(1024) As Byte

        ' Create a TCP/IP socket.
        Dim sender As New Socket(AddressFamily.InterNetwork, _
            SocketType.Stream, ProtocolType.Tcp)

        If msgData = String.Empty Then Return "Error: No Data To Send"

        'LTBMMDAEventLog.WriteEntry("Connecting to " & cAddr.ToString & " : " & cAddr.Port)

        sender.SendTimeout = 10000
        sender.ReceiveTimeout = 10000

        Try
            ' Connect the socket to the remote endpoint.
            sender.Connect(remoteEP)
        Catch ex As Exception
            'LTBMMDAEventLog.WriteEntry("Unable to Connect to Client: " & ex.ToString)
            Return "ERROR: Unable to Connect to Client at " & cAddr.ToString & " : " & cAddr.Port
        End Try

        ' Encode the data string into a byte array.
        Dim msg As Byte() = _
            Encoding.ASCII.GetBytes(msgData)

        ' Send the data through the socket.
        'LTBMMDAEventLog.WriteEntry("Sending: " & msgData)
        Dim bytesSent As Integer = sender.Send(msg)

        Dim retmsg As String = String.Empty

        If reqRespFlg Then
            Try
                ' Receive the response from the remote device.
                Dim bytesRec As Integer = sender.Receive(bytes)
                retmsg = Encoding.ASCII.GetString(bytes, 0, bytesRec)
            Catch ex As Exception
                retmsg = "Receive Timeout"
            End Try
        End If

        'LTBMMDAEventLog.WriteEntry("Received: " & retmsg)

        ' Release the socket.
        sender.Shutdown(SocketShutdown.Both)
        sender.Close()

        Return retmsg
    End Function
End Module
