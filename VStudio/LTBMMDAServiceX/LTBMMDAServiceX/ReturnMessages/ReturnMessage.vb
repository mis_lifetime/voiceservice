﻿Imports VoiceMessageLib
Imports System.Net

Public MustInherit Class ReturnMessage

    Public Property pickDirectorMessageHandler As PickDirectorMessageHandler
    Public Property commPoint As IPEndPoint
    Public Property mocaConnection As MocaConnection
    Public Property rmTimings As ReturnMessageTimings
    Public Property rcpId As ULong
    Public Property msgId As ULong

    Public Overridable Function getReturnMessage() As String

        Return pickDirectorMessageHandler.returnMessage
    End Function

    Public Overridable Function getReturnMessageWithTimings() As ReturnMessageTimings

        Return rmTimings
    End Function

    Public Sub sendFastAckMessage()

        sendAndReceiveData(commPoint, getReturnMessage, False, pickDirectorMessageHandler.messageType)        
    End Sub

    Sub New(ByVal pdmh As PickDirectorMessageHandler, mc As MocaConnection, Optional ByVal fastAck As Boolean = False)
        mocaConnection = mc
        pickDirectorMessageHandler = pdmh
        pickDirectorMessageHandler.processMessage(mc.getMocaConnection, fastAck)

        rmTimings = New ReturnMessageTimings(pdmh.mocaCmdUsed, pdmh.mocaCmdStartTime, pdmh.mocaCmdEndTime, pdmh.mocaCmdResult, pdmh.returnMessage)

        Dim cAddr As IPAddress = Nothing

        Dim okIp As Boolean = IPAddress.TryParse(pickDirectorIP, cAddr)
        commPoint = New IPEndPoint(cAddr, pickDirectorPort)
    End Sub
End Class
