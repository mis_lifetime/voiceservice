﻿Public Interface DelimitedMessageParser

    Property baseMessage() As String
    ReadOnly Property delimiter() As String
    Property messageKeys() As String()
    Property messageDictionary() As Dictionary(Of String, String)

    Function loadMessageKeys() As String()
    Sub parseMessage()
End Interface
