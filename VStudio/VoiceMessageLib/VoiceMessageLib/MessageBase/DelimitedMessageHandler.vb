﻿Public Interface DelimitedMessageHandler
    Property messageFormat() As String
    Property delimiter() As String
    Property baseMessage() As DelimitedMessageParser
    Property messageOutParameters() As Dictionary(Of String, String)
End Interface