﻿Module Module1

    Sub Main()
        validateOrdKeys()
    End Sub

    Public Sub validateOrdKeys()
        Dim rs As Dictionary(Of String, Dictionary(Of String, String)) = New Dictionary(Of String, Dictionary(Of String, String))
        Dim newProdConnn As RedPrairie.MOCA.Client.FullConnection = getNewProdMocaConnection()
        rs = processMocaCmdAndGetResults(newProdConnn, "[select ordnum from ord where rownum < 2", False)

        For Each kv As String In rs.Keys
            Console.WriteLine(kv)
            Dim srs As Dictionary(Of String, String) = rs.Item(kv)
            For Each skv As String In srs.Keys
                Console.WriteLine(skv + " : " + srs.Item(skv))
            Next
        Next

        Console.WriteLine("Done . . .")
        Console.ReadLine()
    End Sub

    Public Sub testConnection()
        Dim rs As Dictionary(Of String, Dictionary(Of String, String)) = New Dictionary(Of String, Dictionary(Of String, String))

        Console.WriteLine("Connecting . . .")
        Dim mocaConnection As RedPrairie.MOCA.Client.FullConnection = getMocaConn()
        Console.WriteLine("Connected . . .")

        rs = processMocaCmdAndGetResults(mocaConnection, "publish data where wh_id = 'WMD1' | get usr cart info for voice where cartid = 'DTCGO100'", False)
        'rs = processMocaCmdAndGetResults(mocaConnection, "[select * from DEVF.LES_USR_ATH where usr_id like 'VOI%']", False)
        For Each kv As String In rs.Keys
            Console.WriteLine(kv)
            Dim srs As Dictionary(Of String, String) = rs.Item(kv)
            For Each skv As String In srs.keys
                Console.WriteLine(skv + " : " + srs.Item(skv))
            Next
        Next

        Console.WriteLine("Done . . .")
        Console.ReadLine()
    End Sub

    Public Function getNewProdMocaConnection() As RedPrairie.MOCA.Client.FullConnection
        Dim comObj As RedPrairie.MOCA.Client.FullConnection = New RedPrairie.MOCA.Client.FullConnection
        Dim mocaServerIP As String = "172.16.1.43"
        Dim mocaServerPort As Short = 4600
        Dim mocaServerEnv As String = "dev"
        comObj.Connect("WMD1", mocaServerIP, mocaServerPort, mocaServerEnv, False)

        Return comObj
    End Function

    Public Sub connectMocaConn(ByRef mc As RedPrairie.MOCA.Client.FullConnection)
        Dim mocaServerIP As String = "172.16.1.133"
        Dim mocaServerPort As Short = 4250
        Dim mocaServerEnv As String = "dev"
        mc.Connect("WMD1", mocaServerIP, mocaServerPort, mocaServerEnv, False)
        mc.Login("VOICE_SRV", "1234")       
    End Sub

    Public Function getMocaConn() As RedPrairie.MOCA.Client.FullConnection
        Dim comObj As RedPrairie.MOCA.Client.FullConnection = New RedPrairie.MOCA.Client.FullConnection

        connectMocaConn(comObj)

        Return comObj
    End Function

    Public Function processMocaCmdAndGetResults(mocaConn As RedPrairie.MOCA.Client.FullConnection, ByVal mCmd As String, logSQL As Boolean) As Dictionary(Of String, Dictionary(Of String, String))

        Dim resultsDictionary As Dictionary(Of String, String) = New Dictionary(Of String, String)
        Dim mocaResults As Dictionary(Of String, String) = Nothing
        Dim dictionaryList As New Dictionary(Of String, Dictionary(Of String, String))
        Dim mocaPerfomance As New Dictionary(Of String, String)

        resultsDictionary.Add("VOICE_MSG_COMMAND", mCmd)

        Try
            ' MMDA.dll setup            
            ' Dim comObj As RedPrairie.MOCA.Client.FullConnection = New RedPrairie.MOCA.Client.FullConnection

            ' MMDA.dll Return Object            
            Dim errorMsg As String = String.Empty
            Dim mocaExecResult As Integer = 9999

            ' Initialize Moca Server Connection
            ' comObj.Connect("WMD1", mocaServerIP, mocaServerPort, mocaServerEnv, False)
            ' comObj.Login("VOICE_SRV", "1234")

            ' Execute Moca Command    
            Dim DT As DataTable = New DataTable

            Dim st As Date = Now
            mocaExecResult = mocaConn.Execute(mCmd, DT)
            Dim et As Date = Now

            mocaPerfomance.Add("CMD", mCmd)
            mocaPerfomance.Add("ST", Format(st, "yyyyMMddHHmmssfff"))
            mocaPerfomance.Add("ET", Format(et, "yyyyMMddHHmmssfff"))
            mocaPerfomance.Add("RSL", mocaExecResult.ToString)

            resultsDictionary.Add("VOICE_CMD_RESULT", mocaExecResult.ToString)

            If mocaExecResult = 0 AndAlso DT.Rows.Count > 0 Then

                Dim lineCount As Integer = 1
                For Each row As DataRow In DT.Rows
                    mocaResults = New Dictionary(Of String, String)
                    For Each column As DataColumn In DT.Columns

                        mocaResults.Add(column.ToString, row(column).ToString)
                    Next

                    dictionaryList.Add("MOCA_" & lineCount, mocaResults)

                    lineCount += 1
                Next

                resultsDictionary.Add("LINES", lineCount - 1)
            Else

                resultsDictionary.Add("LINES", 0)
                dictionaryList.Add("MOCA_1", New Dictionary(Of String, String))
                resultsDictionary.Add("ERROR", errorMsg)
            End If

            Try
                DT = Nothing
            Catch ex As Exception
            End Try

            'Try
            'comObj.Close()
            'Catch ex As Exception
            'End Try

            'comObj = Nothing
        Catch ex As Exception
            Try
                resultsDictionary.Add("LINES", 0)
            Catch exx As Exception
            End Try

            Try
                dictionaryList.Add("MOCA_1", New Dictionary(Of String, String))
            Catch exx As Exception
            End Try

            Try
                resultsDictionary.Add("ERROR2", ex.ToString)
            Catch exx As Exception
            End Try
        End Try

        dictionaryList.Add("VOICE CMD RESULTS", resultsDictionary)
        dictionaryList.Add("MOCA_PERF", mocaPerfomance)

        'If logSQL Then
        'For Each widget As Object In dictionaryList.Item("VOICE CMD RESULTS")
        'logMessage(widget.Key & ": " & widget.Value, "RETURNDATA", False, "0", Format(Now, "yyyyMMddHHmmssfff"))
        'Next
        'End If

        Return dictionaryList
    End Function ' ProcessMocaCmdAndGetResults
End Module
