﻿Module StatisticVarsModule
    ' statistical categories

    ' statistical variables
    Public statsInterval As Long = 900000
    Public serverStartTime As Date
    Public pickDirectorConnectTime As Date
    Public pickDirectorDisconnects As Integer
    Public lastPickDirectorDisconnect As Date
    Public lastMessageType As String
    Public lastMessageTime As Date
    Public totalMessages As Integer
    Public totalHeartBeats As Integer
    Public lastHeartbeat As Date
    Public totalCartRequests As Integer
    Public lastCartRequest As Date
    Public lastCartRequestZone As String
    Public lastCartRequestOperator As String
    Public totalCartResponses As Integer
    Public lastCartResponse As Date
    Public lastCartResponseId As String
    Public totalPickDownloadRequests As Integer
    Public lastPickDownloadRequest As Date
    Public lastPickDownloadRequestId As String
    Public totalCancelRequests As String
    Public lastCancelRequest As Date
    Public lastCancelId As String
    Public lastCancelType As String
    Public totalDropCartRequests As Integer
    Public lastDropCartRequest As Date
    Public lastDropCartID As String
    Public totalDropCartResponse As Integer
    Public lastDropCartResponse As Date
    Public lastDropCartResponseId As String
    Public totalItemStatus As Integer
    Public lastItemStatus As Date
    Public lastItemStatusId As String
    Public lastItemStatusState As String
    Public totalCartonStatus As Integer
    Public lastCartonStatus As Date
    Public lastCartonStatusId As String
    Public lastCartonStatusState As String
    Public totalCancelStatus As Integer
    Public lastCancelStatus As Date
    Public lastCancelStatusId As String
    Public lastCancelStatusState As String
    Public totalCartStatus As Integer
    Public lastCartStatus As Date
    Public lastCartStatusId As String
    Public lastCartStatusState As String
    Public totalOperatorStatus As Integer
    Public lastOperatorStatus As Date
    Public lastOperatorStatusId As String
    Public lastOperatorStatusState As String
    Public totalDropCartScans As Integer
    Public lastDropCartScan As Date
    Public lastDropCartScanId As String
    Public totalUnknownMessages As Integer
    Public lastUnknownMessage As Date
    Public lastUnknownType As String
    Public totalPickDownloadLines As Integer
    Public lastPickDownloadLine As Date
    Public lastPickDownloadLineId As String

    Public Sub updateStatistic(ByVal mp As VoiceMessageLibDEV.PickDirectorMessageParser)
        Try
            logGenericAlertMessage("UpdatingStats for " & mp.messageType, "STATS")
            Dim kv1 As String = "N/A"
            Dim kv2 As String = "N/A"
            Dim mt As String = mp.messageType

            Dim d As Dictionary(Of String, String) = mp.messageBodyDictionary

            Select Case mt
                Case "DROPCARTSCAN"
                    kv1 = getDictionaryItem(d, "CartID")
                    updateStatistic(mt, kv1)
                Case "HEARTBEAT"
                    updateStatistic(mt)
                Case "NEXTCARTREQUEST"
                    kv1 = getDictionaryItem(d, "Zone")
                    kv2 = getDictionaryItem(d, "OperatorID")
                    updateStatistic(mt, kv1, kv2)
                Case "NEXTCARTRESPONSE"
                    kv1 = getDictionaryItem(d, "CartID")
                    updateStatistic(mt, kv1)
                Case "PICKDOWNLOAD"
                    kv1 = getDictionaryItem(d, "CartID")
                    updateStatistic(mt, kv1)
                Case "CANCELREQUEST"
                    kv1 = getDictionaryItem(d, "CancelID")
                    kv2 = getDictionaryItem(d, "CancelType")
                    updateStatistic(mt, kv1, kv2)
                Case "DROPCARTREQUEST"
                    kv1 = getDictionaryItem(d, "CartID")
                    updateStatistic(mt, kv1)
                Case "ITEMSTATUS"
                    kv1 = getDictionaryItem(d, "LineID")
                    kv2 = getDictionaryItem(d, "State")
                    updateStatistic(mt, kv1, kv2)
                Case "CARTONSTATUS"
                    kv1 = getDictionaryItem(d, "OrderID")
                    kv2 = getDictionaryItem(d, "Status")
                    updateStatistic(mt, kv1, kv2)
                Case "CARTSTATUS"
                    kv1 = getDictionaryItem(d, "CartID")
                    kv2 = getDictionaryItem(d, "Type")
                    updateStatistic(mt, kv1, kv2)
                Case "OPERATORSTATUS"
                    kv1 = getDictionaryItem(d, "OperatorID")
                    kv2 = getDictionaryItem(d, "Status")
                    updateStatistic(mt, kv1, kv2)
                Case "CANCELSTATUS"
                    kv1 = getDictionaryItem(d, "DeleteID")
                    kv2 = getDictionaryItem(d, "State")
                    updateStatistic(mt, kv1, kv2)
                Case Else
                    totalUnknownMessages += 1
                    lastUnknownMessage = Date.Now
                    lastUnknownType = mt
            End Select

            totalMessages += 1
            lastMessageTime = Date.Now
            lastMessageType = mt
        Catch ex As Exception
            logGenericAlertMessage("Stat Error1: " & ex.ToString, "ERROR", logLevel.APPERROR)
            'notifyClients(ex.Message, "STATS")
        End Try
    End Sub

    Public Sub updateStatistic(ByVal msgTyp As String, Optional idOne As String = "N/A", Optional idTwo As String = "N/A")

        logGenericAlertMessage("UpdatingStats for " & msgTyp & ":" & idOne & ":" & idTwo, "STATS")
        Try
            Select Case msgTyp
                Case "DROPCARTSCAN"
                    totalDropCartScans += 1
                    lastDropCartScan = Date.Now
                    lastDropCartScanId = idOne
                Case "HEARTBEAT"
                    totalHeartBeats += 1
                    lastHeartbeat = Date.Now
                Case "NEXTCARTREQUEST"
                    totalCartRequests += 1
                    lastCartRequest = Date.Now
                    lastCartRequestZone = idOne
                    lastCartRequestOperator = idTwo
                Case "NEXTCARTRESPONSE"
                    totalCartResponses += 1
                    lastCartResponse = Date.Now
                    lastCartResponseId = idOne
                Case "PICKDOWNLOAD"
                    totalPickDownloadRequests += 1
                    lastPickDownloadRequest = Date.Now
                    lastPickDownloadRequestId = idOne
                Case "PICKDOWNLOADLINES"
                    totalPickDownloadLines += 1
                    lastPickDownloadLine = Date.Now
                    lastPickDownloadLineId = idOne
                    totalMessages += 1
                    lastMessageTime = Date.Now
                    lastMessageType = msgTyp
                Case "CANCELREQUEST"
                    totalCancelRequests += 1
                    lastCancelRequest = Date.Now
                    lastCancelId = idOne
                    lastCancelType = idTwo
                Case "CANCELSTATUS"
                    totalCancelStatus += 1
                    lastCancelStatus = Date.Now
                    lastCancelStatusId = idOne
                    lastCancelStatusState = idTwo
                    totalMessages += 1
                    lastMessageTime = Date.Now
                    lastMessageType = msgTyp
                Case "DROPCARTREQUEST"
                    totalDropCartRequests += 1
                    lastDropCartRequest = Date.Now
                    lastDropCartID = idOne
                Case "ITEMSTATUS"
                    totalItemStatus += 1
                    lastItemStatus = Date.Now
                    lastItemStatusId = idOne
                    lastItemStatusState = idTwo
                Case "CARTONSTATUS"
                    totalCartonStatus += 1
                    lastCartonStatus = Date.Now
                    lastCartonStatusId = idOne
                    lastCartonStatusState = idTwo
                Case "CARTSTATUS"
                    totalCartStatus += 1
                    lastCartStatus = Date.Now
                    lastCartStatusId = idOne
                    lastCartStatusState = idTwo
                Case "OPERATORSTATUS"
                    totalOperatorStatus += 1
                    lastOperatorStatus = Date.Now
                    lastOperatorStatusId = idOne
                    lastOperatorStatusState = idTwo
                Case "CANCELSTATUS"
                    totalCancelStatus += 1
                    lastCancelStatus = Date.Now
                    lastCancelStatusId = idOne
                    lastCancelStatusState = idTwo
                Case Else
                    totalUnknownMessages += 1
                    lastUnknownMessage = Date.Now
                    lastUnknownType = msgTyp
            End Select
        Catch ex As Exception
            'notifyClients(ex.Message, "STATS")
            logGenericAlertMessage("Stat Error2: " & ex.ToString, "STATSERROR", logLevel.APPERROR)
        End Try
    End Sub

    Private Function getDictionaryItem(ByRef d As Dictionary(Of String, String), ByVal kv As String) As String
        Dim s As String = String.Empty

        Try
            s = d.Item(kv)
        Catch ex As Exception
            s = kv & " NOT FOUND"
        End Try

        Return s
    End Function

    Public Function getStatistics() As Dictionary(Of String, String)
        Dim d As Dictionary(Of String, String) = New Dictionary(Of String, String)

        Select Case socketLogMode
            Case logMode.ALL
                d.Add("LogMode", "All")
            Case logMode.DCSONLY
                d.Add("LogMode", "DCSOnly")
            Case logMode.EVENTONLY
                d.Add("LogMode", "EventLogOnly")
            Case logMode.SILENT
                d.Add("LogMode", "Silent")
        End Select

        d.Add("StatsInterval", statsInterval)
        d.Add("RunTime", Format(serverStartTime, "M/d/yyyy HH:mm:ss:fff"))        
        d.Add("PickDirectorConnectTime", Format(pickDirectorConnectTime, "M/d/yyyy HH:mm:ss:fff"))
        d.Add("PickDirectorDisconnects", pickDirectorDisconnects)
        d.Add("LastPickDirectorDisconnect", Format(lastPickDirectorDisconnect, "M/d/yyyy HH:mm:ss:fff"))
        d.Add("TotalMessages", totalMessages)
        d.Add("LastMessage", Format(lastMessageTime, "M/d/yyyy HH:mm:ss:fff"))
        d.Add("LastMessageType", lastMessageType)
        d.Add("TotalDropCartScans", totalDropCartScans)
        d.Add("LastDropCartScan", Format(lastDropCartScan, "M/d/yyyy HH:mm:ss:fff"))
        d.Add("LastDropCartScanId", lastDropCartScanId)
        d.Add("TotalHeartBeats", totalHeartBeats)
        d.Add("LastHeartBeat", Format(lastHeartbeat, "M/d/yyyy HH:mm:ss:fff"))
        d.Add("TotalNextCartRequests", totalCartRequests)
        d.Add("LastNextCartRequest", Format(lastCartRequest, "M/d/yyyy HH:mm:ss:fff"))
        d.Add("LastCartRequestZone", lastCartRequestZone)
        d.Add("LastCartRequestOperator", lastCartRequestOperator)
        d.Add("TotalNextCartResponse", totalCartResponses)
        d.Add("LastNextCartResponse", Format(lastCartResponse, "M/d/yyyy HH:mm:ss:fff"))
        d.Add("LastNextCartResponseId", lastCartResponseId)
        d.Add("TotalPickDownloads", totalPickDownloadRequests)
        d.Add("LastPickDownload", Format(lastPickDownloadRequest, "M/d/yyyy HH:mm:ss:fff"))
        d.Add("LastPickDownloadId", lastPickDownloadRequestId)
        d.Add("TotalCancelRequests", totalCancelRequests)
        d.Add("LastCancelRequest", Format(lastCancelRequest, "M/d/yyyy HH:mm:ss:fff"))
        d.Add("LastCancelId", lastCancelId)
        d.Add("LastCancelType", lastCancelType)
        d.Add("TotalCancelStatus", totalCancelStatus)
        d.Add("LastCancelStatus", Format(lastCancelStatus, "M/d/yyyy HH:mm:ss:fff"))
        d.Add("LastCancelStatusId", lastCancelStatusId)
        d.Add("LastCancelStatusState", lastCancelStatusState)
        d.Add("TotalDropCartRequests", totalDropCartRequests)
        d.Add("LastDropCartRequest", Format(lastDropCartRequest, "M/d/yyyy HH:mm:ss:fff"))
        d.Add("LastDropCartID", lastDropCartID)
        d.Add("TotalItemStatus", totalItemStatus)
        d.Add("LastItemStatus", Format(lastItemStatus, "M/d/yyyy HH:mm:ss:fff"))
        d.Add("LastItemStatusId", lastItemStatusId)
        d.Add("LastItemStatusState", lastItemStatusState)
        d.Add("TotalCartonStatus", totalCartonStatus)
        d.Add("LastCartonStatus", Format(lastCartonStatus, "M/d/yyyy HH:mm:ss:fff"))
        d.Add("LastCartonStatusId", lastCartonStatusId)
        d.Add("LastCartonStatusState", lastCartonStatusState)
        d.Add("TotalCartStatus", totalCartonStatus)
        d.Add("LastCartStatus", Format(lastCartStatus, "M/d/yyyy HH:mm:ss:fff"))
        d.Add("LastCartStatusId", lastCartStatusId)
        d.Add("LastCartStatusState", lastCartStatusState)
        d.Add("TotalOperatorStatus", totalOperatorStatus)
        d.Add("LastOperatorStatus", Format(lastOperatorStatus, "M/d/yyyy HH:mm:ss:fff"))
        d.Add("LastOperatorStatusId", lastOperatorStatusId)
        d.Add("LastOperatorStatusState", lastOperatorStatusState)
        d.Add("TotalUnknownMessages", totalUnknownMessages)
        d.Add("LastUnknownMessage", Format(lastUnknownMessage, "M/d/yyyy HH:mm:ss:fff"))
        d.Add("LastUnknownType", lastUnknownType)
        d.Add("PickDownloadLines", totalPickDownloadLines)
        d.Add("LastPickDownloadLine", lastPickDownloadLine)
        d.Add("LastPickDownloadLineId", lastPickDownloadLineId)      

        Return d
    End Function
End Module