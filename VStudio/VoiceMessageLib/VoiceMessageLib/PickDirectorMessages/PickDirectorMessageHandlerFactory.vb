﻿Public Class PickDirectorMessageHandlerFactory
    Inherits PickDirectorMessageHandler

    Public Shared Function getPickDirectorMessageHandler(ByVal msgParser As PickDirectorMessageParser) As PickDirectorMessageHandler
        Dim pickDirectorMessageHandler As PickDirectorMessageHandler = New PickDirectorMessageHandlerFactory(msgParser)

        Select Case msgParser.messageType
            Case "HEARTBEAT"
                Return New HeartbeatHandler(msgParser)
            Case "NEXTCARTREQUEST"
                Return New NextCartResponse(msgParser)
            Case "PICKDOWNLOAD"
                Return New PickDownload(msgParser)
            Case "PICKDOWNLOADACK"
                Return New PickDownloadACKHandler(msgParser)
            Case "DROPCARTREQUEST"
                Return New DropCartResponse(msgParser)
            Case "CANCELREQUEST"
                Return New CancelRequestHandler(msgParser)
            Case "ITEMSTATUS"
                Return New ItemStatusHandler(msgParser)
            Case "CARTONSTATUS"
                Return New CartonStatusHandler(msgParser)
            Case "CANCELSTATUS"
                Return New CancelStatusHandler(msgParser)
            Case "CARTSTATUS"
                Return New CartStatusResponse(msgParser)
            Case "OPERATORSTATUS"
                Return New OperatorStatusHandler(msgParser)
            Case "DROPCARTSCAN"
                Return New DropScanResponse(msgParser)
            Case "SOCKETMONITOR"
                Return New SocketMonitorHandler(msgParser)
            Case Else
                Return New UnknownMessageHandler(msgParser)
        End Select

        Return Nothing
    End Function

    Public Overrides ReadOnly Property messageType As String
        Get
            Return "PICKDIRECTORMESSAGEHANDLERFACTORY"
        End Get
    End Property

    Private Sub New(ByVal pdParser As PickDirectorMessageParser)
        MyBase.New(pdParser)
    End Sub
End Class
