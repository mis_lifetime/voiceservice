﻿Imports System.Net
Imports System.Text
Imports VoiceMessageLib

Public Class SendMessageFactory

    Public Shared Sub sendFastAckMessage(ByVal sMsg As PickDirectorMessageParser, mc As MocaConnection)
        Dim messageHandler As PickDirectorMessageHandler = PickDirectorMessageHandlerFactory.getPickDirectorMessageHandler(sMsg)
        Dim rm As ReturnMessage = New FastAckMessage(messageHandler, mc)

        rm.sendFastAckMessage()
    End Sub

    Public Shared Function getFastAckMessage(ByVal sMsg As PickDirectorMessageParser, mc As MocaConnection) As String
        Dim messageHandler As PickDirectorMessageHandler = PickDirectorMessageHandlerFactory.getPickDirectorMessageHandler(sMsg)
        Dim rm As ReturnMessage = New FastAckMessage(messageHandler, mc)

        Return rm.getReturnMessage
    End Function

    Public Shared Function getSendMessage(ByVal sMsg As PickDirectorMessageParser, mc As MocaConnection) As String
        Dim sendData As String = String.Empty

        Try

            sendData = getReturnMessage(sMsg, mc)
        Catch ex As Exception
            sendData = ex.ToString
        End Try

        Return sendData
    End Function

    Public Shared Function getSendMessageWithTimings(ByVal sMsg As PickDirectorMessageParser, mc As MocaConnection, _
                                                     Optional rcpId As ULong = 0, Optional msgId As ULong = 0) As ReturnMessageTimings
        Dim sendDataTimings As ReturnMessageTimings

        Try

            sendDataTimings = getReturnMessageWithTimings(sMsg, mc, rcpId, msgId)
        Catch ex As Exception
            sendDataTimings = New ReturnMessageTimings("", "", "", "", ex.ToString)
        End Try

        Return sendDataTimings
    End Function

    Private Shared Function getReturnMessageWithTimings(ByVal mp As PickDirectorMessageParser, mc As MocaConnection, _
                                                        Optional rcpId As ULong = 0, Optional msgId As ULong = 0) As ReturnMessageTimings
        Dim messageHandler As PickDirectorMessageHandler = PickDirectorMessageHandlerFactory.getPickDirectorMessageHandler(mp)
        Dim rm As ReturnMessage = Nothing

        Select Case mp.messageType
            Case "PICKDOWNLOAD"
                rm = New PickDownloadReturnMessage(messageHandler, mc, rcpId, msgId)
            Case "CANCELREQUEST"
                rm = New CancelRequestReturnMessage(messageHandler, mc)
            Case Else
                rm = New NormalReturnMessage(messageHandler, mc)
        End Select

        Return rm.getReturnMessageWithTimings
    End Function

    Private Shared Function getReturnMessage(ByVal mp As PickDirectorMessageParser, mc As MocaConnection) As String
        Dim messageHandler As PickDirectorMessageHandler = PickDirectorMessageHandlerFactory.getPickDirectorMessageHandler(mp)
        Dim rm As ReturnMessage = Nothing

        Select Case mp.messageType
            Case "PICKDOWNLOAD"
                rm = New PickDownloadReturnMessage(messageHandler, mc, 0, 0)
            Case "CANCELREQUEST"
                rm = New CancelRequestReturnMessage(messageHandler, mc)
            Case Else
                rm = New NormalReturnMessage(messageHandler, mc)
        End Select

        Return rm.getReturnMessage
    End Function
End Class
