﻿Public MustInherit Class BaseMessageParser
    Implements DelimitedMessageParser

    Public MustOverride ReadOnly Property delimiter As String Implements DelimitedMessageParser.delimiter
    Public Property baseMessage() As String Implements DelimitedMessageParser.baseMessage
    Public Property messageDictionary() As System.Collections.Generic.Dictionary(Of String, String) = New Dictionary(Of String, String) _
        Implements DelimitedMessageParser.messageDictionary
    Protected Property messageKeys() As String() Implements DelimitedMessageParser.messageKeys

    Protected Overridable Sub parseMessage() Implements DelimitedMessageParser.parseMessage
        Dim splitMessage As String() = baseMessage.Split(delimiter)

        For i As Integer = 0 To messageKeys.Count
            Try
                messageDictionary.Add(messageKeys(i), splitMessage(i))
            Catch ex As Exception
                ' TODO: error processing                
            End Try
        Next
    End Sub

    Protected MustOverride Function loadMessageKeys() As String() Implements DelimitedMessageParser.loadMessageKeys

    Sub New(ByVal msg As String)
        baseMessage = msg
        messageKeys = loadMessageKeys()

        parseMessage()
    End Sub
End Class
