﻿Public Class PickUpload
    Inherits PickDirectorMessageParser

    Public Overrides Property messageType() As String = "PICKUPLOAD"

    Protected Overrides Function loadMessageBodyKeys() As String()
        Dim mbk As String() = {"MessageType", "CartID", "OrderType", "WaveID", "OrderID", "ItemID", _
                               "LineID", "UPC", "Description", "UOM", "Location", "RequiredQuantity", _
                               "ActualQuantity", "Date", "Time", "OperatorID", "OperatorDescription", "State"}
        Return mbk
    End Function

    Sub New(ByVal msg As String)
        MyBase.New(msg)
    End Sub
End Class