﻿Imports VoiceMessageLibDEV

Public MustInherit Class BaseVoiceCommandTest

    Protected Property stx() As String = Chr(2)
    Protected Property etx() As String = Chr(3)
    Protected ReadOnly Property messageTemplate As String
        Get
            Return stx & "PublisherLocation^PublisherType^SubscriberLocation^SubscriberType^{MT}^^{MESSAGE}" & etx
        End Get
    End Property

    Protected MustOverride ReadOnly Property messageBodyTemplate As String
    Protected MustOverride Property testName As String
    Protected MustOverride Property testMessageType As String
    Protected Property testData As Dictionary(Of String, String) = New Dictionary(Of String, String)
    Protected Property pickDirectorMessageParser As PickDirectorMessageParser
    Protected Property messageBodyParameters As Queue(Of String)
    Protected Property messageParser As MessageParserFactory.messageHandlers
    Protected Property messageHandler As messageHandlers
    Protected Property messageString As String
    Protected Property messageQueue As Queue(Of String) = New Queue(Of String)

    Protected MustOverride Function getMessageBodyDataParameters() As Queue(Of String)
    Protected MustOverride Function validateTestData() As Dictionary(Of String, String)

    Protected Function formatMessage() As String
        Dim baseTemplate As String = messageTemplate
        baseTemplate = baseTemplate.Replace("{MT}", testMessageType)

        messageBodyParameters = getMessageBodyDataParameters()
        Dim msg As String = autoMessageFormatToTemplate(messageBodyTemplate, messageBodyParameters)
        baseTemplate = baseTemplate.Replace("{MESSAGE}", msg)

        Return baseTemplate
    End Function

    Public Function getFormattedMessage() As String
        messageBodyParameters = getMessageBodyDataParameters()

        Dim formattedMessage As String = messageTemplate
        formattedMessage = formattedMessage.Replace("{MT}", testMessageType)

        Dim msg As String = autoMessageFormatToTemplate(messageBodyTemplate, messageBodyParameters)
        formattedMessage = formattedMessage.Replace("{MESSAGE}", msg)

        Return formattedMessage
    End Function

    Public Function runTest() As Dictionary(Of String, Dictionary(Of String, String))
        Dim result As Dictionary(Of String, Dictionary(Of String, String)) = New Dictionary(Of String, Dictionary(Of String, String))
        Dim resultsDictionary As Dictionary(Of String, String) = New Dictionary(Of String, String)
        Dim testDataResults As Dictionary(Of String, String)

        Console.Clear()
        Console.WriteLine("Running Test {0}", testName)
        For Each widget As String In testData.Keys
            Console.WriteLine("{0}: {1}{2}", widget, testData.Item(widget), vbCrLf)
        Next

        testDataResults = validateTestData()
        If testDataResults IsNot Nothing AndAlso testDataResults.Count > 0 Then
            resultsDictionary = testDataResults
            result.Add("FAILED", resultsDictionary)

            Return result
        Else
            messageBodyParameters = getMessageBodyDataParameters()
        End If

        Dim formattedMessage As String = getFormattedMessage()
        Console.WriteLine(formattedMessage)
        'Console.ReadLine()

        Dim voiceAddress As Net.IPAddress = Nothing
        Net.IPAddress.TryParse(pickDirectorIP, voiceAddress)
        Dim serviceEndPoint As Net.IPEndPoint = New Net.IPEndPoint(voiceAddress, 8888)
        Dim x As String = VoiceTestGlobals.sendAndReceiveData(serviceEndPoint, formattedMessage, True)

        'Dim mParse As PickDirectorMessageParser = getMessageParser(messageParser, formattedMessage)
        'Dim mHandler As PickDirectorMessageHandler = getMessageHandler(messageHandler, mParse)
        'mHandler.processMessage()

        'Dim d As Dictionary(Of String, Dictionary(Of String, String)) = mHandler.returnData
        'For Each widget As String In d.Keys

        '    Dim dx As Dictionary(Of String, String) = d.Item(widget)
        '    result.Add(widget, dx)
        '    For Each widgetx As String In dx.Keys

        '        If widgetx.Equals("VOICE_CMD_RESULT") AndAlso Not dx.Item(widgetx).Equals("0") Then
        '            resultsDictionary.Add("CMD_FAIL", "Command Failed with code " & dx.Item(widgetx))
        '            resultsDictionary.Add("ERROR", dx.Item("ERROR"))
        '            result.Add("FAILED", resultsDictionary)

        '            Return result
        '        End If
        '    Next
        'Next

        resultsDictionary.Add("OK", x)
        result.Add("PASSED", resultsDictionary)

        Return result
    End Function

    Public Sub supplyTestData(ByVal tData As Dictionary(Of String, String))
        testData = tData
    End Sub

    Sub New(ByVal msgParser As messageParsers, ByVal msgHandler As messageHandlers)

        messageParser = msgParser
        messageHandler = msgHandler
    End Sub
End Class
