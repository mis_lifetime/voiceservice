﻿Imports System.Text
Imports System.Net
Imports System.Net.Sockets
Imports System.Threading
Imports System.Collections.Concurrent
Imports System.Xml

Module LTBMMDAServiceGlobals

    ' Run mode
    Public Enum runMode
        TEST ' No real Pick Director Connection
        LIVE ' Connect to Pick Director
    End Enum

    Public Enum logLevel        
        LOW = 1
        NORMAL = 3
        HIGH = 5        
        APPERROR = 7
        CRITICAL = 9
    End Enum

    ' Log Mode
    Public Enum logMode
        SILENT      ' No Logging
        EVENTONLY   ' Log Only to the Event Log
        DCSONLY     ' Log Only to DCS
        ALL         ' Log Both
    End Enum

    Public Enum dbMode
        NEWPROD
        PROD
    End Enum

    Public numMConns As UShort = 15
    Public mConns As Dictionary(Of UShort, MocaConnection) = New Dictionary(Of UShort, MocaConnection)
    Public mConnIdx As UShort
    Public mConnLock As New Object

    ' SET THESE BASED ON WHETHER THIS IS THE TEST VERSION OR LIVE VERSION THEN BUILD
    'Public socketRunMode As runMode = runMode.TEST
    'Public pubLocPubType = "PD-TEST-NJ^LTBMMDAService"
    'Public socketRunMode As runMode = runMode.LIVE ' MAKE THIS GO AWAY   
    'Public socketDBMode As dbMode = dbMode.PROD ' am i using this????
    'Public PickDirectorIP As String = "172.16.1.149"
    'Public serviceSocketIP As String = "172.16.1.149"  

    Public socketLogMode As logMode = logMode.DCSONLY
    Public eventLogLevel As logLevel = logLevel.HIGH
    Public dbLogLevel As logLevel = logLevel.HIGH
    Public retryInterval As Integer = 15000

    Public pubLocPubType = "PD-LTB-NJ^LTBMMDAService"
    Public pickDirectorIP As String = "172.16.1.149"
    Public serviceSocketIP As String = "172.16.1.177"
    Public pickDirectorPort As Integer = 7346
    Public serviceSocketPort As Integer = 6780

    ' LIVE SETTINGS!!
    'Public pubLocPubType = "PD-LTB-NJ^LTBMMDAService"
    'Public pickDirectorIP As String = "172.16.1.149"
    'Public serviceSocketIP As String = "172.16.1.149"
    'Public pickDirectorPort As Integer = 7346
    'Public serviceSocketPort As Integer = 6780

    ' TEST SETTINGS!!
    'Public pubLocPubType = "PD-LTB-NJ^LTBMMDAService"
    'Public pickDirectorIP As String = "172.16.1.170"
    'Public serviceSocketIP As String = "172.16.1.170"
    'Public pickDirectorPort As Integer = 7346
    'Public serviceSocketPort As Integer = 6780

    ' DEV SETTINGS!!
    'Public pubLocPubType = "PD-LTB-NJ^LTBMMDAService"
    'Public pickDirectorIP As String = "172.16.1.169"
    'Public serviceSocketIP As String = "172.16.1.169"
    'Public pickDirectorPort As Integer = 7346
    'Public serviceSocketPort As Integer = 6780

    Public Const bufferSize As Integer = 1024

    ' PickDirector Connection Variables    
    Public filterYellow As String = Chr(2) & pubLocPubType & "^^MessageDistributor^$filter$^^MT = 'CARTONSTATUS' or MT = 'ITEMSTATUS' or MT = 'DROPCARTSCAN'" & Chr(3)
    Public filterGreen As String = Chr(2) & pubLocPubType & "^^MessageDistributor^$filter$^^MT = 'PICKDOWNLOAD_ACK' or MT = 'PICKDOWNLOADACK' or MT = 'OPERATORSTATUS'" & Chr(3)
    Public filterOrange As String = Chr(2) & pubLocPubType & "^^MessageDistributor^$filter$^^MT = 'NEXTCARTREQUEST' or MT = 'DROPCARTREQUEST' or MT = 'CARTSTATUS'" & Chr(3)
    Public filterCancelRequest As String = Chr(2) & pubLocPubType & "^^MessageDistributor^$filter$^^MT = 'CANCELSTATUS'" & Chr(3)

    ' constant message delimiters and characters
    Public Const messageDelimiter As Char = "^"
    Public Const messageDataDelimiter As Char = ","
    Public Const stxVal As String = Chr(2)
    Public Const etxVal As String = Chr(3)

    ' Event Log Constants
    Public Const EVT_SOURCE As String = "LTBMMDAServiceSocket"
    Public Const EVT_LOG As String = "Application"
    Public Const EVT_EVENT As String = "Socket State Event"
    Public Const EVT_MACHINE As String = "."
    Public eventData As New EventSourceCreationData(EVT_SOURCE, EVT_LOG)
    Public LTBMMDAEventLog As EventLog = New EventLog(EVT_LOG, EVT_MACHINE, EVT_SOURCE)

    Public monitors As ConcurrentDictionary(Of String, IPEndPoint) = New ConcurrentDictionary(Of String, IPEndPoint)

    ' Thread States
    Public Enum threadStates
        Started
        Stopped
    End Enum

    ' Socket State Enumeration
    Public Enum socketStates
        Stopping
        Stopped
        Starting
        Started
        Blocking
        Receiving
        Received
        Sending
        ConnectingMOCA
        ReceivingMOCA
        WaitingForEOF
        Borked
        Other
    End Enum

    Private threadLockx As Object = New Object

    'Public Sub notifyClients(ByVal msg As String, ByVal id As String)
    '    Try
    '        msg = "(" & id & ") " & msg
    '        ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf notifyThreaded), msg)
    '    Catch ex As Exception
    '        logAlertMessage("NTFY ThreadERR: " & ex.ToString, "NOTIFYTL", logLevel.APPERROR)
    '    End Try
    'End Sub

    Public Function getNextMocaConn() As MocaConnection
        SyncLock mConnLock
            mConnIdx += 1
            If mConnIdx > numMConns Then
                mConnIdx = 1
            End If

            Dim mc As MocaConnection = Nothing
            Dim msg As StringBuilder = New StringBuilder
            If mConns.ContainsKey(mConnIdx) Then
                mc = mConns.Item(mConnIdx)
                If mc.isBusy Then

                    msg.Append("|").Append(mConnIdx.ToString).Append(" is busy")

                    Dim tries As UShort = 1
                    Dim conFound As Boolean = False

                    Do

                        mConnIdx += 1
                        If mConnIdx > numMConns Then
                            mConnIdx = 1
                        End If

                        mc = mConns.Item(mConnIdx)
                        If Not mc.isBusy Then
                            conFound = True
                            msg.Append("| Found not busy conn: ").Append(mConnIdx.ToString)
                        End If

                        tries += 1
                    Loop Until tries = numMConns Or conFound

                    If Not conFound Then
                        msg.Append("| No free connections, starting a new one . . .")
                        mc = Nothing
                        mc = New MocaConnection
                        mc.setUnmanaged()
                    End If

                    If mc Is Nothing Or Not mc.isConnected Then
                        msg.Append("| reconnecting . . .")
                        mc.connect()
                    End If
                Else
                    msg.Append("| Using idx conn ").Append(mConnIdx.ToString)

                    If mc Is Nothing Or Not mc.isConnected Then
                        msg.Append("| reconnection . . .")
                        mc.connect()
                    End If
                End If
            Else
                msg.Append("| Bad Key new conn returned")
                mc = New MocaConnection
                mc.setUnmanaged()
            End If

            mc.setMsg("")
            mc.setMsg(msg.ToString)
            If Not mc.isUnmanaged Then mc.makeBusy()

            Return mc
        End SyncLock
    End Function

    Public Sub loadMocaConnections()

        For cnt As UShort = 1 To numMConns
            mConns.Add(cnt, New MocaConnection)
        Next

        mConnIdx = 0
    End Sub

    Public Sub stopMocaConnections()
        Try
            For cnt As UShort = 1 To numMConns
                mConns.Item(cnt).closeConnection()
                mConns.Item(cnt) = Nothing
            Next
        Catch mEx As Exception
        End Try
    End Sub

    Public Sub notifyThreaded(ByVal msg As String)
        logGenericAlertMessage("Monitors: " & monitors.Count.ToString, "NOTIFY", logLevel.LOW)

        For Each widget As String In monitors.Keys
            Try
                logGenericAlertMessage(msg, "NOTIFY", logLevel.LOW)
                sendAndReceiveData(monitors.Item(widget), msg, False, "NOTIFY")
            Catch ex As Exception
                logGenericAlertMessage("NotifyERR: " + ex.ToString, "NOTIFY", logLevel.APPERROR)
            End Try
        Next
    End Sub

    Public Sub handleRegisterUnregister(ByRef sm As VoiceMessageLib.PickDirectorMessageParser)
        Try
            Dim mip As String = sm.messageBodyDictionary.Item("IP")
            Dim monitorPort As Integer = Integer.Parse(sm.messageBodyDictionary.Item("Port"))
            Dim monitorIP As IPAddress = IPAddress.Parse(mip)
            Dim monitorKey As String = mip & ":" & monitorPort.ToString
            Dim kv As String = sm.messageBodyDictionary.Item("KV")

            logGenericAlertMessage(mip & ":" & monitorPort.ToString & " (" & kv & ")", "REGUNREG", logLevel.NORMAL)

            SyncLock threadLockx
                If kv.Equals("REGISTER") Then
                    If Not monitors.ContainsKey(monitorKey) Then
                        Dim monitorEndPoint As IPEndPoint = New IPEndPoint(monitorIP, monitorPort)
                        If Not monitors.TryAdd(monitorKey, monitorEndPoint) Then
                            logGenericAlertMessage("Failed to Add Monitor", "REGMONITOR", logLevel.APPERROR)
                        End If
                    End If
                Else
                    Dim ipep As IPEndPoint = Nothing
                    If Not monitors.TryRemove(monitorKey, ipep) Then
                        logGenericAlertMessage("Failed to Remove Monitor", "REGMONITOR", logLevel.APPERROR)
                    End If
                End If
            End SyncLock
        Catch ex As Exception
            logGenericAlertMessage(ex.ToString, "REGUNREG", logLevel.APPERROR)
        End Try
    End Sub

    Public Sub logGenericAlertMessage(ByVal msg As String, Optional ByVal ttl As String = "INFO", Optional ByVal llvl As logLevel = logLevel.LOW)

        Dim mc As MocaConnection = getNextMocaConn()

        Dim am As AlertMessage = New AlertMessage(mc, msg, llvl, ttl)
        ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf am.logAlertMessage))
    End Sub

    Public Sub logServiceMessage(ByVal mid As ULong, ByVal msgtx As String, ByVal stdte As String, ByVal spdte As String, _
                                   ByVal msgseq As UInteger, msgrtn As String, msgtyp As String, msgkey As String, msgack As String, pdMsg As Boolean, lid As String)
        Dim mc As MocaConnection = getNextMocaConn()

        Dim am As AlertMessage = New AlertMessage(mid, msgtx, stdte, spdte, msgseq, msgrtn, msgtyp, msgkey, msgack, mc, pdMsg, lid)
        ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf am.logServiceMessage))
    End Sub

    Public Function sendAndReceiveData(ByVal cAddr As IPEndPoint, ByVal msgData As String, ByVal reqRespFlg As Boolean, tran As String) As String
        ' Establish the local endpoint for the socket.                        
        Dim remoteEP As New IPEndPoint(cAddr.Address, cAddr.Port)

        ' Data buffer for incoming data.
        Dim bytes(bufferSize) As Byte

        ' Create a TCP/IP socket.
        Dim sender As New Socket(AddressFamily.InterNetwork, _
            SocketType.Stream, ProtocolType.Tcp)

        If msgData = String.Empty Then Return "Error: No Data To Send"

        'LTBMMDAEventLog.WriteEntry("Connecting to " & cAddr.ToString & " : " & cAddr.Port)

        sender.SendTimeout = 10000
        sender.ReceiveTimeout = 10000

        Try
            ' Connect the socket to the remote endpoint.
            sender.Connect(remoteEP)

            If tran.Equals("CANCELREQUEST") Then
                'logGenericAlertMessage("sendAndReceiveData(): " & filterCancelRequest, "FILTERX", logLevel.LOW)
                sender.Send(Encoding.ASCII.GetBytes(filterCancelRequest))
            End If
        Catch ex As Exception
            'LTBMMDAEventLog.WriteEntry("Unable to Connect to Client: " & ex.ToString)
            Return "ERROR: Unable to Connect to Client at " & cAddr.ToString & " : " & cAddr.Port
        End Try

        ' Encode the data string into a byte array.
        Dim msg As Byte() = Encoding.ASCII.GetBytes(msgData)

        ' Send the data through the socket.        
        logGenericAlertMessage("sendAndReceiveData(=>" & cAddr.ToString & "): " & msgData, "SEND", logLevel.NORMAL)

        Dim bytesSent As Integer = sender.Send(msg)

        Dim retmsg As String = String.Empty

        If reqRespFlg Then
            Try
                ' Receive the response from the remote device.
                Dim bytesRec As Integer = sender.Receive(bytes)
                retmsg = Encoding.ASCII.GetString(bytes, 0, bytesRec)

                logGenericAlertMessage("sendAndReceiveData(<=" & cAddr.ToString & "): " & retmsg, "RECEIVE", logLevel.NORMAL)
            Catch ex As Exception
                retmsg = "Receive Timeout"
            End Try
        End If

        'LTBMMDAEventLog.WriteEntry("Received: " & retmsg)

        ' Release the socket.
        sender.Shutdown(SocketShutdown.Both)
        sender.Close()

        Return retmsg
    End Function

    Public Sub initProcess()
        LTBMMDAEventLog.WriteEntry("initProcess()")
        ' Put the XML file in the installed dirctory c:\programfiles\ltbmmdaservice   
        Dim reader As XmlTextReader = New XmlTextReader(AppDomain.CurrentDomain.BaseDirectory + "LTBMMDAServiceInit.xml")

        Do While (reader.Read())
            Select Case reader.NodeType
                Case XmlNodeType.Element 'Display beginning of element.
                    If reader.Name = "voice-ip" Then
                        reader.Read()
                        pickDirectorIP = reader.Value
                    ElseIf reader.Name = "voice-port" Then
                        reader.Read()
                        pickDirectorPort = reader.Value
                    ElseIf reader.Name = "service-ip" Then
                        reader.Read()
                        serviceSocketIP = reader.Value
                    ElseIf reader.Name = "service-port" Then
                        reader.Read()
                        serviceSocketPort = reader.Value
                    ElseIf reader.Name = "retry-interval" Then
                        reader.Read()
                        retryInterval = reader.Value
                    ElseIf reader.Name = "pub-type" Then
                        reader.Read()
                        pubLocPubType = reader.Value
                    ElseIf reader.Name = "socket-log-mode" Then
                        reader.Read()
                        Dim st As String = reader.Value
                        Select Case st
                            Case "SILENT"
                                socketLogMode = logMode.SILENT
                            Case "EVENTONLY"
                                socketLogMode = logMode.EVENTONLY
                            Case "DCSONLY"
                                socketLogMode = logMode.DCSONLY
                            Case "ALL"
                                socketLogMode = logMode.ALL
                            Case Else
                                socketLogMode = logMode.DCSONLY
                        End Select
                    ElseIf reader.Name = "event-log-level" Then
                        reader.Read()
                        Dim st As String = reader.Value
                        Select Case st
                            Case "APPERROR"
                                eventLogLevel = logLevel.APPERROR
                            Case "LOW"
                                eventLogLevel = logLevel.LOW
                            Case "NORMAL"
                                eventLogLevel = logLevel.NORMAL
                            Case "HIGH"
                                eventLogLevel = logLevel.HIGH
                            Case Else
                                eventLogLevel = logLevel.APPERROR
                        End Select
                    ElseIf reader.Name = "db-log-level" Then
                        reader.Read()
                        Dim st As String = reader.Value
                        Select Case st
                            Case "APPERROR"
                                dbLogLevel = logLevel.APPERROR
                            Case "LOW"
                                dbLogLevel = logLevel.LOW
                            Case "NORMAL"
                                dbLogLevel = logLevel.NORMAL
                            Case "HIGH"
                                dbLogLevel = logLevel.HIGH
                            Case Else
                                dbLogLevel = logLevel.APPERROR
                        End Select
                    End If
            End Select
        Loop

        reader.Close()

        Try
            logGenericAlertMessage(pickDirectorIP.ToString + ":" + pickDirectorPort.ToString, "CONN-PDSOCK", logLevel.CRITICAL)
            logGenericAlertMessage(serviceSocketIP.ToString + ":" + serviceSocketPort.ToString, "CONN-LBSOCK", logLevel.CRITICAL)
            logGenericAlertMessage(socketLogMode.ToString, "LOGMODE", logLevel.CRITICAL)
            logGenericAlertMessage(eventLogLevel.ToString, "EVENTLOGLVL", logLevel.CRITICAL)
            logGenericAlertMessage(dbLogLevel.ToString, "DBLOGLVL", logLevel.CRITICAL)
        Catch ex As Exception
            logGenericAlertMessage(ex.StackTrace, "INIT", logLevel.CRITICAL)
        End Try

        logGenericAlertMessage("DONE Initializing From File", "INIT", logLevel.CRITICAL)
    End Sub
End Module
