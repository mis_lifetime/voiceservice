﻿Public Class PickDownload
    Inherits PickDirectorMessageHandler

    Public Shared ReadOnly Property sharedMessageBodyTemplate
        Get
            Return "{""MT"":""{0}"",""CartID"":""{1}"",""OrderType"":""{2}"",""Zone"":""{3}"",""OrderID"":""{4}""," & _
            """CartSeq"":""{5}"",""ItemID"":""{6}"",""InnerCode"":""{7}"",""CaseCode"":""{8}"",""UPC"":""{9}""," & _
            """LineID"":""{10}"",""Description"":""{11}"",""UOM"":""{12}"",""UOM_SP"":""{13}"",""Location"":""{14}""," & _
            """VoiceAisle"":""{15}"",""VoiceBay"":""{16}"",""VoiceShelf"":""{17}"",""VoiceAisle_SP"":""{18}"",""VoiceBay_SP"":""{19}""," & _
            """VoiceShelf_SP"":""{20}"",""LocationSequence"":""{21}"",""CheckDigit"":""{22}"",""RequiredQuantity"":""{23}"",""SpecialHandling"":""{24}""," & _
            """SpecialHandling_SP"":""{25}"",""VoiceItemId"":""{26}""}"
        End Get
    End Property

    Public Overrides ReadOnly Property messageType() As String
        Get
            Return "PICKDOWNLOAD"
        End Get
    End Property

    Protected Overrides Function loadMocaCommand() As String
        Return "get usr cart info for voice where cartid = '{0}'"
    End Function

    Protected Overrides Function loadMessageBodyTemplate() As String
        Return sharedMessageBodyTemplate
    End Function

    Protected Overrides Function loadMocaCommandParameters() As Queue(Of String)
        Dim queueOfParms As Queue(Of String) = New Queue(Of String)
        Dim cId As String = String.Empty

        Try
            cId = pickDirectorBaseMessage.messageBodyDictionary.Item("CartID")
            cId = cId.Replace(Chr(3), "")
        Catch ex As Exception
        End Try

        queueOfParms.Enqueue(cId)

        Return queueOfParms
    End Function

    Public Shared Function getUpdateCommandFromDictionary(ByVal mainTemplate As String, _
                                                         ByVal rowDictionary As Dictionary(Of String, String)) As String

        Dim lineID As String = getValueFromDictionary("line_id", rowDictionary)
        Return "set usr pick upload to voice date where line_id = '" & lineID & "' and ackflg = 0"
    End Function

    Public Shared Function getReturnStringFromDictionary(ByVal mainTemplate As String, _
                                                         ByVal rowDictionary As Dictionary(Of String, String)) As String

        Dim bodyString As String = String.Empty
        Dim bodyQ As Queue(Of String) = New Queue(Of String)
        Dim lineID As String = getValueFromDictionary("line_id", rowDictionary)

        bodyQ.Enqueue("PICKDOWNLOAD")
        bodyQ.Enqueue(getValueFromDictionary("cart_id", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("order_type", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("zone", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("order_id", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("cart_seq", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("item_id", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("inner_code", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("case_code", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("upc", rowDictionary))
        bodyQ.Enqueue(lineID)
        bodyQ.Enqueue(getValueFromDictionary("description", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("uom", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("uom_spanish", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("location", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("voiceaisle", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("voicebay", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("voiceshelf", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("voiceaisle_spanish", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("voicebay_spanish", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("voiceshelf_spanish", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("walk_sequence", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("check_digit", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("required_quantity", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("special_handling", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("special_handling_spanish", rowDictionary))
        bodyQ.Enqueue(getValueFromDictionary("voice_item_id", rowDictionary))

        bodyString = autoMessageFormatToTemplate(sharedMessageBodyTemplate, bodyQ)

        Return mainTemplate.Replace("{Message}", bodyString).Replace("{SEQNUM}", lineID)
    End Function

    Protected Overrides Function loadMessageBodyParameterQueue() As Queue(Of String)
        Dim msgQ As Queue(Of String) = New Queue(Of String)        

        Return msgQ
    End Function

    Sub New(ByVal msgIn As DelimitedMessageParser)
        MyBase.New(msgIn)
    End Sub
End Class