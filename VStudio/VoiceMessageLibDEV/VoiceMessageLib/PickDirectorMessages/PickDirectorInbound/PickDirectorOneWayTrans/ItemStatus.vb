﻿Public Class ItemStatus
    Inherits PickDirectorMessageParser

    Public Overrides Property requiresFastAck As Boolean = True
    Public Overrides Property requiresReturnMsg As Boolean = False

    Protected Overrides Function loadMessageBodyKeys() As String()
        Dim mbk As String() = {"MT", "CartID", "OrderID", "LineID", "Location", "RequiredQuantity", "ActualQuantity", _
                               "Date", "Time", "OperatorID", "OperatorDescription", "State"}
        Return mbk
    End Function

    Public Overrides Property messageType As String = "ITEMSTATUS"

    Sub New(ByVal msg As String)
        MyBase.New(msg)
    End Sub
End Class