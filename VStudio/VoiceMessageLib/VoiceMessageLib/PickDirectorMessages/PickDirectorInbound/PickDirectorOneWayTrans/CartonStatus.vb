﻿Public Class CartonStatus
    Inherits PickDirectorMessageParser

    Public Overrides Property requiresFastAck As Boolean = True
    Public Overrides Property requiresReturnMsg As Boolean = False

    Protected Overrides Function loadMessageBodyKeys() As String()
        Dim mbk As String() = {"MT", "OrderID", "Status"}
        Return mbk
    End Function

    Public Overrides Property messageType As String = "CARTONSTATUS"

    Sub New(ByVal msg As String)
        MyBase.New(msg)
    End Sub
End Class