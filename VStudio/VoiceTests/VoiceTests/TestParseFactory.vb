﻿Imports VoiceMessageLibDEV

Public Class TestParseFactory
    Inherits PickDirectorMessageParser

    Protected Overrides Function loadMessageBodyKeys() As String()
        Return {"PICKDIRECTORMESSAGEPARSERFACTORY"}
    End Function

    Public Overrides Property messageType As String = "PICKDIRECTORMESSAGEPARSERFACTORY"

    Public Shared Function getPickDirectorParser(ByVal msgData As String) As PickDirectorMessageParser
        Dim pickDirectorMessageParser As TestParseFactory = New TestParseFactory(msgData)

        Console.WriteLine("TPF: {0}", pickDirectorMessageParser.messageDictionary.Item("MessageType"))

        Select Case pickDirectorMessageParser.messageDictionary.Item("MessageType")
            Case "PICKDOWNLOAD"
                pickDirectorMessageParser.messageTypeIn = messageTypesIn.PICKDOWNLOAD
                Return New PickDownloadParser(pickDirectorMessageParser.baseMessage)
            Case "NEXTCARTREQUEST"
                pickDirectorMessageParser.messageTypeIn = messageTypesIn.NEXTCARTREQUEST
                Return New NextCartRequest(pickDirectorMessageParser.baseMessage)
            Case "DROPCARTREQUEST"
                pickDirectorMessageParser.messageTypeIn = messageTypesIn.DROPCARTREQUEST
                Return New DropCartRequest(pickDirectorMessageParser.baseMessage)
            Case "CANCELREQUEST"
                pickDirectorMessageParser.messageTypeIn = messageTypesIn.CANCELREQUEST
                Return New CancelRequest(pickDirectorMessageParser.baseMessage)
            Case "ITEMSTATUS"
                pickDirectorMessageParser.messageTypeIn = messageTypesIn.ITEMSTATUS
                Return New ItemStatus(pickDirectorMessageParser.baseMessage)
            Case "$HeartBeat$"
                pickDirectorMessageParser.messageTypeIn = messageTypesIn.HEARTBEAT
                Return New HeartbeatParser(pickDirectorMessageParser.baseMessage)
            Case Else
                pickDirectorMessageParser.messageTypeIn = messageTypesIn.INVALID
                Return New UnknownMessage(pickDirectorMessageParser.baseMessage)
        End Select
    End Function

    ' Don't let outside routines instantiate this object
    Private Sub New(ByVal msgData As String)
        MyBase.New(msgData)
    End Sub
End Class
