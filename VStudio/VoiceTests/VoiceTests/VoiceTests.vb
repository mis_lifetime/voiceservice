﻿Imports System.Net.Sockets
Imports System.Net
Imports System.Threading
Imports VoiceMessageLibDEV

Module VoiceTests
    Dim tests As List(Of BaseVoiceCommandTest) = New List(Of BaseVoiceCommandTest)
    Public socketLogMode As logMode = logMode.EVENTONLY
    Public eventLogLevel As LogLevel = logLevel.HIGH
    Public dbLogLevel As logLevel = logLevel.APPERROR
    Private mConns As Dictionary(Of UShort, MocaConnection) = New Dictionary(Of UShort, MocaConnection)
    Private mConnLock As New Object
    Private mConnIdx As UShort

    ' Log Mode
    Public Enum logMode
        SILENT      ' No Logging
        EVENTONLY   ' Log Only to the Event Log
        DCSONLY     ' Log Only to DCS
        ALL         ' Log Both
    End Enum

    Public Enum logLevel
        LOW = 1
        NORMAL = 3
        HIGH = 5
        APPERROR = 7
        CRITICAL = 9
    End Enum

    Sub Main()
        Dim npd As NotPickDirectorMulti = Nothing

        Try
            testSimpleMocaConn()

            'testMocaConns()

            'runNewCartStatusStandAloneTest()
            'runPickDownloadACKStandAloneTest()

            'npd = NotPickDirectorMulti.getNotPickDirectorInstance

            'Console.ReadLine()

            'Dim strHostName As String = System.Net.Dns.GetHostName()
            'Console.WriteLine("MachineName: {0}", strHostName)                           

            'checkDropCartRequest()

            'handleMessageChunksTest()
            'writeTestMessageToDB("VoiceTests - Test Message to DB")

            'checkCartonStatus()
            'checkCartStatus()

            'runNextCartStandAloneTest()

            'runNextCartManual()
            'sendCartsToPD()

            'runTestQueryWithMocaConn()
            'testStuff()

            'checkCancelStatus()

            'checkParserFactory("PublisherLocation^PublisherType^PD-TEST-NJ^LTBMMDAService^PICKDOWNLOAD_ACK^WRL4089453_ACK^")
            'checkFastAck("PublisherLocation^PublisherType^SubscriberLocation^SubscriberType^PICKDOWNLOAD^^{""MT"":""PICKDOWNLOAD"",""CartID"":""WMS2H001""}")

            'testPickDownloadStandAlone()
            'runStandAloneHeartbeatTest()            
            'runPickDownLoadStandAloneTest()
            'runDropCartResponseStandAloneTest()
            'runCancelRequestStandAloneTest()
            'runDropCartScanStandAloneTest()
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
            Console.ReadLine()
        Finally

        End Try
    End Sub

    Private Sub runTestQueryWithMocaConn()
        Console.WriteLine("loading connection . . .")
        Dim mc As MocaConnection = New MocaConnection

        Console.WriteLine("Connected . . .")

        mc.cleanUp()
        mc.closeConnection()

        Console.ReadLine()
    End Sub

    Private Function getNextMocaConn() As MocaConnection
        SyncLock mConnLock
            mConnIdx += 1
            If mConnIdx > 10 Then
                mConnIdx = 1
            End If

            Console.WriteLine("getting conn {0}", mConnIdx)

            Dim mc As MocaConnection = Nothing
            If mConns.ContainsKey(mConnIdx) Then
                mc = mConns.Item(mConnIdx)
                If mc.isBusy Then

                    Dim tries As UShort = 1
                    Dim conFound As Boolean = False

                    Do

                        mConnIdx += 1
                        If mConnIdx > 10 Then
                            mConnIdx = 1
                        End If

                        mc = mConns.Item(mConnIdx)
                        If Not mc.isBusy Then
                            conFound = True                        
                            Console.WriteLine("Found not busy conn: {0}", mConnIdx)
                        End If

                        tries += 1
                    Loop Until tries = 10 Or conFound

                    If Not conFound Then
                        Console.WriteLine("No free connections, starting a new one . . .")
                        mc = Nothing
                        mc = New MocaConnection
                        mc.setUnmanaged()
                    End If

                    If mc Is Nothing Or Not mc.isConnected Then
                        Console.WriteLine("conn is not connected, connecting it")
                        mc.connect()
                    End If
                Else
                    Console.WriteLine("Using idx conn")

                    If mc Is Nothing Or Not mc.isConnected Then
                        Console.WriteLine("conn is not connected, connecting it")
                        mc.connect()
                    End If
                End If
            Else
                Console.WriteLine("Bad Key new conn returned")
                mc = New MocaConnection
                mc.setUnmanaged()
            End If

            mc.makeBusy()
            Return mc
        End SyncLock
    End Function

    Private Sub sendCartsToPD()
        Dim mc As MocaConnection = New MocaConnection

        Dim mocaCmdOne As String = "send usr cart info to voice where cartid = '{0}'"
        'Dim mocaCmdTwo As String = "[update locmst set rescod = 'BUILT25' where stoloc = '{0}']"
        Dim mocaCmdThree As String = "[update usr_pd_test_carts set is_used = 1 where cartid = '{0}']"
        Dim runCmd As String = Nothing

        For cnt As Integer = 401 To 1000
            Dim cartId As String = "TEST" + Format(cnt, "0000").ToString
            runCmd = mocaCmdOne.Replace("{0}", cartId)
            Console.WriteLine(runCmd)
            mc.execute(runCmd)
            'runCmd = mocaCmdTwo.Replace("{0}", cartId)
            'Console.WriteLine(runCmd)
            'mc.execute(runCmd)
            runCmd = mocaCmdThree.Replace("{0}", cartId)
            Console.WriteLine(runCmd)
            mc.execute(runCmd)

            Thread.Sleep(50)
        Next

        mc.closeConnection()

        Console.ReadLine()
    End Sub

    Private Sub loadMocaConnections()

        For cnt As UShort = 1 To 10
            Console.WriteLine("loading connection {0}", cnt.ToString)
            Dim mc As MocaConnection = New MocaConnection

            'If cnt = 2 Or cnt = 4 Or cnt = 6 Or cnt = 8 Or cnt = 10 Then
            'mc.makeBusy()
            'End If

            mConns.Add(cnt, mc)
        Next
    End Sub

    Private Sub testStuff()
        Dim x As String = "TEST"

        If x = "TEST" Then
            Console.WriteLine("X is TEST")
        Else
            Console.WriteLine("X is not TEST")
        End If

        Console.ReadLine()
    End Sub

    Private Sub closeMocaConns()

        For cnt As UShort = 1 To 10
            Console.WriteLine("closing connection {0}", cnt.ToString)
            Console.WriteLine("conn was used {0} times", mConns.Item(cnt).getUsedCount.ToString)
            mConns.Item(cnt).closeConnection()
            mConns.Item(cnt) = Nothing
        Next
    End Sub

    Private Sub testSimpleMocaConn()

        Dim mc = New MocaConnection

        While True
            Dim rs As Integer = mc.execute("[select 1 fld from shipment where rownum < 2]")
            Console.WriteLine("Cmd res {0}", rs.ToString)
            Thread.Sleep(5000)
        End While
    End Sub

    Private Sub testMocaConns()
        Dim cmd = "[select '{0}' from dual]"
        mConnIdx = 0
        loadMocaConnections()
        Dim borkOne As Boolean = True

        For cnt As UShort = 1 To 10
            Dim xcmd = cmd.Replace("{0}", cnt.ToString)
            Console.WriteLine(xcmd)
            Dim mc As MocaConnection = getNextMocaConn()
            Dim rs As Integer = mc.execute(xcmd)
            Console.WriteLine("Cmd res {0}", rs.ToString)
            mc.incUsedCnt()
            mc.makeFree()

            If borkOne Then
                Console.WriteLine("Borking the connection . . .")
                mc.closeConnection()
                borkOne = False
            End If

            If mc.isUnmanaged Then
                Console.WriteLine("Closing unmanaged connection")
                mc.closeConnection()
            End If
        Next

        closeMocaConns()
        Console.ReadLine()
    End Sub

    Private Sub writeTestMessageToDB(ByRef msg As String)
        Console.WriteLine("Logging message to DB - " & msg)
        VoiceMessageLibDEV.logMessage(msg, "VoiceTests", Format(Now, "yyyyMMddHHmmssfff"))
        Dim am As AlertMessage = New AlertMessage(msg, logLevel.CRITICAL, "VoiceTests")
        ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf am.logAlertMessage))
        Console.ReadLine()
    End Sub

    Private Sub handleMessageChunksTest()
        Dim chunk1 As String = "Rcv: PD-LTB01-NJ^MissionControl^^^ITEMSTATUS^1604^{""MT"":""ITEMSTATUS"",""CartID"":""PPKOHLS33-355884"",""OrderID"":""00000530050626287626"",""LineID"":""WRP8535772"",""Location"":""KF2024"",""RequiredQuantity"":""3"",""ActualQuantity"":""3"",""Date"":""07/30/2015"",""Time"":""16:45:10"",""OperatorID"":""PremlataP"",""OperatorDescription"":""Premlata Patel"",""State"":"" COMPLETED""}PD-LTB01-NJ^MissionControl^^^ITEMSTATUS^1604^{""MT"":""ITEMSTATUS"",""CartID"":""PPKOHLS33-355884"",""OrderID"":""00000530050626287626"",""LineID"":""WRP8535772"",""Location"":""KF2024"",""RequiredQuantity"":""3"",""ActualQuantity"":""3"",""Date"":""07/30/2015"",""Time"":""16:45:10"",""OperatorID"":""PremlataP"",""OperatorDescription"":""Premlata Patel"",""State"":"" COMPLETED""}PremlataP^VOICE^^^OPERATORSTATUS^^{""OperatorID"": ""PremlataP"", ""TalkmanSerialNumber"": ""6314250059"", ""Date"": ""07/30/2015"", ""Time"": ""16:45:17"", ""Status"": ""LOGOFF"", ""Zone"": ""Kohls"", ""MT"": ""OPERATORSTATUS""}PD-LTB01-NJ^MissionControl^^^ITEMSTATUS^1604^{""MT"":""ITEMSTATUS"",""CartID"":""PPKOHLS33-355884"",""OrderID"":""00000530050626287626"",""LineID"":""WRP8535772"",""L"
        Dim chunk2 As String = "ocation"":""KF2024"",""RequiredQuantity"":""3"",""ActualQuantity"":""3"",""Date"":""07/30/2015"",""Time"":""16:45:10"",""OperatorID"":""PremlataP"",""OperatorDescription"":""Premlata Patel"",""State"":"" COMPLETED""}PD-LTB01-NJ^MissionControl^^^ITEMSTATUS^1605^{""MT"":""ITEMSTATUS"",""CartID"":""PPKOHLS33-355884"",""OrderID"":""00000530050626287541"",""LineID"":""WRP8535686"",""Location"":""KF2024"",""RequiredQuantity"":""3"",""ActualQuantity"":""3"",""Date"":""07/30/2015"",""Time"":""16:45:10"",""OperatorID"":""PremlataP"",""OperatorDescription"":""Premlata Patel"",""State"":"" COMPLETED""}"
        'Dim chunk3 As String = "ETED""}"

        Console.WriteLine(chunk1)
        Console.WriteLine(chunk2)
        'Console.WriteLine(chunk3)

        Console.ReadLine()

        Dim simulationQ As Queue(Of String) = New Queue(Of String)
        simulationQ.Enqueue(chunk1)
        simulationQ.Enqueue(chunk2)
        'simulationQ.Enqueue(chunk3)

        Dim messageQ As Queue(Of String) = New Queue(Of String)
        Dim leftOverMessage As String = ""

        ' The receive would be here
        'pickDirector.receive(1024)

        ' This should be a function
        Do           
            Dim message As String = simulationQ.Dequeue           
            Dim noMoreChunks As Boolean = False
            If leftOverMessage.Length > 0 Then
                message = (leftOverMessage + message).Trim
                leftOverMessage = ""
            End If

            Do
                If message.StartsWith(Chr(2)) Then
                    If message.Contains(Chr(3)) Then
                        Dim termPos As Integer = message.IndexOf(Chr(3))
                        If message.Substring(0, termPos + 1).StartsWith(Chr(2)) And message.Substring(0, termPos + 1).EndsWith(Chr(3)) Then
                            messageQ.Enqueue(message.Substring(0, termPos + 1))
                        Else
                            Console.WriteLine("Warning: Message is not a message")
                        End If
                        message = message.Substring(termPos + 1)
                        If message.Length = 0 Then
                            noMoreChunks = True
                        End If
                    Else
                        noMoreChunks = True
                    End If
                    Else
                        Console.WriteLine("Warning: chunk did not start with token")
                        Console.WriteLine("W: " + message)
                        noMoreChunks = True
                    End If
            Loop Until noMoreChunks

            If message.Length > 0 Then
                leftOverMessage = message
                message = Nothing
            End If

            Console.WriteLine("Message Simulation")
            Console.WriteLine("MsgQSize: " + messageQ.Count.ToString)
            For i As Integer = 1 To messageQ.Count
                Console.WriteLine(messageQ.Dequeue)
                Thread.Sleep(500)
            Next

            Console.WriteLine("LeftOver: " + leftOverMessage)
            Console.WriteLine("MsgQSize: " + messageQ.Count.ToString)
        Loop Until simulationQ.Count = 0

        Console.ReadLine()
    End Sub

    Private Sub checkDropCartRequest()
        Dim x As PickDirectorMessageParser = New DropCartRequest("SandraR^VOICE^^^DROPCARTREQUEST^^{""CartID"": ""FRED10-312851"", ""LastPickLoc"": ""D1451E"", ""OperatorID"": ""SandraR"", ""MT"": ""DROPCARTREQUEST""}PD-LTB-NJ^MissionControl^^^ITEMSTATUS^12019^{""MT"":""ITEMSTATUS"",""CartID"":""DTCGO21-312718"",""OrderID"":""00000530050593892571"",""LineID"":""WRP3369440"",""Location"":""KC5103A"",""RequiredQuantity"":""1"",""ActualQuantity"":""1"",""Date"":""01/13/2015"",""Time"":""07:56:43"",""OperatorID"":""DulceP"",""OperatorDescription"":""Dulce Peralta"",""State"":"" COMPLETED""}")

        For Each widget As String In x.messageDictionary.Keys
            Console.WriteLine("TPF-{0}: {1}", widget, x.messageDictionary.Item(widget))
        Next

        Console.WriteLine("{0}BODY", vbCrLf)
        For Each widget As String In x.messageBodyDictionary.Keys
            Console.WriteLine("TPF-{0}: {1}", widget, x.messageBodyDictionary.Item(widget))
        Next

        Dim y As PickDirectorMessageHandler = New DropCartResponse(x)
        'y.processMessage()

        Console.WriteLine("{0}YBODY", vbCrLf)
        For Each widget As String In y.messageBodyParameterQueue
            Console.WriteLine("{0}: {1}", widget, widget)
        Next

        Console.WriteLine("RMSG: {0}", y.returnMessage)
        Console.ReadLine()
    End Sub

    Private Sub testMultipleGlomMessage()
        Dim mgm As String = "VC_VoiceApp^VC_VOICE^^^ITEMSTATUS^^{""MT"":""ITEMSTATUS"",""CartID"":""PPKOHLS09-092373"",""OrderID"":""00000530050423876368"",""LineID"":""WRL4089235"",""Location"":""KZ2089A"",""RequiredQuantity"":""3"",""ActualQuantity"":""0"",""Date"":""07/08/2014"",""Time"":""14_13_47"",""OperatorID"":""TylerKelsey"",""OperatorDescription"":""TylerKelsey"",""State"":""SKIPSLOT""}VC_VoiceApp^VC_VOICE^^^ITEMSTATUS^^{""MT"":""ITEMSTATUS"",""CartID"":""PPKOHLS09-092373"",""OrderID"":""00000530050423876368"",""LineID"":""WRL4089235"",""Location"":""KZ2089A"",""RequiredQuantity"":""3"",""ActualQuantity"":""0"",""Date"":""07/08/2014"",""Time"":""14_13_47"",""OperatorID"":""TylerKelsey"",""OperatorDescription"":""TylerKelsey"",""State"":""SKIPSLOT""}VC_VoiceApp^VC_VOICE^^^ITEMSTATUS^^{""MT"":""ITEMSTATUS"",""CartID"":""PPKOHLS09-092373"",""OrderID"":""00000530050423876429"",""LineID"":""WRL4089247"",""Location"":""KZ2089A"",""RequiredQuantity"":""3"",""ActualQuantity"":""0"",""Date"":""07/08/2014"",""Time"":""14_13_47"",""OperatorID"":""TylerKelsey"",""OperatorDescription"":""TylerKelsey"",""State"":""SKIPSLOT""}VC_VoiceApp^VC_VOICE^^^ITEMSTATUS^^{""MT"":""ITEMSTATU"
        Dim startPos As Integer = 0
        Dim flg As Boolean = True

        Do
            Dim pdMessage = getPDMessageFromString(mgm)
            If pdMessage IsNot Nothing AndAlso Not pdMessage.Equals(String.Empty) Then
                Dim p As PickDirectorMessageParser = PickDirectorMessageParserFactory.getPickDirectorParser(pdMessage)
                For Each widget As String In p.messageDictionary.Keys
                    Console.WriteLine("{0}: {1}", widget, p.messageDictionary.Item(widget))
                Next

                Console.ReadLine()

            Else
                flg = False
            End If
        Loop Until Not flg
    End Sub

    Private Function getPDMessageFromString(ByRef msg As String) As String
        Dim sp As Integer = 0
        Dim ep As Integer = 0
        Dim pdm As String = String.Empty

        If msg.Contains(Chr(2)) AndAlso msg.Contains(Chr(3)) Then
            sp = msg.IndexOf(Chr(2))
            ep = msg.IndexOf(Chr(3)) + 1

            pdm = msg.Substring(sp, ep)
            msg = msg.Substring(ep)
        End If

        Return pdm
    End Function

    Private Sub checkCartonStatus()
        Dim x As PickDirectorMessageParser = New CartonStatus("VoiceApp^VOICE^^^CARTONSTATUS^^{""OrderID"": ""00000530050423876108"", ""Status"": ""REMOVED"", ""MT"": ""CARTONSTATUS""}")

        For Each widget As String In x.messageDictionary.Keys
            Console.WriteLine("TPF-{0}: {1}", widget, x.messageDictionary.Item(widget))
        Next

        Console.WriteLine("{0}BODY", vbCrLf)
        For Each widget As String In x.messageBodyDictionary.Keys
            Console.WriteLine("TPF-{0}: {1}", widget, x.messageBodyDictionary.Item(widget))
        Next

        Dim y As PickDirectorMessageHandler = New CartonStatusHandler(x)
        'y.processMessage()

        'Console.WriteLine("{0}YBODY", vbCrLf)
        'For Each widget As String In y.messageBodyParameterQueue
        '    Console.WriteLine("{0}: {1}", widget, widget)

        'Next

        Console.WriteLine("RMSG: {0}", y.returnMessage)
        Console.ReadLine()
    End Sub

    Private Sub checkCartStatus()
        Dim x As PickDirectorMessageParser = New CartStatus("PD-LTB-NJ^MADFilter^VoiceApp^VOICE^CARTSTATUS^^{""CartID"": ""PPKOHLS04-092366"", ""Date"": ""06/27/2014"", ""Time"": ""09:22:38"", ""OperatorID"": ""TylerKelsey"", ""Type"": ""AUTOASSIGNED"", ""DropOffLocation"": """", ""MT"": ""CARTSTATUS""}")

        For Each widget As String In x.messageDictionary.Keys
            Console.WriteLine("TPF-{0}: {1}", widget, x.messageDictionary.Item(widget))
        Next

        Console.WriteLine("{0}BODY", vbCrLf)
        For Each widget As String In x.messageBodyDictionary.Keys
            Console.WriteLine("TPF-{0}: {1}", widget, x.messageBodyDictionary.Item(widget))
        Next

        Dim y As PickDirectorMessageHandler = New CartStatusHandler(x)
        'y.processMessage()

        'Console.WriteLine("{0}YBODY", vbCrLf)
        'For Each widget As String In y.messageBodyParameterQueue
        '    Console.WriteLine("{0}: {1}", widget, widget)

        'Next

        Console.WriteLine("RMSG: {0}", y.returnMessage)
        Console.ReadLine()
    End Sub

    Private Sub checkCancelStatus()
        Dim x As PickDirectorMessageParser = New CancelStatus("VoiceApp^VOICE^^^CARTSTATUS^^{""CartID"": ""PPKOHLS04-092366"", ""Date"": ""06/27/2014"", ""Time"": ""09:22:38"", ""OperatorID"": ""TylerKelsey"", ""Type"": ""AUTOASSIGNED"", ""DropOffLocation"": "", ""MT"": ""CARTSTATUS""}")

        For Each widget As String In x.messageDictionary.Keys
            Console.WriteLine("TPF-{0}: {1}", widget, x.messageDictionary.Item(widget))
        Next

        Console.WriteLine("{0}BODY", vbCrLf)
        For Each widget As String In x.messageBodyDictionary.Keys
            Console.WriteLine("TPF-{0}: {1}", widget, x.messageBodyDictionary.Item(widget))
        Next

        Dim y As PickDirectorMessageHandler = New CancelStatusHandler(x)
        'y.processMessage()

        Console.WriteLine("{0}YBODY", vbCrLf)
        For Each widget As String In y.messageBodyParameterQueue
            Console.WriteLine("{0}: {1}", widget, widget)

        Next

        Console.WriteLine("RMSG: {0}", y.getPlainReturnMessage)
        Console.ReadLine()
    End Sub

    Private Sub checkFastAck(ByVal msg As String)
        Dim x As PickDirectorMessageParser = PickDirectorMessageParserFactory.getPickDirectorParser(msg)
        Console.WriteLine("TESTMSGTYPE: {0}", x.messageType)

        Console.WriteLine("SNX: {0}", x.sequenceNumber)

        For Each widget As String In x.messageDictionary.Keys
            Console.WriteLine("TPF-{0}: {1}", widget, x.messageDictionary.Item(widget))
        Next

        Console.WriteLine("{0}BODY", vbCrLf)
        For Each widget As String In x.messageBodyDictionary.Keys
            Console.WriteLine("TPF-{0}: {1}", widget, x.messageBodyDictionary.Item(widget))
        Next

        Console.WriteLine("MMS: {0}", x.getMocaMessageString)

        Dim h As PickDirectorMessageHandler = PickDirectorMessageHandlerFactory.getPickDirectorMessageHandler(x)

        'h.processMessage(x.requiresFastAck)

        Console.WriteLine("FASTACK: {0}", x.requiresFastAck)
        Console.WriteLine("RMSG: {0}", h.returnMessage)

        Console.ReadLine()
    End Sub

    Private Sub testPickDownloadStandAlone()
        Try
            Dim msg As String = "PublisherLocation^PublisherType^SubscriberLocation^SubscriberType^PICKDOWNLOAD^^{""MT"":""PICKDOWNLOAD"",""CartID"":""PPKOHLS08""}"
            Dim md As Dictionary(Of String, Dictionary(Of String, String)) = Nothing
            Dim lines As Integer = 0

            Dim x As PickDirectorMessageParser = PickDirectorMessageParserFactory.getPickDirectorParser(msg)
            Console.WriteLine("TESTMSGTYPE: {0}", x.messageType)

            Console.WriteLine("SNX: {0}", x.sequenceNumber)

            For Each widget As String In x.messageDictionary.Keys
                Console.WriteLine("TPF-{0}: {1}", widget, x.messageDictionary.Item(widget))
            Next

            Console.WriteLine("{0}BODY", vbCrLf)
            For Each widget As String In x.messageBodyDictionary.Keys
                Console.WriteLine("TPF-{0}: {1}", widget, x.messageBodyDictionary.Item(widget))
            Next

            Console.WriteLine("MMS: {0}", x.getMocaMessageString)

            Dim h As PickDirectorMessageHandler = PickDirectorMessageHandlerFactory.getPickDirectorMessageHandler(x)
            'h.processMessage()

            md = h.returnData

            Try
                lines = Integer.Parse(md.Item("VOICE CMD RESULTS").Item("LINES"))
            Catch ex As Exception
                EventLog.WriteEntry("LTBMMDAServiceSocket", ex.ToString, _
                                   EventLogEntryType.Information, 200, 200)
                lines = 0
            End Try

            If lines = 0 Then
                Console.WriteLine("ERROR: No Data")
                Console.ReadLine()
            Else
                For i As Integer = 1 To lines
                    Dim idx As String = "MOCA_" & i.ToString
                    Console.WriteLine(PickDownload.getReturnStringFromDictionary(h.getMainTemplate, md.Item(idx)))

                    Dim id As String = "N/A"

                    Try
                        id = md.Item(idx).Item("line_id")
                    Catch ex As Exception
                    End Try
                Next

                Console.WriteLine("ACK: " & lines & " Lines Sent to Pick Director!")
                Console.ReadLine()
            End If
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
            Console.ReadLine()
        End Try
    End Sub

    Private Sub checkParserFactory(ByVal msg As String)
        Dim x As PickDirectorMessageParser = PickDirectorMessageParserFactory.getPickDirectorParser(msg)
        Console.WriteLine("TESTMSGTYPE: {0}", x.messageType)

        Console.WriteLine("SNX: {0}", x.sequenceNumber)

        For Each widget As String In x.messageDictionary.Keys
            Console.WriteLine("TPF-{0}: {1}", widget, x.messageDictionary.Item(widget))
        Next

        Console.WriteLine("{0}BODY", vbCrLf)
        For Each widget As String In x.messageBodyDictionary.Keys
            Console.WriteLine("TPF-{0}: {1}", widget, x.messageBodyDictionary.Item(widget))
        Next

        Console.ReadLine()

        Console.WriteLine("MMS: {0}", x.getMocaMessageString)

        Dim h As PickDirectorMessageHandler = PickDirectorMessageHandlerFactory.getPickDirectorMessageHandler(x)
        Console.WriteLine("Handler: {0}", h.messageType)

        Console.WriteLine("RMSG: {0}", h.returnMessage)

        Console.ReadLine()
    End Sub

    Private Sub runTests()
        Dim results As Dictionary(Of String, Dictionary(Of String, String))

        For Each test As BaseVoiceCommandTest In tests

            results = test.runTest

            For Each widget As String In results.Keys
                Dim d As Dictionary(Of String, String) = results.Item(widget)

                Console.Write("{0}: ", widget)
                For Each widgetx As String In d.Keys()
                    Console.WriteLine("{0}: {1}", widgetx, d.Item(widgetx))
                Next
            Next

            Console.ReadLine()
        Next
    End Sub

    Private Sub loadTests()
        Dim bvct As BaseVoiceCommandTest
        Dim testData As Dictionary(Of String, String)

        testData = New Dictionary(Of String, String)
        bvct = New NextCartRequestTest
        testData.Add("Area", "KOHLS")
        bvct.supplyTestData(testData)

        tests.Add(bvct)

        bvct = New CartStatusTest
        testData = New Dictionary(Of String, String)
        testData.Add("CartID", "1234567890")
        bvct.supplyTestData(testData)

        tests.Add(bvct)
    End Sub


    Private Sub runCancelRequestStandAloneTest()
        Dim test As BaseVoiceCommandTest = New CancelRequestTest
        Dim testData As Dictionary(Of String, String) = New Dictionary(Of String, String)
        Dim template As Dictionary(Of String, String) = New Dictionary(Of String, String)

        testData.Add("CancelType", "CART")
        testData.Add("CancelID", "PPKOHLS04")
        test.supplyTestData(testData)

        Console.Write("MSG: {0}", test.getFormattedMessage)
        Dim pdp As PickDirectorMessageParser = New CancelRequest(test.getFormattedMessage)

        For Each widget As String In pdp.messageDictionary.Keys
            Console.WriteLine("{0}: {1}", widget, pdp.messageDictionary.Item(widget))
        Next

        Console.ReadLine()
        Console.Clear()

        Dim pdh As PickDirectorMessageHandler = New CancelRequestHandler(pdp)
        'pdh.processMessage()

        For Each dKey As String In pdh.returnData.Keys
            For Each dVal As String In pdh.returnData.Item(dKey).Keys
                Console.WriteLine("{0}-{1}: {2}", dKey, dVal, pdh.returnData.Item(dKey).Item(dVal))
            Next
        Next

        Console.WriteLine("MSG: {0}", pdh.returnMessage)
        Console.ReadLine()
    End Sub

    Private Sub runStandAloneHeartbeatTest()
        Dim test As BaseVoiceCommandTest = New HeartbeatTest
        Dim testData As Dictionary(Of String, String) = New Dictionary(Of String, String)
        Dim template As Dictionary(Of String, String) = New Dictionary(Of String, String)

        test.supplyTestData(testData)

        Console.Write("MSG: {0}", test.getFormattedMessage)
        Dim pdp As PickDirectorMessageParser = New HeartbeatParser(test.getFormattedMessage)

        For Each widget As String In pdp.messageDictionary.Keys
            Console.WriteLine("{0}: {1}", widget, pdp.messageDictionary.Item(widget))
        Next

        Console.ReadLine()
        Console.Clear()

        Dim pdh As PickDirectorMessageHandler = New HeartbeatHandler(pdp)
        'pdh.processMessage()

        For Each dKey As String In pdh.returnData.Keys
            For Each dVal As String In pdh.returnData.Item(dKey).Keys
                Console.WriteLine("{0}-{1}: {2}", dKey, dVal, pdh.returnData.Item(dKey).Item(dVal))
            Next
        Next

        Console.WriteLine("MSG: {0}", pdh.returnMessage)
        Console.ReadLine()
    End Sub

    Private Sub runDropCartResponseStandAloneTest()
        Dim test As BaseVoiceCommandTest = New DropCartRequestTest
        Dim testData As Dictionary(Of String, String) = New Dictionary(Of String, String)
        Dim template As Dictionary(Of String, String) = New Dictionary(Of String, String)

        testData.Add("CartID", "PPKOHLS02-092361")
        testData.Add("LastPickLoc", "KZ1078A")
        test.supplyTestData(testData)

        Console.Write("MSG: {0}", test.getFormattedMessage)
        Dim pdp As PickDirectorMessageParser = New DropCartRequest(test.getFormattedMessage)

        For Each widget As String In pdp.messageDictionary.Keys
            Console.WriteLine("{0}: {1}", widget, pdp.messageDictionary.Item(widget))
        Next

        Console.ReadLine()
        Console.Clear()

        Dim pdh As PickDirectorMessageHandler = New DropCartResponse(pdp)
        'pdh.processMessage()

        For Each dKey As String In pdh.returnData.Keys
            For Each dVal As String In pdh.returnData.Item(dKey).Keys
                Console.WriteLine("{0}-{1}: {2}", dKey, dVal, pdh.returnData.Item(dKey).Item(dVal))
            Next
        Next

        Console.WriteLine("MSG: {0}", pdh.returnMessage)
        Console.ReadLine()
    End Sub

    Private Sub runNextCartManual()
        Dim x As PickDirectorMessageParser = New NextCartRequest("S1PICK2^VOICE^^^NEXTCARTREQUEST^^{""Zone"": ""MIKASA"", ""MT"": ""NEXTCARTREQUEST"", ""OperatorID"": ""S1PICK2""}^")

        For Each widget As String In x.messageDictionary.Keys
            Console.WriteLine("TPF-{0}: {1}", widget, x.messageDictionary.Item(widget))
        Next

        Console.WriteLine("MBODYKEYS: {0}", x.messageBodyKeys.Length)
        For Each widget As Object In x.messageBodyKeys
            Console.WriteLine("W: {0}", widget)
        Next

        Console.WriteLine("{0}BODY", vbCrLf)

        For Each widget As String In x.messageBodyDictionary.Keys
            Console.WriteLine("TPF-{0}: {1}", widget, x.messageBodyDictionary.Item(widget))
        Next

        Console.WriteLine("MMSTR: {0}", x.getMocaMessageString())

        Console.WriteLine("Press Enter to Run")
        Console.ReadLine()

        Dim y As PickDirectorMessageHandler = New CartStatusResponse(x)
        'y.processMessage()

        For Each dKey As String In y.returnData.Keys
            For Each dVal As String In y.returnData.Item(dKey).Keys
                Console.WriteLine("{0}-{1}: {2}", dKey, dVal, y.returnData.Item(dKey).Item(dVal))
            Next
        Next

        Console.WriteLine("RMSG: {0}", y.returnMessage)
        Console.ReadLine()
    End Sub

    Private Sub runNewCartStatusStandAloneTest()
        Dim x As PickDirectorMessageParser = New CartStatus("PD-LTB-NJ^MADFilter^VoiceApp^VOICE^CARTSTATUS^^{""CartID"": ""PPKOHLS04-092366"", ""Date"": ""06/27/2014"", ""Time"": ""09:22:38"", ""OperatorID"": ""TestOper"", ""Type"": ""AUTOASSIGNED"", ""DropOffLocation"": """", ""MT"": ""CARTSTATUS"", ""LineCount"": ""5""}")

        For Each widget As String In x.messageDictionary.Keys
            Console.WriteLine("TPF-{0}: {1}", widget, x.messageDictionary.Item(widget))
        Next

        Console.WriteLine("MBODYKEYS: {0}", x.messageBodyKeys.Length)
        For Each widget As Object In x.messageBodyKeys
            Console.WriteLine("W: {0}", widget)
        Next

        Console.WriteLine("{0}BODY", vbCrLf)        

        For Each widget As String In x.messageBodyDictionary.Keys
            Console.WriteLine("TPF-{0}: {1}", widget, x.messageBodyDictionary.Item(widget))
        Next

        Console.WriteLine("MMSTR: {0}", x.getMocaMessageString())

        Console.WriteLine("Press Enter to Run")
        Console.ReadLine()

        Dim y As PickDirectorMessageHandler = New CartStatusResponse(x)
        'y.processMessage()

        For Each dKey As String In y.returnData.Keys
            For Each dVal As String In y.returnData.Item(dKey).Keys
                Console.WriteLine("{0}-{1}: {2}", dKey, dVal, y.returnData.Item(dKey).Item(dVal))
            Next
        Next

        Console.WriteLine("RMSG: {0}", y.returnMessage)
        Console.ReadLine()
    End Sub

    Private Sub runPickDownloadACKStandAloneTest()
        Dim x As PickDirectorMessageParser = New PickDownloadACKParser("PD-LTB-NJ^MADFilter^VoiceApp^VOICE^PICKDOWNLOADACK^^{""CartID"": ""CART144-347425"", ""LineID"": ""WRP7423219"", ""MT"": ""PICKDOWNLOAD""")

        For Each widget As String In x.messageDictionary.Keys
            Console.WriteLine("TPF-{0}: {1}", widget, x.messageDictionary.Item(widget))
        Next

        Console.WriteLine("MBODYKEYS: {0}", x.messageBodyKeys.Length)
        For Each widget As Object In x.messageBodyKeys
            Console.WriteLine("W: {0}", widget)
        Next

        Console.WriteLine("{0}BODY", vbCrLf)
        For Each widget As String In x.messageBodyDictionary.Keys
            Console.WriteLine("TPF-{0}: {1}", widget, x.messageBodyDictionary.Item(widget))
        Next

        Console.WriteLine("MMSTR: {0}", x.getMocaMessageString())


        Console.WriteLine("Press Enter to Run")
        Console.ReadLine()

        Dim y As PickDirectorMessageHandler = New PickDownloadACKHandler(x)
        'y.processMessage()

        For Each dKey As String In y.returnData.Keys
            For Each dVal As String In y.returnData.Item(dKey).Keys
                Console.WriteLine("{0}-{1}: {2}", dKey, dVal, y.returnData.Item(dKey).Item(dVal))
            Next
        Next

        Console.WriteLine("RMSG: {0}", y.returnMessage)
        Console.ReadLine()
    End Sub

    Private Sub runNextCartStandAloneTest()
        Dim test As BaseVoiceCommandTest = New NextCartRequestTest
        Dim testData As Dictionary(Of String, String) = New Dictionary(Of String, String)
        Dim template As Dictionary(Of String, String) = New Dictionary(Of String, String)

        testData.Add("Area", "KOHLS")
        test.supplyTestData(testData)

        Dim pdp As PickDirectorMessageParser = New NextCartRequest(test.getFormattedMessage)
        Dim pdh As PickDirectorMessageHandler = New NextCartResponse(pdp)
        'pdh.processMessage()

        For Each dKey As String In pdh.returnData.Keys
            For Each dVal As String In pdh.returnData.Item(dKey).Keys
                Console.WriteLine("{0}-{1}: {2}", dKey, dVal, pdh.returnData.Item(dKey).Item(dVal))
            Next
        Next

        Console.WriteLine("MSG: {0}", pdh.returnMessage)

        Console.ReadLine()
    End Sub

    Private Sub runPickDownLoadStandAloneTest()
        Dim test As BaseVoiceCommandTest = New PickDownLoadTest
        Dim testData As Dictionary(Of String, String) = New Dictionary(Of String, String)
        testData.Add("CartID", "PPKOHLS05")
        test.supplyTestData(testData)


        Dim pdp As PickDirectorMessageParser = New PickDownloadParser(test.getFormattedMessage)
        Dim splitMessageBody As String() = pdp.messageDictionary.Item("MessageBody").Split(",")
        Console.WriteLine("MSG: {0}", pdp.messageDictionary.Item("MessageBody"))
        Console.WriteLine("COUNT: {0}", splitMessageBody.Count)
        For i As Integer = 0 To pdp.messageBodyKeys.Count - 1
            Try
                Dim wv As String = splitMessageBody(i)

                If wv.Contains(":") Then
                    wv = wv.Substring(wv.IndexOf(":"))
                End If

                wv = wv.Replace("{", "").Replace("""", "").Replace("}", "").Replace(":", "")

                Console.WriteLine("{2}: {0}: {1}", pdp.messageBodyKeys(i), wv, wv.Length)
            Catch ex As Exception
                Console.WriteLine("ER: {0}", ex.Message)
            End Try
        Next

        For Each widget As String In pdp.messageDictionary.Keys
            Console.WriteLine("{0}: {1}", widget, pdp.messageDictionary.Item(widget))
        Next
        Console.ReadLine()
        Console.Clear()

        Dim pdh As PickDirectorMessageHandler = New PickDownload(pdp)
        'pdh.processMessage()

        Dim x As Dictionary(Of String, Dictionary(Of String, String)) = pdh.returnData
        Console.WriteLine("Template: {0}", pdh.getMainTemplate)
        Dim lines As Integer = Integer.Parse(x.Item("VOICE CMD RESULTS").Item("LINES"))

        For i As Integer = 1 To lines
            Dim idx As String = "MOCA_" & i
            Dim st As String = String.Empty

            st = PickDownload.getReturnStringFromDictionary(pdh.getMainTemplate, x.Item(idx))

            Console.WriteLine("LINE-{0}: {1}", i, st)
            Console.ReadLine()
        Next


        Console.ReadLine()
    End Sub

    Private Sub connectToVoice()
        Dim s As Socket = Nothing

    End Sub
End Module
