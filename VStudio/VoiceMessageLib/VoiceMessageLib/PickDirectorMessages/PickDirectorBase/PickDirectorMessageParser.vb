﻿Public MustInherit Class PickDirectorMessageParser
    Inherits BaseMessageParser

    Public Enum messageTypesIn
        NEXTCARTREQUEST
        PICKDOWNLOAD
        PICKDOWNLOAD_ACK
        DROPCARTSCAN
        ITEMSTATUS
        SOCKETMONITOR
        CARTONSTATUS
        CANCELSTATUS
        CARTSTATUS
        OPERATORSTATUS
        CANCELREQUEST
        DROPCARTREQUEST
        UNIMPLIMENTED
        HEARTBEAT
        INVALID
        ACK
        NACK
    End Enum

    ' message types
    Public messageTypeIn As messageTypesIn

    Public Property stx() As String = Chr(2)
    Public Property etx() As String = Chr(3)

    Public Overridable Property passThroughMessage As Boolean = False
    Public Overridable Property requiresFastAck As Boolean = False
    Public Overridable Property requiresReturnMsg As Boolean = True
    Public Overridable Property subscriberLocation() As String = serviceLocationGlobal
    Public Overridable Property subscriberType() As String = serviceTypeGlobal
    Public Overridable Property sequenceNumber() As String = String.Empty
    Public Overridable Property bodyDelimiter() As String = ","
    Public Property messageBodyKeys() As String()
    Public Property messageBodyDictionary() As Dictionary(Of String, String) = New Dictionary(Of String, String)
    Public Overridable Property publisherLocation()
    Public Overridable Property publisherType() As String

    Public Overrides ReadOnly Property delimiter() As String
        Get
            Return "^"
        End Get
    End Property

    Protected Overrides Function loadMessageKeys() As String()
        Dim mk As String() = {"STX", "PublisherLocation", "PublisherType", "SubscriberLocation", _
                              "SubscriberType", "MessageType", "SequenceNumber", "MessageBody", "ETX"}
        Return mk
    End Function

    Protected Overrides Sub parseMessage()
        Dim splitMessage As String() = baseMessage.Split(delimiter)
        Dim mb As String = String.Empty

        For i As Integer = 1 To splitMessage.Length
            Dim ival As String = splitMessage(i - 1)
            If ival.StartsWith(Chr(2)) Then
                ival = ival.Substring(1)
                messageDictionary.Add("STX", Chr(2))
            End If

            If ival.EndsWith(Chr(3)) Then
                ival = ival.Substring(0, ival.Length - 1)
                messageDictionary.Add("ETX", Chr(3))
            End If

            Select Case i
                Case 1
                    publisherLocation = ival
                    messageDictionary.Add("PublisherLocation", ival)
                Case 2
                    publisherType = ival
                    messageDictionary.Add("PublisherType", ival)
                Case 3
                    messageDictionary.Add("SubscriberLocation", ival)
                Case 4
                    messageDictionary.Add("SubscriberType", ival)
                Case 5
                    messageDictionary.Add("MessageType", ival)
                Case 6
                    messageDictionary.Add("SequenceNumber", ival)
                    sequenceNumber = ival
                Case Else
                    If mb = String.Empty Then
                        mb = ival
                    Else
                        mb &= delimiter & ival
                    End If
            End Select
        Next

        messageDictionary.Add("MessageBody", mb)
    End Sub

    Private Sub parseMessageBody()

        Try
            Dim splitMessageBody As String() = messageDictionary.Item("MessageBody").Split(bodyDelimiter)
            For i As Integer = 0 To _messageBodyKeys.Count - 1
                Try
                    Dim wv As String = splitMessageBody(i)
                    Dim kv As String = splitMessageBody(i)
                    If wv.Contains(":") Then
                        wv = wv.Substring(wv.IndexOf(":"))
                        kv = kv.Substring(1, kv.IndexOf(":") - 1)
                    End If

                    wv = wv.Replace("{", "").Replace("""", "").Replace("}", "").Replace(":", "")
                    kv = kv.Replace("{", "").Replace("""", "").Replace("}", "").Replace(":", "")

                    messageBodyDictionary.Add(kv.Trim, wv.Trim)
                    messageDictionary.Add("MB_" & kv.Trim, wv.Trim)
                Catch ex As Exception
                    ' TODO: Handle Error                
                End Try
            Next
        Catch ex As Exception
        End Try
    End Sub

    Public Function getMocaMessageString() As String
        Dim str As String = "EMPTY"

        Try
            For Each widget As Object In messageBodyKeys
                If str.Equals("EMPTY") Then
                    str = messageBodyDictionary.Item(widget)
                Else
                    str = str & "," & messageBodyDictionary.Item(widget)                    
                End If

                If str.Contains(Chr(3)) Then
                    str = str.Substring(0, str.IndexOf(Chr(3)))
                End If
            Next
        Catch ex As Exception
            str = ex.ToString
        End Try

        Return str
    End Function

    ' Must Override Items
    Public MustOverride Property messageType() As String
    Protected MustOverride Function loadMessageBodyKeys() As String()

    ' Constructor
    Sub New(ByVal msg As String)
        MyBase.New(msg)

        messageBodyKeys = loadMessageBodyKeys()
        parseMessageBody()
    End Sub
End Class
