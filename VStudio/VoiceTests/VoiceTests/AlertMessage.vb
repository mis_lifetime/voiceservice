﻿Public Class AlertMessage

    Public Property msg As String
    Public Property ttl As String
    Public Property lvl As Integer
    Public Property dte As Date
    Public Property fmtDte As String

    Public Sub logAlertMessage()
        Select Case socketLogMode
            Case logMode.ALL
                If eventLogLevel <= lvl Then LTBMMDAEventLog.WriteEntry(msg)
                If dbLogLevel <= lvl Then VoiceMessageLibDEV.logMessage(msg, ttl, fmtDte)
            Case logMode.DCSONLY
                If dbLogLevel <= lvl Then VoiceMessageLibDEV.logMessage(msg, ttl, fmtDte)
            Case logMode.EVENTONLY
                If eventLogLevel <= lvl Then LTBMMDAEventLog.WriteEntry(msg)
        End Select
    End Sub

    Overrides Function toString() As String
        Return msg & " " & ttl & " " & fmtDte
    End Function

    Sub New(ByVal m As String, ByVal l As Integer, Optional ByVal t As String = "INFO")
        msg = m
        lvl = l
        ttl = t
        dte = Now
        fmtDte = Format(dte, "yyyyMMddHHmmssfff")
    End Sub
End Class
