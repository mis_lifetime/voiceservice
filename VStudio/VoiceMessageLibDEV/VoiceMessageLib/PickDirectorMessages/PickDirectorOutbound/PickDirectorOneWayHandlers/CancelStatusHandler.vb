﻿Public Class CancelStatusHandler
    Inherits PickDirectorMessageHandler

    Public Overrides ReadOnly Property messageType As String
        Get
            Return "CANCELSTATUSHANDLER"
        End Get
    End Property

    Protected Overrides Function loadMocaCommandParameters() As System.Collections.Generic.Queue(Of String)
        Dim queueOfParms As Queue(Of String) = New Queue(Of String)
        queueOfParms.Enqueue(messageType)
        queueOfParms.Enqueue(pickDirectorBaseMessage.getMocaMessageString)

        Return queueOfParms
    End Function

    Protected Overrides Function loadMessageBodyTemplate() As String
        Return "{0},{1}"
    End Function

    Protected Overrides Function loadMessageBodyParameterQueue() As System.Collections.Generic.Queue(Of String)
        Dim cncId As String = String.Empty
        Dim stt As String = String.Empty
        Dim msgQ As Queue(Of String) = New Queue(Of String)
        Try
            cncId = baseMessage.messageDictionary.Item("MB_DeleteID")
        Catch
        End Try

        Try
            stt = baseMessage.messageDictionary.Item("MB_State")
        Catch
        End Try

        msgQ.Enqueue(cncId)
        msgQ.Enqueue(stt)

        Return msgQ
    End Function

    Sub New(ByVal msgIn As DelimitedMessageParser)
        MyBase.New(msgIn)
    End Sub
End Class
