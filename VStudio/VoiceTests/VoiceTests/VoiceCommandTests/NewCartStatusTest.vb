﻿Public Class NewCartStatusTest
    Inherits BaseVoiceCommandTest

    Protected Overrides ReadOnly Property messageBodyTemplate As String
        Get
            Return "{""MT"":""{0}"",""CartID"":""{1}"",""Date"":""{2}"",""Time"":""{3}"",""OperatorID"":""{4}"",""Type"":""{5}"",""DropOffLocation"":""{6}"",""LineCount"":""{7}""}"
        End Get
    End Property

    Protected Overrides Property testName As String = "New Cart Status Test"
    Protected Overrides Property testMessageType As String = "CARTSTATUS"
    Private Property messageBodyDictionary As Dictionary(Of String, String) = New Dictionary(Of String, String)
    Private Property mt As String
    Private Property zone As String
    Private Property operatorId As String

    Protected Overrides Function getMessageBodyDataParameters() As Queue(Of String)
        Dim q As Queue(Of String) = New Queue(Of String)
        q.Enqueue("CARTSTATUS")
        q.Enqueue("DTCGO50")
        q.Enqueue("08/07/2015")
        q.Enqueue("09:00:00")
        q.Enqueue("Operator")
        q.Enqueue("DIRECTSCAN")
        q.Enqueue("")
        q.Enqueue(testData.Item("5"))

        Return q
    End Function

    Protected Overrides Function validateTestData() As Dictionary(Of String, String)
        Dim testDataResults As Dictionary(Of String, String) = New Dictionary(Of String, String)

        Return testDataResults
    End Function

    Sub New()
        MyBase.New(messageParsers.NextCartRequest, messageHandlers.NextCartResponse)
    End Sub
End Class
