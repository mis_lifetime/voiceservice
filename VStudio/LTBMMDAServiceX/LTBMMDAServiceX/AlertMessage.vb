﻿Public Class AlertMessage

    Public Property msg As String
    Public Property ttl As String
    Public Property lvl As Integer
    Public Property dte As Date
    Public Property fmtDte As String

    Public Property msgId As ULong
    Public Property msg_txt As String
    Public Property msg_str As String
    Public Property msg_stp As String
    Public Property msg_seq As UInteger
    Public Property msg_note As String
    Public Property msg_rtn As String
    Public Property msg_typ As String
    Public Property msg_key As String
    Public Property msg_ack As String
    Public Property loggerId As String

    Public Property isPDServiceMsg As Boolean

    Private Property mocaConn As MocaConnection
    Public logCommand As String = "log usr voice msg where vmsg_typ = '{0}' and vmsg_data = '{1}' and vmsg_status = '{2}' and tsval = '{3}'"
    Private Property logServiceCommand As String = "log usr new voice msg where msg_id = '{0}' and msg_txt = '{1}' and msg_str = '{2}'" & _
                                            " and msg_stp = '{3}' and msg_seq = '{4}' and msg_rtn = '{5}' and msg_typ = '{6}' and" & _
                                            " msg_key = '{7}' and msg_ack = '{8}' and msg_oth = '{9}' and pd_flag = '{10}' and logger_id = '{11}'"

    Public Sub logAlertMessage()
        Try
            Select Case socketLogMode
                Case logMode.ALL
                    If eventLogLevel <= lvl Then LTBMMDAEventLog.WriteEntry(msg)
                    If dbLogLevel <= lvl Then logAlertMessageToDCS()
                    If dbLogLevel > lvl Then mocaConn.cleanUp()
                Case logMode.DCSONLY
                    If dbLogLevel <= lvl Then logAlertMessageToDCS()
                    If dbLogLevel > lvl Then mocaConn.cleanUp()
                Case logMode.EVENTONLY
                    If eventLogLevel <= lvl Then LTBMMDAEventLog.WriteEntry(msg)
            End Select
        Catch ex As Exception
        End Try
    End Sub

    Overrides Function toString() As String
        Return msg & " " & ttl & " " & fmtDte
    End Function

    Private Sub logAlertMessageToDCS()
        Try
            execGenericMessage()
        Catch ex As Exception
            Try
                msg = ex.ToString
                ttl = "ERROR"
                fmtDte = Format(Now, "yyyyMMddHHmmssfff")

                execGenericMessage()
            Catch exc As Exception
            End Try
        Finally
            mocaConn.cleanUp()
        End Try
    End Sub

    Private Function execGenericMessage() As Integer
        Dim mocaExecResult As Integer = 9999

        Dim qos As Queue(Of String) = New Queue(Of String)
        msg = msg & " | " & mocaConn.getMsg
        msg = msg.Replace("'", "''")
        qos.Enqueue(ttl)
        qos.Enqueue(msg)
        qos.Enqueue(0)
        qos.Enqueue(fmtDte)

        Dim tmcmd = VoiceMessageLib.autoMessageFormatToTemplate(logCommand, qos)
        mocaExecResult = mocaConn.execute(tmcmd)

        Return mocaExecResult
    End Function

    Public Sub logServiceMessage()
        Try
            Dim fmt_stdte As String = msg_str
            Dim fmt_spdte As String = msg_stp

            Dim mocaExecResult As Integer = 9999

            Dim qos As Queue(Of String) = New Queue(Of String)
            msg_txt = msg_txt.Replace("'", "''")
            If Not String.IsNullOrEmpty(msg_rtn) Then msg_rtn = msg_rtn.Replace("'", "''")
            If Not String.IsNullOrEmpty(msg_ack) Then msg_ack = msg_ack.Replace("'", "''")

            qos.Enqueue(msgId)
            qos.Enqueue(msg_txt)
            qos.Enqueue(fmt_stdte)
            qos.Enqueue(fmt_spdte)
            qos.Enqueue(msg_seq)
            qos.Enqueue(msg_rtn)
            qos.Enqueue(msg_typ)
            qos.Enqueue(msg_key)
            qos.Enqueue(msg_ack)
            qos.Enqueue(mocaConn.getMsg)            

            If isPDServiceMsg Then
                qos.Enqueue(1)
            Else
                qos.Enqueue(0)
            End If

            qos.Enqueue(loggerId)

            Dim tmcmd As String = VoiceMessageLib.autoMessageFormatToTemplate(logServiceCommand, qos)

            mocaExecResult = mocaConn.execute(tmcmd)

            If mocaExecResult <> 0 Then
                Try
                    msg = tmcmd
                    ttl = "----"
                    fmtDte = Format(Now, "yyyyMMddHHmmssfff")

                    execGenericMessage()
                Catch ex As Exception
                End Try
            End If
        Catch ex As Exception
            Try
                msg = ex.ToString
                ttl = "ERROR"
                fmtDte = Format(Now, "yyyyMMddHHmmssfff")

                execGenericMessage()
            Catch exc As Exception
            End Try
        Finally
            mocaConn.cleanUp()
        End Try
    End Sub

    Sub New(ByVal mid As ULong, ByVal mtxt As String, ByVal stdte As String, ByVal spdte As String, ByVal msgseq As UInteger, _
            ByVal msgrtn As String, ByVal msgtyp As String, ByVal msgkey As String, ByVal msgack As String, mc As MocaConnection, _
            pdMsg As Boolean, lid As String)
        msgId = mid
        msg_txt = mtxt
        msg_str = stdte
        msg_stp = spdte
        msg_seq = msgseq
        msg_rtn = msgrtn
        msg_typ = msgtyp
        msg_key = msgkey
        msg_ack = msgack
        mocaConn = mc
        isPDServiceMsg = pdMsg
        loggerId = lid
    End Sub

    Sub New(mc As MocaConnection, ByVal m As String, ByVal l As Integer, Optional ByVal t As String = "INFO")
        msg = m
        lvl = l
        ttl = t
        dte = Now
        fmtDte = Format(dte, "yyyyMMddHHmmssfff")
        mocaConn = mc
    End Sub
End Class
