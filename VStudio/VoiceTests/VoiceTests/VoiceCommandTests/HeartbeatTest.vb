﻿Public Class HeartbeatTest
    Inherits BaseVoiceCommandTest

    Protected Overrides Function getMessageBodyDataParameters() As Queue(Of String)
        Dim q As Queue(Of String) = New Queue(Of String)        

        Return q
    End Function

    Protected Overrides ReadOnly Property messageBodyTemplate As String
        Get
            Return ""
        End Get
    End Property

    Protected Overrides Property testMessageType As String = "$HeartBeat$"

    Protected Overrides Property testName As String = "Cancel Request Test"

    Protected Overrides Function validateTestData() As Dictionary(Of String, String)
        Dim testDataResults As Dictionary(Of String, String) = New Dictionary(Of String, String)

        Return testDataResults
    End Function

    Sub New()
        MyBase.New(messageParsers.Heartbeat, messageHandlers.HeartbeatHandler)
    End Sub
End Class
