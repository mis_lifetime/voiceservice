﻿Public Class PickDownloadACKParser
    Inherits PickDirectorMessageParser

    'Public Overrides Property passThroughMessage As Boolean = True
    Public Overrides Property requiresReturnMsg As Boolean = False
    Public Overrides Property messageType As String = "PICKDOWNLOADACK"

    Protected Overrides Function loadMessageBodyKeys() As String()
        Dim mbk As String() = {"MT", "CartID", "LineID"}
        Return mbk
    End Function

    Sub New(ByVal msg As String)
        MyBase.New(msg)
    End Sub
End Class
