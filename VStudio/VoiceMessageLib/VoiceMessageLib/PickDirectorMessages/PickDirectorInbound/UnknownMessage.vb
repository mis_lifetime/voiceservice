﻿Public Class UnknownMessage
    Inherits PickDirectorMessageParser

    Public Overrides Property requiresReturnMsg As Boolean = False

    Protected Overrides Function loadMessageBodyKeys() As String()
        Return {"UNKNOWNMESSAGE"}
    End Function

    Public Overrides Property messageType As String = "UNKNOWN"

    Sub New(ByVal msg As String)
        MyBase.New(msg)
    End Sub
End Class
