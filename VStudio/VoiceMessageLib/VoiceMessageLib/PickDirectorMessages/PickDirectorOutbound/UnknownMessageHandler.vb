﻿Public Class UnknownMessageHandler
    Inherits PickDirectorMessageHandler

    Public Overrides ReadOnly Property messageType As String
        Get
            Return "UNKNOWNMESSAGEHANDLER"
        End Get
    End Property

    Sub New(ByVal msgIn As DelimitedMessageParser)
        MyBase.New(msgIn)
    End Sub
End Class
