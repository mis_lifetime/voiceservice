﻿Public Class CancelStatus
    Inherits PickDirectorMessageParser

    Protected Overrides Function loadMessageBodyKeys() As String()
        Dim mbk As String() = {"MT", "CancelType", "DeleteID", "State"}
        Return mbk
    End Function

    Public Overrides Property messageType As String = "CANCELSTATUS"

    Sub New(ByVal msg As String)
        MyBase.New(msg)
    End Sub
End Class