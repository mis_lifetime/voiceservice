﻿Imports VoiceMessageLibDEV
Imports System.Threading

Public Class CancelRequestReturnMessage
    Inherits ReturnMessage

    Private Function processMessage() As String
        Dim sendString As String = String.Empty

        Try
            sendString = pickDirectorMessageHandler.returnMessage

            'logGenericAlertMessage("send(CancelRequestRetMsg): " & sendString, " (CancelRequestRetMsg) " & pickDirectorMessageHandler.messageType, logLevel.HIGH)
            Dim pdRetData As String = sendAndReceiveData(commPoint, sendString, True, pickDirectorMessageHandler.messageType)
            Thread.Sleep(50)

            If pdRetData Is Nothing Or pdRetData.Equals(String.Empty) Then
                sendString = "NOTHING RETURNED"
            Else
                Dim pdmp As PickDirectorMessageParser = New CancelStatus(pdRetData)

                'StatisticVarsModule.updateStatistic(pdmp)

                Dim rpdmh As PickDirectorMessageHandler = New CancelStatusHandler(pdmp)
                rpdmh.processMessage(mocaConnection.getMocaConnection)
                sendString = rpdmh.getPlainReturnMessage
            End If

            'logGenericAlertMessage("send(CancelRequestRetMsg): " & sendString, " (CancelRequestRetMsg) " & pickDirectorMessageHandler.messageType, logLevel.HIGH)
        Catch ex As Exception
            sendString = ex.ToString
        End Try

        Return sendString
    End Function

    Public Overrides Function getReturnMessage() As String        
        Return processMessage()
    End Function

    Public Overrides Function getReturnMessageWithTimings() As ReturnMessageTimings        

        rmTimings.returnMessage = processMessage()
        Return rmTimings
    End Function

    Sub New(ByVal mh As PickDirectorMessageHandler, mc As MocaConnection)
        MyBase.New(mh, mc)
    End Sub
End Class
