﻿Imports VoiceMessageLib

Public Class MocaConnection
    Private state As UShort
    Private usedCount As ULong
    Private isManaged As Boolean
    Private msg As String
    Private mocaCmd As String
    Private startTime As Date
    Private endTime As Date
    Private mocaConnection As RedPrairie.MOCA.Client.FullConnection

    Public Function execute(ByVal cmd As String) As Integer
        mocaCmd = cmd

        startTime = Now
        Dim r As Integer = mocaConnection.Execute(cmd)
        endTime = Now

        Return r
    End Function

    Public Function executeAndGetResults(ByVal cmd As String) As Dictionary(Of String, String)
        Dim DT As DataTable = New DataTable
        Dim mocaResults As Dictionary(Of String, String) = New Dictionary(Of String, String)

        startTime = Now
        Dim mocaExecResult As Integer = mocaConnection.Execute(cmd, DT)
        endTime = Now

        If mocaExecResult = 0 AndAlso DT.Rows.Count > 0 Then

            Dim lineCount As Integer = 1
            For Each row As DataRow In DT.Rows
                For Each column As DataColumn In DT.Columns

                    mocaResults.Add(column.ToString, row(column).ToString)
                Next
            Next
        End If

        Return mocaResults
    End Function

    Public Sub makeBusy()
        state = 1
    End Sub

    Public Sub makeFree()
        state = 0
    End Sub

    Public Sub setUnmanaged()
        isManaged = False
    End Sub

    Public Function getMocaConnection() As RedPrairie.MOCA.Client.FullConnection
        Return mocaConnection
    End Function

    Public Sub closeConnection()
        Try
            mocaConnection.Close()
        Catch ex As Exception
        End Try

        mocaConnection = Nothing
    End Sub

    Public Function isUnmanaged() As Boolean
        Return Not isManaged
    End Function

    Public Sub incUsedCnt()
        usedCount += 1
    End Sub

    Public Function getUsedCount() As ULong
        Return usedCount
    End Function

    Public Function isConnected() As Boolean
        If mocaConnection Is Nothing OrElse Not getMocaConnection.Connected Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Sub setMsg(ByVal m As String)
        msg = m
    End Sub

    Public Sub wipeMsg()
        msg = ""
    End Sub

    Public Function getMsg() As String
        Return msg
    End Function

    Sub connect()
        If mocaConnection Is Nothing Then
            mocaConnection = VoiceMessageLib.getMocaConn
        Else
            VoiceMessageLib.connectMocaConn(mocaConnection)
        End If
    End Sub

    Public Function isBusy() As Boolean
        If state = 1 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub cleanUp()
        incUsedCnt()
        makeFree()
        wipeMsg()

        If isUnmanaged() Then
            Try
                closeConnection()
            Catch ex As Exception
            End Try
        End If
    End Sub

    Sub New()
        state = 0
        usedCount = 0
        connect()
        isManaged = True
        msg = ""
    End Sub
End Class
