﻿Public Class PickDownloadParser
    Inherits PickDirectorMessageParser

    Public Overrides Property passThroughMessage As Boolean = True

    Protected Overrides Function loadMessageBodyKeys() As String()
        Return {"MT", "CartID"}
    End Function

    Public Overrides Property messageType As String = "PICKDOWNLOAD"

    Sub New(ByVal msg As String)
        MyBase.New(msg)
    End Sub
End Class
