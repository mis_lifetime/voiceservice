﻿Public Class SocketMonitorHandler
    Inherits PickDirectorMessageHandler

    Public Overrides ReadOnly Property messageType() As String
        Get
            Return "SOCKETMONITORRESPONSE"
        End Get
    End Property

    Protected Overrides Function loadMessageBodyTemplate() As String
        Dim s As String = "UNKNOWN"

        Try
            s = baseMessage.messageDictionary.Item("MB_KV")
        Catch ex As Exception
        End Try

        Return s
    End Function

    Protected Overrides Function loadMocaCommandParameters() As System.Collections.Generic.Queue(Of String)
        Dim queueOfParms As Queue(Of String) = New Queue(Of String)
        Dim x As String = String.Empty

        queueOfParms.Enqueue(baseMessage.messageDictionary.Item("MessageType"))
        queueOfParms.Enqueue(pickDirectorBaseMessage.getMocaMessageString)

        Return queueOfParms
    End Function

    Sub New(ByVal msgIn As DelimitedMessageParser)
        MyBase.New(msgIn)
    End Sub
End Class
