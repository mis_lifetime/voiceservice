﻿Imports VoiceMessageLibDEV

Public Class DropCartRequestTest
    Inherits BaseVoiceCommandTest

    Protected Overrides Function getMessageBodyDataParameters() As Queue(Of String)
        Dim q As Queue(Of String) = New Queue(Of String)
        q.Enqueue("DROPCARTREQUEST")
        q.Enqueue(testData.Item("CartID"))
        q.Enqueue(testData.Item("LastPickLoc"))

        Return q
    End Function

    Protected Overrides ReadOnly Property messageBodyTemplate As String
        Get
            Return "{""MT"":""{0}"",""CartID"":""{1}"",""LastPickLoc"":""{2}""}"
        End Get
    End Property

    Protected Overrides Property testMessageType As String = "DROPCARTREQUEST"

    Protected Overrides Property testName As String = "Drop Cart Response Test"

    Protected Overrides Function validateTestData() As Dictionary(Of String, String)
        Dim testDataResults As Dictionary(Of String, String) = New Dictionary(Of String, String)

        If Not testData.ContainsKey("CartID") AndAlso Not testData.ContainsKey("LastPickLoc") Then
            testDataResults.Add("NO_ARG", "Missing TestData Parameter CartID or LastPickLoc")
            Return testDataResults
        Else
            Return testDataResults
        End If
    End Function

    Sub New()
        MyBase.New(messageParsers.DropCartRequest, messageHandlers.DropCartResponse)
    End Sub
End Class
