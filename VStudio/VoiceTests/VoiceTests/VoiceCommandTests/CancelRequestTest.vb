﻿Public Class CancelRequestTest
    Inherits BaseVoiceCommandTest

    Protected Overrides Function getMessageBodyDataParameters() As Queue(Of String)
        Dim q As Queue(Of String) = New Queue(Of String)
        q.Enqueue("CANCELREQUEST")
        q.Enqueue(testData.Item("CancelType"))
        q.Enqueue(testData.Item("CancelID"))

        Return q
    End Function

    Protected Overrides ReadOnly Property messageBodyTemplate As String
        Get
            Return "{""MT"":""{0}"",""CancelType"":""{1}"",""CancelID"":""{2}""}"
        End Get
    End Property

    Protected Overrides Property testMessageType As String = "CANCELREQUEST"

    Protected Overrides Property testName As String = "Cancel Request Test"

    Protected Overrides Function validateTestData() As Dictionary(Of String, String)
        Dim testDataResults As Dictionary(Of String, String) = New Dictionary(Of String, String)

        If Not testData.ContainsKey("CancelType") AndAlso Not testData.ContainsKey("CancelID") Then
            testDataResults.Add("NO_ARG", "Missing TestData Parameter CancelType or CancelID")
            Return testDataResults
        Else
            Return testDataResults
        End If
    End Function

    Sub New()
        MyBase.New(messageParsers.CancelRequest, messageHandlers.CancelResponse)
    End Sub
End Class
