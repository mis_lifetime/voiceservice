﻿Public Class DropCartScan
    Inherits PickDirectorMessageParser

    Protected Overrides Function loadMessageBodyKeys() As String()
        Dim mbk As String() = {"MT", "CartID", "DropOffLocation"}
        Return mbk
    End Function

    Public Overrides Property messageType As String = "DROPCARTSCAN"

    Sub New(ByVal msg As String)
        MyBase.New(msg)
    End Sub
End Class
