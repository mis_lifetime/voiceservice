﻿Public Class PickDownLoadTest
    Inherits BaseVoiceCommandTest

    Protected Overrides Function getMessageBodyDataParameters() As Queue(Of String)
        Dim q As Queue(Of String) = New Queue(Of String)
        q.Enqueue("PICKDOWNLOAD")
        q.Enqueue(testData.Item("CartID"))        

        Return q
    End Function

    Protected Overrides ReadOnly Property messageBodyTemplate As String
        Get
            Return "{""MT"":""{0}"",""CartID"":""{1}""}"
        End Get
    End Property

    Protected Overrides Property testMessageType As String = "PICKDOWNLOAD"

    Protected Overrides Property testName As String = "Pick Download Test"

    Protected Overrides Function validateTestData() As Dictionary(Of String, String)
        Dim testDataResults As Dictionary(Of String, String) = New Dictionary(Of String, String)

        If Not testData.ContainsKey("CartID") Then
            testDataResults.Add("NO_ARG", "Missing TestData Parameter Cart ID")
            Return testDataResults
        Else
            Return testDataResults
        End If
    End Function

    Sub New()
        MyBase.New(messageParsers.PickDownload, messageHandlers.PickDownloadHandler)
    End Sub
End Class
