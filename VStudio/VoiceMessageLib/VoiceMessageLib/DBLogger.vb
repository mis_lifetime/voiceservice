﻿Public Class DBLogger
    Public message As String
    Public logType As String
    Public xyz As Boolean
    Public mRes As String

    Public Sub logIt(x As String)
        message = message.Replace("'", "''")
        Dim qos As Queue(Of String) = New Queue(Of String)
        qos.Enqueue(logType)
        qos.Enqueue(message)
        qos.Enqueue(mRes)

        Dim tmcmd As String = autoMessageFormatToTemplate(mocaCmd, qos)

        Dim resDta As Dictionary(Of String, String) = New Dictionary(Of String, String)
        resDta = VoiceMessageLibGlobals.processMocaCmdAndGetResults(getMocaConn, tmcmd, xyz).Item("VOICE CMD RESULTS")
    End Sub
End Class